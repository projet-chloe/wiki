# Directory traversal

Comme pour toute entrée fournie par l'utilisateur, il est important de s'assurer qu'une stratégie de validation des entrées spécifique au contexte est en place.

Une solution évidente serait, d'abord, de canoniser le nom du chemin complet, puis de valider que le chemin canonisé se trouve dans un répertoire prévu/autorisé sur le système de fichiers.

## JAVA

```java
File file = new File("/tmp/" + filename);
String canonicalPath = file.getCanonicalPath();
if(!canonicalPath.startsWith("/tmp/")) {
    throw new GenericException("Unauthorized access");
}
```

## PHP

```php
//  $id = $_GET['id'];
$id = basename(realpath($_GET['id']));
```

## .NET

la GetFullPath() méthode .NET btient la chaîne de chemin complet du paramètre de fichier en résolvant le chemin relatif des fichiers par rapport au répertoire actuel, et supprime en outre tous les caractères de chemin relatifs tels que ./et../

Enfin, la chaîne de chemin renvoyée par la GetFullPath() méthode est vérifiée par la StartsWith() méthode de .NET pour s'assurer que le chemin d'accès au fichier commence par le bon répertoire. Si cette vérification échoue, l'application générera une Unauthorized accesserreur et reviendra, empêchant ainsi l'utilisateur de charger des fichiers en dehors de la structure de répertoires.

```java
string path = Path.GetFullPath(System.Web.HttpContext.Current.Server.MapPath("/")) + fileName;
if (!path.StartsWith(System.Web.HttpContext.Current.Server.MapPath("/") + "trades\\")){
    Response.Write("Unauthorized Access");
    Response.End();
    return;
}
```