# Insecure Object Deserialization

Tout d'abord, Alice doit se rappeler que la désérialisation de contenu arbitraire fourni par l'utilisateur est une chose dangereuse.

Deuxièmement, Alice doit s'assurer de ne pas faire confiance aux données sérialisées et d'utiliser la liste blanche : définir des autorisations explicites pour les types, pour vérifier le type d'un objet qui doit être désérialisé.

## JAVA

request.getReader() : méthode pour extraire le corp d'une requete POST   
IOUtils.toString() : convertit le corps POST extrait en une chaîne XML    
méthode fromXML() de l'objet XStream : est appelée pour désérialiser un objet Account Manager à partir d'un XML    
Dans son état par défaut, un objet XStream a accès à toutes les classes disponibles pour le code ; cela peut inclure des classes dont l'utilisation n'était pas prévue par le développeur. 

Pour atténuer le problème avec l'objet XStream par défaut ayant accès à toutes les classes disponibles pour le code, la portée des classes auxquelles XStream a accès doit être réduite uniquement à celles qui sont requises et ont été conçues par le développeur d'une manière qui ne permettre l'exécution de tout code malveillant.

NoTypePermission.NONE : définit les TypePermissions sur None, et XStream ne peut plus accéder à tout l'espace des classes disponibles pour l'exécution du code.

Ensuite, en ajoutant une permission spécifique à la classe ExplicitTypePermission, le développeur garantit que les seules classes qui peuvent être désérialisées par XStream sont celles de son choix, comme le AccountManager.class.

```java
public class XStreamDeserializer {
    public static AccountManager getAccountManager(HttpRequestServlet request) {
        XStream xstream = new XStream();
        xstream.addPermission(NoTypePermission.NONE);
        TypePermission tp = new ExplicitTypePermission(
            new Class[] {
                AccountManager.class
            });
        xstream.addPermission(tp);
        try {
            String accountManagerXML = IOUtils.toString(request.getReader());
            AccountManager am = (AccountManager)xstream.fromXML(accountManagerXML);
            return am;
        }
        catch ( /*..*/ ) {
            /* Handle various XStream parsing exceptions */
            return null;
        }
    }
}
```

## PHP

Dans cette classe, __construct()la fonction désérialise d'abord le corps sérialisé de la requête POST à ​​l'aide du unserialize()module, puis transfère les paramètres à la createOrFetchAccountManager()fonction.

Cela ne signifie pas qu'en tant que programmeur, vous ne devriez pas utiliser les fonctions __construct()ou __destruct(). Cela signifie seulement que vous devez éviter d'utiliser la désérialisation n'importe où dans votre code.

Utilisez JSON pour transférer des données dans les requêtes POST et json_decodepour en extraire des données.

```php
public function __construct($fullname, $email, $phone){
    //if (isset($_POST['accountmanager'])) {
    //  $am = unserialize($_POST['accountmanager']);
    //}
    if (isset($_POST['AccManName']) && isset($_POST['AccManEmail']) && isset($_POST['AccManPhone'])){
      $am_params = json_decode($HTTP_RAW_POST_DATA);
      $fullname = $am_params['AccManName'];
      $email = $am_params['AccManEmail'];
      $phone = $am_params['AccManPhone'];
      $am = new AccountManager($name, $email, $phone);
    }
    $this->fullname = $fullname;
    $this->email = $email;
    $this->phone = $phone;
    $this->userId = $this->createOrFetchAccountManager($fullname, $email, $phone);
    $this->verifyAccountManager();
}
public function createOrFetchAccountManager($fullname, $email, $phone) {
    return userId;
}
```

## .NET

StreamReader.ReadToEnd() permet de lire le corps d'une requete PoST   
La JsonSerializerSettings classe est ensuite utilisée pour spécifier les autorisations de paramètres : avec le TypeNameHandling paramètre défini sur All, il devient possible d'écrire et d'utiliser des noms de type pour les objets et les collections lors de la désérialisation.   
Après cela, la Json.Net bibliothèque est utilisée pour désérialiser l' AccountManagerobjet. La méthode de l'objet Json.Net JsonConvert.DeserializeObject()est appelée pour désérialiser un objet Account Manager à partir d'un JSON, en utilisant la accountManagerJSONvariable String comme entrée JSON.

Par défaut, Json.NET ne lit ni n'écrit les noms de type lors de la désérialisation. Lorsque le TypeNameHandling paramètre est défini sur All, l'objet Json.NET a accès à toutes les classes disponibles pour le code ; cela peut inclure des classes qui ne devaient pas être utilisées par le développeur.

Par défaut , le TypeNameHandling paramètre est défini sur None, et il ne permet pas de lire ou d'écrire des noms de type pendant la désérialisation. Cela empêche explicitement la gestion des noms par défaut pour les types .NET, empêchant par conséquent la création d'objets .NET arbitraires. Cependant, cela peut également restreindre les fonctionnalités à un ensemble limité de classes.

```java
public static AccountManager getAccountManager(){
  string accountManagerJSON;
  using (Stream receiveStream = Request.InputStream)  {
    using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))    {
      accountManagerJSON = readStream.ReadToEnd();
    }
  }
  JsonSerializerSettings settings = new JsonSerializerSettings();
//settings.TypeNameHandling = TypeNameHandling.All;
  settings.TypeNameHandling = TypeNameHandling.None;
  try {
 // AccountManager am = (AccountManager)JsonConvert.DeserializeObject(accountManagerJSON, settings);
    AccountManager am = JsonConvert.DeserializeObject< AccountManager>( accountManagerJSON, settings);
    return am;
  }
  catch { /* Handle Exceptions */ }
}
```
