# Injection SQL

Les requêtes préparées (autrement dit les requêtes paramétrées) constituent le meilleur mécanisme pour contrer les attaques par injection SQL.

On utilise les requêtes préparées pour séparer la syntaxe SQL des paramètres d'entrée. Des templates (modèles) de requête sont d'abord définis au niveau de la couche application, puis on leur passe des paramètres.

Non seulement les requêtes préparées constituent une bonne protection contre les attaques par injection SQL, mais elles permettent également d'avoir un code de meilleure qualité, plus lisible et facile à maintenir, car elles séparent la logique SQL des valeurs à traiter.

Une autre façon d'éviter les injections SQL est d'utiliser ORM ( Object Relational Mapping). Il fournit au développeur un moyen de manipuler la base de données sans écrire de requêtes SQL en définissant des relations entre les tables de base de données et les objets d'application.

(Notez que ORM autorise les requêtes SQL brutes vulnérables à l'injection SQL si la requête est construite en concaténant l'entrée utilisateur avec les mots clés SQL. Les requêtes brutes sont souvent utilisées pour les requêtes SQL complexes difficiles à créer à l'aide d'objets. Ne pas concaténer l'utilisateur -données fournies avec des requêtes SQL effectuées par ORM .)

## JAVA

```java
String query = "select * from sqli.payments where userid = ? and comment LIKE ? and type = ? ORDER BY date";
PreparedStatement statement = connection.prepareStatement(query);
statement.setInt(1,currentUserId);
statement.setString(2,"%"+paymentComment+"%");
statement.setInt(3,paymentType);
ResultSet rs = statement.executeQuery();`
```

## PHP

### Via Prepare
```php
$statement = $connection->prepare("select * from payments p where p.userid = ? and p.type = ? and p.comment like concat('%',?,'%') ORDER BY p.date"); 
$statement->bind_param("iis", $currentUserId, $paymentType, $paymentComment); 
$statement->execute();
$result = $statement->get_result();
```
ou 

```php
$stmt = $db->prepare("SELECT * FROM users WHERE (email=:email AND password=:password) LIMIT 0,1");
$stmt->bindParam(':email', $email);
$stmt->bindParam(':password', $password);
$stmt->execute();
```

### Via ORM   
Le framework Laravel a un ORM intégré. Par conséquent, l'utilisation d'instructions préparées est superflue. Cependant, la recommandation est toujours valable si l'ORM n'est pas utilisé. Au lieu d'interpoler les valeurs dans la chaîne d'instructions, le générateur de requêtes peut être utilisé.   

```php
$result = DB::table("payments")
    ->where("userid", "=", ":currentUserId")
    ->where("comment", "=", ":paymentComment")
    ->where("type", "=", ":paymentType")
    ->setBindings([
        "currentUserId" => $currentUserId, 
        'paymentComment' => $paymentComment, 
        'paymentType' => $paymentType
    ])
    ->get();
```

## .NET

### Via parametre

L'un des moyens sécurisés de créer une instruction SQL consiste à utiliser le paramétrage. Les paramètres ne sont pas interprétés comme faisant partie de la syntaxe SQL, ils ne peuvent donc pas casser la syntaxe SQL et conduire à l'injection SQL.   
Notez que .Net Core Entity Framework crée en toute sécurité une requête SQL lorsqu'elle est construite à l'aide d'une interpolation de chaîne à l'intérieur FromSql ou d'arguments de méthode équivalents.

```java
public IEnumerable<PaymentEntity> GetFilteredPayments(SearchFilter filter){
    var currentUserId = user.id;
    var paymentType = filter.Type;
    var paymentComment = filter.Comment;
//  return _context.Payments.FromSql("select * from payments where userid = " + currentUserId + " and type = " + paymentType + " and comment LIKE '%" + paymentComment + "%' ORDER BY date");
    return _context.Payments.FromSql($"select * from payments where userid = {currentUserId} and type = {paymentType} and comment LIKE '%{paymentComment}%' ORDER BY date", currentUserId, paymentType, paymentComment); 
}
```

ou 

```java
//return _ctx.Transfers.FromSql("select * from Transfers where username = '" + username + "'").toList();
var searchParam = new SqlParameter("username", username);
return _ctx.Transfers.FromSql("select * from Transfers where username = @username", searchParam).ToList();
```

ou 

Remarquez que les noms des variables email et password sont maintenant précédés du symbole @ qui agit comme un « placeholder » (paramètre fictif) pour notre requête paramétrable.

```java
//    string sql = "SELECT UserId FROM User WHERE " + "(email = '" + email + "' AND " + "password = '" + password + "')";
string sql = "SELECT UserID FROM User WHERE email = @email and password = @password ";
using (MySqlCommand cmd = new MySqlCommand(sql)){
    cmd.Parameters.AddWithValue("@email", email);
    cmd.Parameters.AddWithValue("@password", password);
    cmd.Connection = connnection;
    try{
        cmd.Connection.Open();
        var userId = cmd.ExecuteScalar();
    }
    catch (SqlException sx){
        // Handle exceptions before moving on.
    }
}
```
### Via ORM

L'ORM (Object Relational Mapping) est un autre moyen sécurisé de manipuler les données dans la base de données. C'est une meilleure alternative aux requêtes SQL brutes. Si vous avez besoin d'une recherche complexe qui ne peut pas être réalisée par ORM, utilisez des requêtes SQL paramétrées.

```java
return _context.Payments.Where(c => c.Userid == currentUserId && c.Type == paymentType && c.Comment.Contains(paymentComment)).ToList();
```

## Second order SQL injection

L' injection SQL de second ordre est un type moins visible de bonne vieille injection SQL. Contrairement à l'injection SQL standard, l'entrée de l'utilisateur n'influence pas directement la requête SQL. Au lieu de cela, une charge utile malveillante provenant de l'entrée de l'utilisateur est, d'abord, enregistrée dans la base de données, et seulement après cela, elle est utilisée par l'application dans la requête SQL.

Le flux typique de l' injection SQL de second ordre :

1. Les données contaminées sont insérées dans la base de données en toute sécurité en utilisant une approche qui empêche les injections SQL.

2. Les données contaminées sont ensuite utilisées de manière non sécurisée dans le cadre d'une requête SQL menant à l'injection SQL.

Pour résumer, toutes les requêtes SQL doivent être effectuées de manière sécurisée, que vous envisagiez ou non d'utiliser l'entrée de l'utilisateur dans la requête. Les données stockées dans la base de données doivent toujours être traitées comme non fiables et une approche de défense en profondeur doit être appliquée.