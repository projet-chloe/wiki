# Nombre aléatoire insuffisant

Le meilleur moyen de remédier à une vulnérabilité due à des valeurs insuffisamment aléatoires est d'utiliser un algorithme actuellement considéré comme fort par les experts du domaine, et de sélectionner des implémentations bien testées avec des graines d'une longueur suffisante.

En général, lorsqu'un générateur de nombres pseudo-aléatoires n'est pas considéré comme cryptographiquement sûr, il ne doit pas être utilisé dans des contextes sensibles en matière de sécurité.

Les générateurs de nombres pseudo-aléatoires peuvent produire des nombres prévisibles si le générateur est connu et qu'il est possible de deviner la graine utilisée. Une graine de 256 bits est un bon point de départ pour produire un nombre « suffisamment aléatoire ».

exemple vulnérabilité :

les développeurs n'ont pas envisagé l'utilisation d'un générateur de nombres aléatoires pour générer leur identificateur de session et se sont naïvement reposés sur l'utilisation de la méthode time() pour garantir le caractère unique de la session.   
La méthode time() renvoie l'heure actuelle en millisecondes, qui est ensuite passée en paramètre à la méthode PHP session_id(), ce qui entraîne l'attribution d'identifiants de session faibles aux utilisateurs.

## PHP

La génération d'une série de valeurs alphanumériques uniques et sécurisées peut se faire en utilisant la méthode openssl_random_pseudo_bytes().

la méthode openssl_random_pseudo_bytes() renvoie une valeur de session unique. Cela garantit que la valeur rendue est suffisamment aléatoire pour être utilisée comme identifiant de session, empêchant ainsi les attaquants potentiels de prévoir cette valeur. L'intégrité et la confidentialité de la session sont ainsi assurées.

```php
$custom_id = bin2hex(openssl_random_pseudo_bytes(128));
```

Cependant, la conception logique et la sécurité peuvent être considérablement améliorées en évitant de développer votre propre système de gestion de session et en utilisant plutôt les routines natives de génération de session de PHP telles que session_start() , session_regenerate_id()

D'autres améliorations peuvent être faites en modifiant php.ini, par exemple en s'assurant que session.hash_function utilise un hachage SHA plutôt que MD5, en configurant un session.entropy_file , et en spécifiant le nombre d'octets d'entropie avec session.entropy_length

## Java

En Java, la génération d'une série de nombres aléatoires cryptographiquement sûrs peut se faire en utilisant la classe java.security.SecureRandom. D

la méthode rand.nextBytes() génère aléatoirement 20 octets qui sont encodés sous forme hexadécimale en utilisant la méthode encodeHex() 

Ainsi, en utilisant SecureRandom, nous pouvons garantir que la valeur renvoyée pour le cookie est suffisamment aléatoire pour interdire la prévision des identifiants de session par des attaquants potentiels. L'intégrité et la confidentialité de la session sont ainsi assurées.

```java
// Generate and return a new session identifier.
protected String newSession(){
    //  long now = System.currentTimeMillis();
    SecureRandom rand = new SecureRandom();
    byte bytes[] = new byte[20];    
    rand.nextBytes(bytes);  
    String cookie = new String(Hex.encodeHex(bytes));   
    return cookie 
}
```

## .NET

En .NET, la génération d'une série de nombres aléatoires cryptographiquement sûrs peut se faire en utilisant la classe RNGCryptoServiceProvider.     
nous commençons par créer une instance de la classe RNGCryptoServiceProvider et nous l'affectons à la variable randgen.    
La méthode GetBytes() est ensuite appelée pour générer un tableau d'octets uniques qui est utilisé pour remplir le buffer d'octets buffer.     
Enfin, la valeur de buffer est passée à BitConverter.ToString qui convertit le tableau d'octets buffer en une chaîne hexadécimale.     
L'utilisation de RNGCryptoServiceProvider nous garantit que le cookie renvoyé est suffisamment aléatoire pour être utilisé comme identifiant de session et empêcher les attaquants potentiels d'en prévoir la valeur. L'intégrité et la confidentialité de la session sont ainsi assurées.

```java
//Generate and return a new session identifier.
static RNGCryptoServiceProvider randgen = new RNGCryptoServiceProvider();
public override string CreateSessionID(HttpContext context){
    byte[] buffer = new byte [15];
    randgen.GetBytes(buffer);
    return BitConverter.ToString(buffer);
//  return DateTime.Now.Ticks.ToString();
}
```