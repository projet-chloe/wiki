# LDAP injection

## vulnérabilité

LDAP est un protocole d'accès et de maintenance des services d'informations d'annuaire. Il permet aux applications et services se connectant au serveur LDAP de récupérer des données de l'annuaire, par exemple pour authentifier les utilisateurs. Pour rechercher et récupérer des données dans le répertoire, un filtre doit être défini et les données correspondantes seront renvoyées en conséquence.

L' injection LDAP se produit lorsqu'une entrée utilisateur non filtrée est utilisée dans le cadre du filtre LDAP . Il permet à l'attaquant de manipuler la requête et de contourner les mécanismes de sécurité, de récupérer des données sensibles ou d'exécuter le code malveillant.

En général, les conséquences d'une injection LDAP réussie dépendent du contexte dans lequel la requête LDAPest utilisé:
- Contournement de l'authentification : (&(nom d'utilisateur=%user_input%)(mot de passe=%user_input%))    
- Élévation de privilèges : (&(documentName=%user_input%)(securityLevel=low)(type=secretDocuments))   
- Divulgation d'information : (&(nom d'utilisateur=%entrée_utilisateur%)(groupe=marketing))   

Les filtres LDAP consistent en une ou plusieurs conditions, chaque condition est entourée de crochets. Plusieurs conditions peuvent être utilisées avec les opérateurs AND ou OR . L'opérateur encapsule les conditions avec un autre jeu de parenthèses et ajoute | ou & avant la première condition. S'il existe plusieurs conditions dans la requête sans opérateurs AND ou OR , seule la première condition est utilisée par le serveur (le comportement peut varier selon le serveur d'annuaire).

1. Exemple insertion dans une seule condition (attribut=valeur) :

Compte tenu de la syntaxe de la requête LDAP, il n'y a pas tellement d'endroits où l'entrée utilisateur pourrait être injectée dans la requête (attribut=valeur). Par exemple , si la requête ressemble à ceci : (uid=%user_input%), le * peut être inséré pour charger tous les enregistrements possibles. Des conditions supplémentaires ne peuvent pas être ajoutées car il n'y a pas d'opérateurs ET ou OU qui pourraient combiner les deux conditions ensemble.

2. Exemple injection dans OR (|(attribut=valeur)(attribut=valeur)) :

La requête contenant l' opérateur OR offre à l'attaquant plus d'options d'insertion. Par exemple , lorsque la requête ressemble à ceci : (|(name=%user_input%)(type=laptop)), l'attaquant pourrait insérer la condition supplémentaire qui est vraie pour chaque enregistrement du répertoire, obligeant ainsi le serveur à envoyer tous enregistrements dont le paramètre commonName (CN) est défini. La charge utile serait la suivante anyName)(cn=* et la requête résultante serait (|(name=anyName)(cn=*)(type=laptop)) .

3. Exemple injection dans AND (&(attribut=valeur)(attribut=valeur)) : 

Dans l' exemple suivant , l'utilisateur définit le nom du document à rechercher. Le niveau d'accès de l'utilisateur est prédéfini comme bas : (&(documentName=%user_input%)(accessLevel=low)) .   
L'attaquant peut contourner la contrainte de niveau de sécurité et accéder à tous les documents en insérant le payload suivant : secretDocument)(accessLevel=\*))(&(uid=*

La requête résultante est : (&(documentName=secretDocument)(accessLevel=* ))(&(uid=*)(accessLevel=low)) qui affichera les documents quel que soit le niveau d'accès de l'utilisateur.

Le premier ET opérateur de la requête récupère le nom de document spécifié avec n'importe quel niveau d'accès, le second opérateur est ignoré par l'analyseur de requête. La charge utile ne rompt pas la syntaxe de la requête, la requête est donc évaluée par le serveur d'annuaire et les données demandées sont renvoyées.

## Remédiation

1. En tant que recommandation générale, il convient d'éviter complètement d'utiliser l'entrée utilisateur dans le cadre de la requête LDAP . Si cela n'est pas possible, l'approche de sécurité approfondie doit être utilisée : des contrôles de sécurité doivent être en place à la fois dans l'application qui effectue les requêtes LDAP et dans le serveur d'annuaire.

2. Nettoyez la saisie de l'utilisateur et autorisez uniquement l'utilisation de caractères alphanumériques dans le cadre des requêtes LDAP . Notez que la mise sur liste noire de caractères spéciaux pourrait résoudre le problème, mais les listes noires ont tendance à être contournées, l'approche de la liste blanche est donc préférable.

3. Ne prenez pas de décisions d'accès uniquement du côté de l'application. Par exemple, si vous définissez le niveau d'accès dans la requête, alors en cas d' injection LDAP, il pourrait y avoir un moyen de contourner le niveau d'accès codé en dur.

4. Utilisez la paramétrisation pour créer des filtres de recherche. Dans ce cas, l'entrée utilisateur est utilisée comme paramètre et ne casse pas la syntaxe du filtre.

5. Notez que si vous créez une chaîne et que vous l'utilisez comme filtre de recherche, quelle que soit la manière dont vous l'avez construite (par exemple, par concaténation ou par interpolation), votre code est probablement vulnérable .

## JAVA

La requête est construite à l'aide de la classe LdapQueryBuilder. Il permet de construire une requête avec l'entrée de l'utilisateur en toute sécurité.

```java
//query().filter("(&(uid=" + login + ")(userPassword=" + password + "))"), 
query().where("uid")
.is(login)
.and("userPassword")
.is(password),
```

## PHP

La ldap_escape méthode échappe les caractères spéciaux LDAP, ainsi un attaquant ne peut pas manipuler la structure de la requête LDAP.

```php
$loginEscaped = ldap_escape($login);
$passwordEscaped = ldap_escape($password);
$results = ldap_search($connect, env('LDAP_CN'), "(&(uid=" . $loginEscaped . ")(userPassword=" . $passwordEscaped . "))"););
```

## .NET

```java
//var searchFilter = "(&(uid=" + Login +")(userPassword="+ Password +"))";
var searchFilter = "(&(uid=" + Encoder.LdapFilterEncode(Login) + ")(userPassword="+ Encoder.LdapFilterEncode(Password) + "))";
```