# CSRF

## GET

Il est déconseillé d'utiliser des requêtes HTTP GET pour effectuer des opérations de mise à jour, de création et de suppression en fonction d'une entrée provenant de l'utilisateur. Il s'agit là d'un exemple de mauvaise utilisation des méthodes HTTP.

Dans le cas de l'application TradeIDEAS2, les développeurs ont improprement utilisé la méthode GET pour invoquer la fonction Tout supprimer. Les bonnes pratiques imposent d'utiliser la méthode GET que pour récupérer des données, alors que les méthodes POST, PUT, PATCH, et / ou DELETE doivent être utilisées pour toutes les actions modifiant l'état de l'application.

Par défaut, de nombreux frameworks de développement Web forcent l'utilisation des méthodes HTTP appropriées. Hors framework, pour une application gérant des données, on peut utiliser le modèle suivant afin de se conformer aux bonnes pratiques en matière d'opérations CRUD :

1. Utiliser HTTP GET pour les opérations de Lecture (SQL SELECT)   
2. Utiliser HTTP PUT pour les opérations de Mise à jour (SQL UPDATE)    
3. Utiliser HTTP POST pour les opérations de Création (SQL INSERT)    
4. Utiliser HTTP DELETE pour les opérations de Suppression (SQL DELETE)    

## POST

La défense contre les attaques CSRF n'est pas aussi simple que la défense contre les attaques XSS. Un modèle de jeton de synchronisation est nécessaire pour se défendre contre les attaques CSRF. Un jeton de synchronisation est également appelé jeton anti-CSRF, jeton CSRF, jeton de défi ou nonce.

Pour être efficace, chaque réponse du serveur Web nécessite la génération d'un jeton aléatoire. Ce jeton est ensuite inséré par l'application sous forme de texte caché dans les champs sensibles du formulaire.

En adoptant cette approche, l'application peut alors vérifier, chaque fois qu'un utilisateur soumet un formulaire, que le jeton est à la fois valide et correct. Il s'agit d'une bonne stratégie de protection contre les attaques CSRF, car l'attaquant ne devrait jamais connaître cette valeur générée aléatoirement.

Elle permet à l'application de répondre à une question simple : Le formulaire POST par l'utilisateur a-t-il été légitimement créé par l'application ou par un tiers inconnu ? Est-il valide ou invalide ?

Notez que chaque fois qu'une génération de nombres pseudo-aléatoires (pRNG) cryptographiquement sécurisée est nécessaire, il ne faut pas réinventer la roue. L'utilisation correcte de java.security.SecureRandom est fortement préférée à un code de génération de nombres aléatoires personnalisé, car ce dernier est extrêmement sujet aux erreurs des développeurs.

## PHP

De nombreux frameworks de développement Web fournissent des moyens simples pour intégrer des mécanismes permettant d'implémenter les protections décrites dans cette leçon.

Plutôt que de réinventer la roue, il est recommandé d'utiliser les fonctionnalités natives de votre framework de développement.

Voici quelques références :

Framework Laravel : http://laravel.com/docs/master/routing#csrf-protection

Framework Symfony : http://symfony.com/doc/current/cookbook/security/csrf_in_login_form.html

Framework Nette : http://doc.nette.org/vulnerability-protection#toc-cross-site-request-forgery-csrf


```html
 <input type="hidden" name="csrf-token" value="uRARsEXKdVjX6iUnQkDcfHiNqvG">
```

## .NET

La pile Web ASP.NET MVC de Microsoft utilise sa propre implémentation du modèle de jeton Synchronizer à l'aide de la HtmlHelper.AntiForgeryToken méthode et de la ValidateAntiForgeryToken classe d'attributs.

Lorsque Synchronizer Token Pattern est utilisé, il est généralement basé sur les deux jetons qui sont soumis au serveur avec chaque requête HTTP POST (en plus du jeton d'authentification) : un jeton - dans un cookie et l'autre - avec la valeur du formulaire.

Les valeurs de jeton générées par le runtime ASP.NET ne sont pas déterministes ou prévisibles par un attaquant. Ils ressemblent à ce qui suit :

`<input name="__RequestVerificationToken" type="hidden" value="i411mJIr0mZKrk17g4Hf-0_G6aXOJLkzzGfd5yn2mVsTqj-35j_n0YUUCzFRXoFet3BXUVpBicpL3p-AqPPA3XEXEtykt4X-_MbRIxLQH6M1" />`

Lorsque les jetons utilisés dans le Synchronizer Token Pattern sont soumis, l' application TradeIDEA autorisera la demande à se poursuivre uniquement si les deux jetons réussissent un contrôle de comparaison.

Pour utiliser les fonctions d'assistance du modèle de jeton de synchronisation d'ASP MVC, nous ajoutons d'abord la Html.AntiForgeryToken() méthode d'assistance dans notre code "View", qui génère un champ de formulaire masqué (jeton anti-contrefaçon) qui est soumis lorsque l' action 'Supprimer tout' est invoquée par un utilisateur. Dans le même temps, la Html.AntiForgeryToken() méthode attribuera également à l'utilisateur un cookie appelé __RequestVerificationTokenavec la même valeur que le jeton anti-contrefaçon.

Ensuite, nous ajoutons l'[ValidateAntiForgeryToken] attribut à notre action 'Supprimer tout', qui vérifie si la requête entrante POST contient les cookies et les form valeurs pertinentes générées par la Html.AntiForgeryToken méthode et si ces valeurs correspondent.

Suite à ce changement, la tentative de l'attaquant d'exploiter le CSRF échouera, étant donné qu'il serait incapable de deviner les valeurs spécifiques à la session pour la vérification de la demande et la valeur du champ de formulaire caché générée par la Html.AntiForgeryToken() méthode.

```java
//////// VIEW ////////

<% using(Html.Form("Ideas", "Delete_All")) { %>
    <%= Html.AntiForgeryToken() %>
    <!-- rest of form goes here -->
<% } %>

//////// CONTROLLER ////////

[ValidateAntiForgeryToken]
public ActionResult Delete_All()
{
    // Delete all trade ideas ...
}
​​```