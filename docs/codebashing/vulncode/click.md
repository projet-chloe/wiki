# Click Jacking

Pour contrer les attaques par Clickjacking dans les navigateurs modernes, on peut mettre en place un en-tête Content-Security-Policy afin de restreindre l'utilisation des frames, en utilisant une directive frame-ancestors qui précise les pages pouvant intégrer la page courante.    
Elle peut prendre les valeurs suivantes :  
- none qui a le même effet que DENY   
- self qui a le même effet que SAMEORIGIN     
- URI source permet à l'administrateur du serveur de spécifier le schéma, l'hôte et le port des pages parentes autorisées.    

D'autre part, pour la majorité des navigateurs, il est possible de définir l'en-tête de réponse X-Frame-Options. Cet en-tête indique au navigateur quels sont les autres domaines autorisés à inclure la page Web dans des frames :    
- DENY interdit toute possibilité d'intégrer la page courante dans un frame    
- SAMEORIGIN n'autorise l'inclusion que si les pages proviennent du même domaine que le domaine courant    
- ALLOW-FROM permet de spécifier un URI de domaine unique pouvant inclure la page.     

Malheureusement, cela signifie que pour plusieurs domaines, l'entête ALLOW-FROM nécessitera une implémentation définissant correctement l'en-tête en fonction de l'en-tête du référent de la requête.

## remédiation code

Pour empêcher les attaques par Clickjacking, l'Internet Engineering Task Force (IETF) a publié un certain nombre d'en-têtes HTTP qui, une fois configurés, fournissent une protection efficace contre ces attaques. 

Dans notre exemple, nous avons paramétré ces en-têtes HTTP en utilisant le module mod_header.so qui fournit des directives pour contrôler et insérer des en-têtes HTTP de requête et de réponse.

Le fait de paramétrer l'en-tête de réponse X-Frame-Options avec deny indique au navigateur de l'utilisateur d'empêcher le chargement et l'affichage de la page attaqué dans un iframe.

L'en-tête Content-Security-Policy est une autre méthode efficace pouvant être utilisée pour empêcher l'inclusion de traderdash.codebashing.com dans un iframe. Notez cependant que la prise en charge de Content-Security-Policy est limitée et que les anciennes versions des navigateurs l'ignorent. Il est donc recommandé d'activer à la fois les en-têtes X-Frame-Options et Content-Security-Policy dans le fichier de configuration de votre serveur Web.

```php
<IfModule mod_headers.so>
# Prevent Clickjacking using X-Frame-Options header
Header set X-Frame-Options "deny"

# Prevent Clickjacking using Content Security Policy (Not supported by all major browsers yet)
Header set Content-Security-Policy: frame-options 'deny'; # Chrome / Firefox
Header set X-Content-Security-Policy: frame-options 'deny'; # Internet Explorer
</IfModule>
```

## PHP

En PHP, on peut effectuer cette configuration en intégrant le bloc de code suivant dans l'include de configuration générale de votre application : `<%= "" %> `

Cependant, une approche plus extensible consiste à déployer les en-têtes HTTP de façon générale, au niveau du serveur Web. 

Notez cependant que la prise en charge de Content-Security-Policy est limitée et que les anciennes versions des navigateurs l'ignorent. Il est donc recommandé d'activer à la fois les en-têtes X-Frame-Options et Content-Security-Policy dans le fichier de configuration de votre serveur Web.

## .NET

Dans notre exemple, nous avons paramétré ces en-têtes HTTP dans l'élément customHeaders de web.config, qui fournit des directives pour contrôler et insérer des en-têtes HTTP de requête et de réponse.

```s
<configuration>
   <system.webServer>
      <httpProtocol>
         <customHeaders>
            <add name="Content-Security-Policy" value="default-src 'self'; script-src 'self' www.google-analytics.com *.cloudflare.com; img-src *.cloudflare.com" />
            <add name="X-Content-Type-Options" value="nosniff" />
            <add name="X-Frame-Options" value="DENY" />
         </customHeaders>
      </httpProtocol>
   </system.webServer>
</configuration>
```
## exemple 

une balise iframe dans la page Web malveillante, qui charge la page à attaquer. Remarquez que cet iframe a été stylé en utilisant le paramètre CSS opacity: 0 afin de s'assurer que l'iframe sera invisible une fois que l'utilisateur aura chargé la page Web malveillante.

```php
<iframe id="evil_iframe" src="https://traderdash.codebashing.com" frameborder="1" style="opacity: 0"></iframe>
```