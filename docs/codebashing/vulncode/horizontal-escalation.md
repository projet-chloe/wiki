# Escalation de privilège horizontal

Selon la nature de l'application, les modèles d'autorisation peuvent varier en complexité. Les modèles de contrôle d'accès basés sur les rôles courants incluent :

1. Groupes > Utilisateurs   
2. Groupes > Utilisateurs > Autorisations    

Dans le cas général, il est essentiel de vérifier systématiquement que toute opération CRUD d'utilisateur est autorisée et que l'autorisation est effectuée conformément à la procédure bien définie. modèle de contrôle d'accès.

Toutefois, si vous devez exposer des références directes aux structures de base de données, assurez-vous que les instructions SQL et les autres méthodes d'accès à la base de données autorisent uniquement l'affichage des enregistrements autorisés. Ceci peut être réalisé en ajoutant une sensibilité au contexte à la requête SQL afin qu'elle filtre uniquement les informations de profil auxquelles l'utilisateur est autorisé à accéde

## JAVA

```java
String uid = request.getParameter("uid");
String currentUser = request.getUserPrincipal().getName();
PreparedStatement qUser = null;

//String qString = "select * from users where userid = ?";
String qString = "select * from users where userid = ? and username = ?";
qUser = conn.prepareStatement(qString);
qUser.setString(1,uid);
qUser.setString(2,currentUser);
ResultSet user = qUser.executeQuery();
```

## PHP

```php
$user = $_SESSION['user']
$sql = 'SELECT * FROM users WHERE id = ? and username = ?'; 
//$sql = 'SELECT * FROM users WHERE id = ?';
$query = $this->_em->createQuery($sql);
$query->setParameter(1, $uid);
$query->setParameter(2, $user);
    return = $query->getSingleResult();
```

## .NET

nous passons currentUserc omme paramètre supplémentaire à notre requête de recherche d'utilisateur modifiée, qui effectuera une vérification de contexte supplémentaire (via le e.usernamechamp ), garantissant que seul le profil des utilisateurs actuellement connectés est chargé par le serveur d'application.

```java
User user = context.Users.Single(e => e.userid == uid && e.username == currentUser);
```