# Fixation de Session

Une combinaison des bonnes pratiques suivantes peut vous aider à vous défendre contre les attaques par fixation de session :

1. Assurez-vous que seules des valeurs de session générées par le serveur sont acceptées par l'application.   

2. Après une connexion réussie, invalidez le jeton de session original et émettez un nouveau jeton de session.    

3. Interdisez à l'application d'accepter des jetons de session par l'intermédiaire de requêtes GET ou POST et ne stockez les valeurs de session que dans des cookies HTTP.    


## PHP

La méthode session_regenerate_id() invalide la valeur de session existante d'Alice avant de réémettre pour elle un nouveau jeton de session, ce qui garantit le recyclage de son identifiant de session à l'issue de l'authentification.

```php
function authenticate($username, $password){
    $results = $accountHandler->login($email, $password);
    if ($results) {
        session_start();
        // Prevent Session Fixation (http://en.wikipedia.org/wiki/Session_fixation)
        session_regenerate_id();        
        $_SESSION['username'] = $username;
        return true;
    } else {
        return false;
    }
}
```

## Java

la méthode request.getSession() pour extraire la valeur de session existante et nous l'affectons à la variable HttpSession session.      
La session existante est ensuite invalidée en appelant la méthode session.invalidate() avant de réémettre une nouvelle session     
Enfin, un nouvel identifiant de session est généré en invoquant la méthode request.getSession(true), ce qui garantit que son identifiant de session est recyclé avant l'authentification.

```java
private Boolean authenticate(HttpServletRequest request, String credential, String password) {
    // Prevent Session Fixation (http://en.wikipedia.org/wiki/Session_fixation)
    HttpSession session = request.getSession(false);
    if (session != null) {
        session.invalidate();
    }
    try {
        request.getSession(true);
        if (request.getUserPrincipal() == null) {
            request.login(credential, password);
        }
        return true;
    } catch (ServletException ex) {
        log.log(Level.WARNING, "Error when authenticate", ex);
    }
    return false;
}​
```

## .NET

la méthode SessionIDManager() nous permet d'utiliser l'état de session d'ASP.NET.   
La session existante est ensuite invalidée en appelant la méthode RemoveSessionID()        
Enfin, un nouvel identifiant de session est généré en invoquant la méthode CreateSessionID(), ce qui garantit que son identifiant de session est recyclé avant l'authentification.

```java
// Prevent Session Fixation (http://en.wikipedia.org/wiki/Session_fixation)
SessionIDManager manager = new SessionIDManager();
manager.RemoveSessionID(System.Web.HttpContext.Current);
var newId = manager.CreateSessionID(System.Web.HttpContext.Current);
bool isRedirected = true;
bool isAdded = true;
manager.SaveSessionID(System.Web.HttpContext.Current, newId, out isRedirected, out isAdded);

try{
    UserAccount authenticatedUser = userModule.login(userid, password);
    string redirectURL = UserRoleDispatcher.getPageByUserRole(authenticatedUser.ROLE);
    Response.Redirect(redirectURL);
}
```