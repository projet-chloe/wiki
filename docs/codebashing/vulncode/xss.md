# XSS

## réfléchie

La principale stratégie pour contrer le XSS réfléchi consiste à traiter toutes les entrées de l'utilisateur comme du texte et non comme du code. Cela peut être fait en réalisant les actions suivantes :

1. Échapper les entrées de l'utilisateur en utilisant des outils propres au langage ou au framework, comme les templates (gabarits) ou l'échappement contextuel. En général, ces mécanismes sont activés par défaut, assurez-vous donc de ne pas les désactiver.

2. Connaître tous les endroits où sont utilisées les entrées de l'utilisateur, et s'efforcer d'éviter de renvoyer des entrées non normalisées vers des emplacements potentiellement dangereux comme le corps et les attributs HTML, le javascript, les paramètres GET, les URL, les liens, le CSS.

3. Utiliser des contrôles de sécurité supplémentaires contribuant à empêcher le XSS dans le cas où les contrôles par échappement échouent ou ne sont pas effectués.

- Flag de cookie HTTPOnly. Ce flag empêche Javascript d'accéder au contenu du cookie, ce qui le protège contre le vol en cas de XSS réfléchi.       
- En-tête HTTP Content Security Policy. Cet en-tête restreint les sources de tout le contenu de la page, y compris le code javascript, à la liste blanche des sources, ce qui rend plus difficile l'exploitation par XSS.      
- En-tête HTTP X-XSS-Protection. Cet en-tête empêche les navigateurs de charger une page s'ils détectent une exploitation par XSS réfléchi.    

## Stocké

Pour se défendre contre les attaques par XSS stocké, il est important de s'assurer que les données fournies par l'utilisateur sont encodées avant que l'application ne les transmette aux autres utilisateurs.

L'encodage des informations devant être affichées peut se faire de façon efficace en échappant les données fournies par l'utilisateur immédiatement avant de les envoyer aux utilisateurs de l'application.

Lorsque les données sont correctement échappées avant d'être transmises à l'utilisateur et affichées dans son navigateur, ce dernier ne les interprète pas comme du code mais comme des données, ce qui garantit qu'elles ne seront pas exécutées.

Par exemple, lorsqu'elle est correctement échappée, la chaîne `<script>` est convertie en &lt;script&gt; et simplement rendue comme du texte dans le navigateur de l'utilisateur, plutôt que d'être interprétée comme du code.

### PHP

La méthode la plus efficace pour se protéger des attaques XSS est d'utiliser les fonctions htmlentities() ou htmlspecialchars() de PHP pour afficher des saisies contrôlées par l'utilisateur. Ces balises échapperont et encoderont automatiquement les caractères HTML dans le code HTML rendu, y compris < , > , " , ' et &, empêchant ainsi l'injection de code HTML potentiellement malveillant.

```php
<p><em><strong><?php echo htmlentities($_GET['search']); ?></em></p>
```

### Java

La méthode la plus efficace pour se protéger des attaques XSS est d'utiliser la balise JSTL c:out ou la fonction EL fn:escapeXml() pour afficher des saisies contrôlées par l'utilisateur. Ces balises échapperont et encoderont automatiquement les caractères HTML dans le code HTML de rendu, y compris < , > , " , ' et &, empêchant ainsi l'injection de code HTML potentiellement malveillant.

```java
<td><c:out value="${contact.name}"/></td>
```

### .NET

La méthode la plus efficace pour se protéger des attaques XSS est d'utiliser la méthode HttpUtility.HtmlEncode de Microsoft ou de remplacer la balise <%= %> par la syntaxe d'expression <%: %> pour afficher des saisies contrôlées par les utilisateurs. Ces balises échapperont et encoderont automatiquement les caractères HTML dans le code HTML rendu, y compris < , > , " , ' et &, empêchant ainsi l'insertion de code HTML potentiellement malveillant.

```php
//<p><em><strong><%= ViewData["search"] %></strong></em></p>
<p><em><strong><%= HttpUtility.HtmlEncode(ViewData["search"]) %></strong></em></p>
```

ou 

chaque objet contact est rendu à l'intérieur de balises td en utilisant la syntaxe d'expression <%= %>    
Cette fonctionnalité permet d'afficher le contenu d'un objet contact après avoir évalué l'expression d'objet.     
Malheureusement, la syntaxe <%= %> de .NET n'effectue pas l'échappement des valeurs HTML. De ce fait, les balises <%= %> restitueront directement le contenu de ce champ sans l'échapper ou l'encoder.    

```php
<% foreach (var contact in Contacts) { %>
    <tr>
//      <td><%=contact.name %></td>
//      <td><%=contact.title %></td>
//      <td><%=contact.number %></td>
        <td><%: contact.name %></td>
        <td><%: contact.title %></td>   
        <td><%: contact.number %></td>  
    </tr>
<%} %>
```

## DOM

Pour se protéger contre les attaques script intersites (Cross-Site Scripting) dans un environnement DOM (Document Object Model), il est nécessaire d'adopter une approche de défense en profondeur combinant plusieurs bonnes pratiques de sécurité.

Vous devez vous rappeler que les injections par Stored XSS (XSS Stocké) et Reflected XSS (XSS réfléchi) se déroulent du côté du serveur plutôt que de celui du client. Avec le DOM XSS, l'attaque est injectée dans le DOM du navigateur, ce qui ajoute un niveau de complexité : elle est ainsi très difficile à éviter et très spécifique au contexte (car un pirate peut injecter du HTML, des attributs HTML ou du CSS, ainsi que des URL).

Le principe général à suivre est que l'application doit d'abord effectuer un encodage HTML, puis un encodage JavaScript de toute donnée fournie par l'utilisateur et renvoyée au client.

Du fait du très large champ d'action de ce type d'attaque, les développeurs sont fortement encouragés à passer en revue les parties de code potentiellement vulnérables aux attaques DOM XSS. Cela comprend, sans y être limité, les éléments suivants : window.name, document.referrer, document.URL, document.documentURI, location, location.href, location.search, location.hash, eval, setTimeout, setInterval, document.write, document.writeIn, innerHTML et outerHTML


### Java

Le code javascript `document.location.hash.split('#')[1]` retourne la partie de l'URL TradeNEWS URL correspondant à l'ancre définie sur #guest
Le code `split('#')[1]` extrait la chaîne "guest" et l'affecte à la variable name.

la méthode javascript match() pour appliquer une expression régulière à la variable name, afin d'identifier les caractères non alphanumériques tels que # , < , > , " , ' , & .

```php
<script>
    var name = document.location.hash.split('#')[1]; //https://tradenews.codebashing.com/guests/landing#guest
    if (name.match(/^[a-zA-Z0-9]*$/)){
        document.write("Hello " + name + "! Please login or signup to access news stories");
    }else{
        window.alert("Security error");
    }
</script>
```

