# Insecure TLS validation

Comme pour toute entrée fournie par l'utilisateur, il est important de s'assurer qu'une stratégie de validation des entrées spécifique au contexte est en place. Par exemple, considérons le cas d'une attaque TLS/SSL man-in-the-middle qui pourrait abuser de la logique de validation de certificat mal définie.

SSL2 et SSL3 présentent des vulnérabilités critiques qui permettent à un attaquant de déchiffrer le texte chiffré. TLS 1.0 en lui-même ne présente pas de vulnérabilités critiques mais pour se conformer à certaines normes, ce protocole ne doit pas être utilisé. TLS 1.2 ne présente aucune vulnérabilité connue. Avec TLS 1.3, la prise en charge de certaines caractéristiques obsolètes et non sécurisées a été supprimée.

## JAVA

java.security.cert : package pour valider les certificats    
getServerCertificates() : méthode de la HttpsURLConnectionclasse pour recevoir le certificat du serveur.   
getKeyUsage() : obtient les informations sur la clé contenue dans le certificat, et la prochaine vérification 'if' garantit que le certificat a été signé par une autorité de certification. ils vérifient si ce certificat n'est pas auto-signé.    
checkValidity() : méthode pour véirifer la validité du certificat   
getSubjectAlternativeNames() : méthode pour récupérer les noms alternantifs (SAN) si le certificat en contient.   
tester si la wildcardSANvaleur de chaîne existe dans la liste des SAN du certificat et est valide.   

```java
java.security.cert.Certificate[] certs = conn.getServerCertificates();
for (java.security.cert.Certificate cert : certs) {
    if(cert instanceof X509Certificate) {
        try{
         boolean[] keyUsage = ((X509Certificate) cert).getKeyUsage();
         if(!keyUsage[5]) {
           ((X509Certificate)cert).checkValidity();
           SANs = ((X509Certificate) cert).getSubjectAlternativeNames();
          if (SANs != null) {
           Iterator<X509Certificate> it = SANs.iterator();
           while (it.hasNext()){
             List list = (List) it.next();
             String domain = (String) list.get(1);
             subjectAltList.add(domain);
           }
          } if (!subjectAltList.contains(wildcardSAN)){
           return false;
          }
         }
        } 
        catch (CertificateExpiredException | CertificateNotYetValidException e) {
          logger.log(e.getMessage());
        }
    }
}return true;
```

## PHP

Dans cet exemple curl_init, la fonction initialise une session via HTTPS, mais aucune validation d'un certificat homologue n'est effectuée. Même la date d'expiration du certificat n'est pas vérifiée.

Par conséquent, en tirant parti de ce code mal conçu, un attaquant peut désormais utiliser des certificats expirés, des certificats révoqués, des certificats auto-signés ou aucun certificat du tout et toujours passer par la validation TLS sans être bloqué.


CURLOPT_CERTINFOoption qui demande des informations sur le certificat SSL, l' CURLOPT_SSL_VERIFYHOSToption qui vérifie si l'hôte auquel nous parlons est l'hôte nommé dans le certificat et l' CURLOPT_SSL_VERIFYPEERoption qui s'assure que le certificat lui-même est valide.

```php
$curl = curl_init('https://www.trade-ssl.com/');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_NOBODY, true);
curl_setopt($curl, CURLOPT_CERTINFO, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 2);
curl_setopt($curl, CURLOPT_VERBOSE, 1);
curl_exec($curl);

$info = curl_getinfo($curl);
$wildcard_domain = "*.trade-ssl.com";
$naked_domain = "trade-ssl.com";
$sans = $info["certinfo"][0]["X509v3 Subject Alternative Name"];

$san = explode(",", $sans); //extract the SANs from the certificate
$san_1 = substr($san[0], 4); 
$san_2 = substr($san[1], 4);

//check that the certificate contains the appropriate SANs for the domain
if ($san_1 != $wildcard_domain || $san_2 != $naked_domain){
  //throw an exception for invalid certificate
}
```

## .NET

Pour s'assurer que les certificats expirés ou révoqués ne sont pas traités, les développeurs ont implémenté la ValidateCertificateméthode, qui effectue la validation des certificats TLS au niveau de la couche de transport.

La méthode accepte en outre le SSLPolicyErrorparamètre de classe, qui encapsule les erreurs qui se produisent lors de la validation, qui est ensuite donné comme paramètre d'entrée que le développeur de l'application doit gérer.

Le code effectue ensuite une ifvérification pour tester si sslPolicyErrorsrenvoie une valeur différente de zéro.
Cependant, au lieu de renvoyer false(en cas d'erreurs), le développeur implémente une vérification supplémentaire en vérifiant le nom d'hôte du certificat par rapport à une liste statique qu'il maintient. S'il est présent, la validation TLS est transmise en retournant true.
Un développeur d'application peut faire cela par exemple : pour tenir compte des erreurs de non-concordance de nom de domaine qui peuvent se produire lors de l'utilisation d'adresses IP, ou lorsqu'un développeur a tenté à tort d'implémenter des protections d'épinglage de certificat.

En tirant parti de cette vérification mal conçue, un attaquant peut désormais utiliser un certificat révoqué, un certificat expiré ou aucun certificat du tout et toujours passer par la validation TLS sans être bloqué car cette vérification supplémentaire reviendra toujours true.

si la classe sslPolicyErrors contient des erreurs, telles qu'une chaîne de certificats invalide, un certificat distant manquant, des certificats révoqués, etc., nous invoquons return falsela vérification que seuls les certificats valides sont transmis pour des vérifications supplémentaires


```java
private static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain SslpolicyErrors sslPolicyErrors){
  if (sslPolicyErrors != SslPolicyErrors.None){
    return false;
  } else {         
    HttpWebRequest webRequest = sender as HttpWebRequest;
    if (webRequest == null){
      return false;
    }
    if (HostNames.Contains(webRequest.RequestUri.Host.ToLower())){
      return true;
    }
    return false;
  }
  return false;
}   
```