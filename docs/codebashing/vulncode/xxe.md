# XXE

Les applications Web utilisant des bibliothèques XML sont particulièrement vulnérables aux attaques de traitement d'entité externe (XXE) car les paramètres par défaut de la plupart des analyseurs XML SAX sont d'avoir XXE activé par défaut.

Pour utiliser ces analyseurs en toute sécurité, vous devez explicitement désactiver le référencement des entités externes dans l'implémentation de l'analyseur SAX que vous utilisez.

Étant donné que l'entrée XML fournie par l'utilisateur provient d'une "source non fiable", il est très difficile de valider correctement le document XML pour se prémunir contre ce type d'attaque.

Au lieu de cela, le processeur XML doit être configuré pour utiliser uniquement une définition de type de document (DTD) définie localement et interdire toute DTD en ligne spécifiée dans le ou les documents XML fournis par l'utilisateur.

En raison du fait qu'il existe de nombreux moteurs d'analyse XML disponibles, chacun a son propre mécanisme pour désactiver la DTD en ligne afin d'empêcher XXE. Vous devrez peut-être rechercher dans la documentation de votre analyseur XML comment "désactiver la DTD en ligne" spécifiquement.

## JAVA

La méthode la plus robuste pour se protéger contre les attaques XXE consiste à configurer l'analyseur XML des applications pour qu'il n'autorise pas les DOCTYPE déclarations. Cela se fait en définissant le paramètre parsers disallow-doctype-decl sur true. Avec cet ensemble, une exception se produit si notre fichier xml contient une DOCTYPE déclaration et que l'analyse s'arrête, empêchant la vulnérabilité d'exposer des informations sensibles.    
De même, l'analyseur SAX a également été configuré pour ne pas traiter les external entités. Ces deux options permettent à l'analyseur SAX de charger des entités externes qui, lorsqu'elles sont spécifiées dans notre fichier xml, peuvent être exploitées par un attaquant pour lire des fichiers système arbitraires. S'ils sont définis sur false dans l'analyseur SAX, cela rejetterait automatiquement le référencement d'entités externes.

```java
documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);   
documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);    
documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);    
```

## PHP

En PHP, la méthode la plus robuste pour se protéger contre les attaques XXE est de configurer l'analyseur XML des applications pour qu'il n'autorise pas les DOCTYPE déclarations. Cela se fait en invoquant la libxml_disable_entity_loader(true) méthode.

Avec cette méthode définie, une exception se produit si notre fichier contient une DOCTYPE déclaration et que l'analyse s'arrête, empêchant la vulnérabilité d'exposer des informations sensibles.

Remarque : nous remettons les options LIBXML à l'état précédent avant de revenir.

```php
$oldValue = libxml_disable_entity_loader(true);
public function load($path){
    libxml_disable_entity_loader($oldValue);
    $dom = new DOMDocument();
    $dom->loadXML(file_get_contents($path));
    return $dom;
}
```

## .NET

L'analyseur SAX côté serveur est développé à l'aide de la XmlReader classe .NET, qui fait partie de l'API de base de Microsoft pour le traitement XML, qui permet aux applications d'analyser et de transformer des documents XML.

les développeurs ont configuré l'analyseur SAX à l'aide de la XmlReaderSettings classe, qui permet de spécifier un ensemble de fonctionnalités de configuration pour l' XmlReaderobjet, qui est transmis à la XmlReader.Create méthode

Notez que les développeurs ont défini l' DtdProcessing option sur Parse, ce qui permet essentiellement à l'analyseur SAX de charger des entités externes qui, lorsqu'elles sont spécifiées dans notre fichier, peuvent être utilisées abusivement par un attaquant potentiel pour lire des fichiers système arbitraires. Si cette option était définie sur Prohibit l'analyseur SAX, il rejetterait automatiquement le référencement d'entités externes.

La méthode la plus robuste pour se protéger contre les attaques XXE consiste à configurer l'analyseur XML des applications pour qu'il n'autorise pas les DOCTYPE déclarations. Cela se fait en définissant le paramètre parsers sur Prohibit. Avec cet ensemble, une exception se produit si fichier contient une DOCTYPE déclaration et que l'analyse s'arrête, empêchant la vulnérabilité d'exposer des informations sensibles.


```java
public XmlReader GetXMLReader(string xmlSource){
    StringReader strReader = new StringReader(xmlSource);
    XmlReaderSettings settings = new XmlReaderSettings();

    settings.XmlResolver = new XmlUrlResolverWithCache();
    //settings.DtdProcessing = DtdProcessing.Parse;
    settings.DtdProcessing = DtdProcessing.Prohibit
    return XmlReader.Create(strReader, settings);
}
```

## Exemple XXE

```xml
<!DOCTYPE foo [<!ELEMENT foo ANY >
<!ENTITY bar SYSTEM "file:///etc/passwd" >]>
<?xml version="1.0" encoding="UTF-8"?>
<trades>
    <metadata>
        <name>Apple Inc</name>
        <stock>APPL</stock>
        <trader>
            <foo>&bar;</foo>
            <name>C.K Frode</name>
        </trader>
        <units>1500</units>
        <price>106</price>
    </metadata>
</trades>
```