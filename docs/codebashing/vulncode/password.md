# Password Storage

## mot de passe enregistré en clair

lorsque les mots de passe sont stockés en clair , un attaquant qui réussit à accéder au contenu de la base de données dispose de toutes les paires nom d' utilisateur/mot de passe qui pourraient être utilisées dans d'autres attaques.

Si, pour une raison quelconque, votre fonctionnalité d'authentification nécessite le stockage de mots de passe en clair (par exemple, lorsque les mots de passe sont envoyés aux utilisateurs par e-mail lorsqu'ils les oublient), vous faites quelque chose de mal.

L' enregistrement des mots de passe en texte clair et l'envoi de mots de passe (qui sont stockés dans les boîtes de réception de l'utilisateur en texte clair) sont deux conceptions très dangereuses , car elles garantissent que les données au repos sont en texte clair à plusieurs endroits. Par conséquent, cette conception ne peut pas être sécurisée.

## mot de passe enregistré chiffré

Le deuxième cas qui vient à l'esprit est le chiffrement . L'objectif principal du cryptage est de maintenir la confidentialité des données , donc crypter les mots de passe dans la base de données pour éviter qu'ils ne soient exposés à un pirate astucieux semble être une bonne idée.

Le cryptage pourrait également être une exigence. Considérez un serveur qui est un client pour un autre serveur et stocke donc les informations d'identification qu'il utilise avec un autre serveur. Cela signifie qu'il doit avoir accès à leur version en clair afin qu'il puisse lui-même se connecter à un autre serveur, ce qui signifie que le cryptage est la seule solution.

Les mots de passe peuvent être chiffrés au niveau de la ligne ou de la table, même la base de données entière peut être chiffrée.

Cela aidera-t-il ? Oui, mais seulement dans une certaine mesure.

Considérons, par exemple, le cas où les mots de passe sont chiffrés au niveau de la base de données. Lorsque l'application accède à la base de données, la base de données déchiffre la partie des données à laquelle l'application doit accéder. Le déchiffrement est autorisé car l'application elle-même est autorisée à accéder à la base de données. Si un attaquant accède à la base de données via l'application (via l' attaque par injection SQL ), il recevra tout ce qu'il demande car les requêtes adressées à la base de données proviennent de la source de confiance - l'application.

Beaucoup dépend également de la manière dont la clé de chiffrement est stockée. S'il est stocké dans la base de données, l'attaquant déchiffrerait facilement les mots de passe volés avec cette clé (ou il pourrait voler la clé avec la base de données).

De plus, étant donné que la majorité des utilisateurs n'appliquent pas beaucoup d'imagination lors de la génération de leurs mots de passe, il y aura des mots de passe identiques pour différents utilisateurs, résultant en un texte chiffré identique (lorsqu'il est chiffré avec la même clé) et facilitant ainsi la vie de l'attaquant pour déterminer quels mots de passe sont plus intéressants à casser.

Pour résumer, s'il n'est pas obligatoire que les mots de passe stockés dans la base de données soient cryptés (au cas où le serveur aurait besoin d'avoir accès à des mots de passe en clair dans le but de s'authentifier auprès d'un autre serveur/service), alorsle cryptage peut être utilisé comme contrôle de sécurité supplémentaire . La clé de chiffrement doit être stockée en dehors de la base de données, par exemple, dans un module de sécurité matériel .

## mot passe enregistré haché

Le hachage est un autre moyen populaire de stocker les mots de passe "en toute sécurité". Le hachage est une fonction irréversible , qui reçoit une valeur et génère une valeur condensée de taille fixe ; l'unicité de cette valeur détermine sa robustesse dans un contexte de sécurité, car les collisions dans une fonction de hachage impliqueront que différents mots de passe peuvent produire des valeurs de hachage identiques.

La seule façon de déchiffrer le mot de passe haché est de calculer les hachages de tous les mots de passe possibles et de les comparer au hachage du mot de passe à déchiffrer. Cela peut sembler une tâche difficilement gérable, mais il existe des raccourcis et des solutions de contournement qui permettent aux attaquants de gagner du temps sur le calcul.

Premièrement, les mots de passe sont souvent facilement prévisibles. Les gens construisent des mots de passe en utilisant des mots anglais, des chiffres et des caractères spéciaux (qui remplacent parfois des lettres spécifiques, comme "@" pour "a", "$" pour "s", "0" pour "o"). Deuxièmement, les gens utilisent également des modèles prévisibles pour les mots de passe (par exemple "nom + année de naissance", "mot + 123", etc.). De plus, de nombreuses bases de données contenant de vrais mots de passe ont été divulguées.

Tout cela permet aux attaquants de créer des soi-disant tables arc-en -ciel pour craquer les hachages de mot de passe. La table arc-en-ciel est un dictionnaire qui contient des mots de passe connus (divulgués, construits à l'aide de modèles prévisibles, etc.) et leurs hachages calculés.

Le matériel moderne permet de calculer d'énormes quantités de hachages par seconde, ce qui rend relativement facile la création d'une table arc-en-ciel basée sur des millions de mots de passe connus. Les tables arc-en-ciel sont spécifiques aux fonctions de hachage pour lesquelles elles ont été créées, et pour les algorithmes de hachage non sécurisés (par exemple MD5 ou SHA-1 ), ces tables sont déjà calculées pour les mots de passe jusqu'à 8 caractères.

## mot de passe enregistré haché avec un sel

Pour rendre les tables arc-en-ciel précalculées moins utiles, le sel peut être utilisé. Salt est une valeur générée aléatoirement qui est ajoutée (comme préfixe, suffixe ou les deux) au mot de passe avant le hachage. Les hachages de combinaisons mot de passe + sel , dans ce cas, sont totalement différents des hachages de mots de passe simples. Le sel n'est pas une valeur secrète et doit être stocké à côté du mot de passe haché en clair.

Chaque mot de passe doit être haché avec son propre sel unique . Dans le cas où le même sel est utilisé pour chaque mot de passe dans la base de données, l'attaquant doit calculer une table arc-en-cielune seule fois, pour le sel spécifique utilisé. Lorsque le sel est unique, l'attaquant doit recalculer les tables arc-en-ciel pour chaque paire sel + mot de passe, ce qui augmente considérablement le temps de calcul total et diminue la vitesse de craquage du mot de passe.

Si le sel est court ou pas assez aléatoire, il peut apparaître plusieurs fois dans la base de données contenant un grand nombre d'enregistrements de mots de passe. Chaque répétition de sel pour différents mots de passe diminue la quantité de calcul que l'attaquant doit effectuer.

De plus, l'utilisation de sel empêche les utilisateurs d'avoir les mêmes hachages de mot de passe pour les mêmes mots de passe (car le sel est différent pour chaque mot de passe). Cela empêche l'attaquant d'accéder à plusieurs comptes d'utilisateurs qui réutilisent le même mot de passe.

## meilleure solution

Le seul moyen sûr d'utiliser le hachage dans le stockage des mots de passe est d' augmenter le temps de calcul du hachage lors du calcul des hachages (c'est-à-dire de faire plusieurs itérations de calcul du hachage : calculer le hachage du mot de passe, puis calculer le hachage du mot de passe haché, puis calculer le hachage du hach du mot de passe haché, etc.). Certains algorithmes permettent un temps de calcul évolutif, en calculant des hachages à l'aide d'un nombre d'itérations personnalisable ; ces algorithmes sont appelés fonctions de dérivation de clé (KDF).

populaires KDF : 
1. bcrypt (n'est pas normalisé)
2. [PBKDF2] (https://tools.ietf.org/html/rfc2898)
3. [scrypt] (https://tools.ietf.org/html/rfc7914)
4. [ARGON2] (https://password-hashing.net/)

Correctement implémenté et configuré, KDF protège efficacement les mots de passe stockés contre les attaques ciblant les mots de passe stockés dans la base de données.

La dernière chose à mentionner ici est que la complexité de calcul doit être configurée en gardant l'équilibre entre le temps nécessaire pour se connecter à l'application et le temps de calcul. Le temps de calcul doit être aussi long que possible sans affecter l'expérience utilisateur de l'application et ses performances (si vous pouvez occuper le serveur avec des hachages, vous pourriez probablement provoquer DOS).

## Transport

En ce qui concerne la mise en œuvre sécurisée de la fonctionnalité d'authentification, le développeur doit décider du meilleur format pour transmettre en toute sécurité un mot de passe après qu'il a été soumis dans le navigateur de l'utilisateur. La question principale est -elle doit-elle être hachée sur le client ou sur le serveur ?

En d'autres termes, la valeur de hachage du mot de passe est utilisée comme mot de passe réel pour la connexion. Que se passe-t-il lorsque la base de données est volée ? L'attaquant utilisera des paires username/hashedPassword pour se connecter en tant que n'importe quel utilisateur. Ainsi, hacher les mots de passe sur le client équivaut à les stocker en clair. Familièrement, l'utilisation de hachages pour s'authentifier plutôt que de mots de passe est connue sous le nom d' attaque Pass-the-Hash .

Le mot de passe doit être envoyé sur un canal crypté sous forme de texte en clair. Envoyer un mot de passe haché et le comparer à un hachage stocké du mot de passe équivaut à stocker les mots de passe en clair. L'utilisation de hachages transmis volés pour l'authentification est connue sous le nom d'attaque Pass The Hash.

## Validation du mot de passe

1. Une fois que l'utilisateur a entré le nom d'utilisateur et le mot de passe, le couple nom d' utilisateur/mot de passe en clair c'est transmis en clair sur un canal sécurisé (TLS) au serveur.   
2. Là, le mot de passe est haché à l'aide d'une fonction de dérivation de clé (KDF) par le serveur (et est enregistré dans la base de données sous la forme hachée) si c'est une création de compte par exemple, ou recherche la paire nom d'utilisateur / KDF dans la base de donnée et en focntion de la réponse donne l'accès ou non.

Le mot de passe ne doit pas être stocké ailleurs sous une autre forme. Par exemple, un mot de passe rejeté ne doit pas être enregistré dans le journal car un attaquant peut y accéder et utiliser le mot de passe rejeté pour dériver le mot de passe correct (par exemple, des fautes de frappe évidentes dans des phrases secrètes lisibles).

(Notez que ce processus n'est utile que pour empêcher l'utilisation de hachages de mots de passe divulgués dans les cas où l'attaquant obtient d'une manière ou d'une autre l'accès au stockage des mots de passe. Attaque par énumération d'utilisateurs ou une attaque MITM lors du transfert de données de l'application vers la base de données doit également être prise en compte lors de la création de la fonctionnalité d'authentification.)

