# Déni de service avec upload de fichiers

Étant donné un nombre pratiquement illimité d'utilisateurs avec un temps et une bande passante pratiquement illimités, les ressources de n'importe quel système peuvent éventuellement être épuisées, mais une approche de défense en profondeur peut rendre l'épuisement beaucoup plus difficile. Les contrôles de sécurité suivants doivent former un mécanisme de défense multicouche contre l'épuisement du stockage :

1. Vérification de la taille des fichiers . Le téléchargement de fichiers volumineux peut entraîner un épuisement de l'espace disque. La taille maximale du fichier doit être définie en fonction des besoins de l'entreprise et communiquée aux utilisateurs.

2. Vérification du nom de fichier. Un nom de fichier long peut entraîner des erreurs inattendues ou des problèmes de mémoire. Si la logique métier de l'application le permet, les noms des fichiers téléchargés doivent être limités en longueur, nettoyés à l'aide d'une liste blanche de caractères autorisés ou remplacés par des chaînes aléatoires.

3. Restriction sur la quantité ou la taille totale des fichiers téléchargés par un seul utilisateur. Ce contrôle de sécurité pourrait aider contre une attaque de téléchargement de fichiers distribués (si la création d'un nouvel utilisateur n'est pas triviale et qu'un attaquant ne peut pas créer des milliers d'utilisateurs) et pourrait réduire considérablement le risque d'épuisement de l'espace disque. Assurez-vous d'aligner les restrictions sur les exigences commerciales de l'application et d'éviter de nuire à l'expérience utilisateur des utilisateurs non malveillants.

4.Le téléchargement de fichiers ou la bibliothèque d'analyse doivent être à jour . S'il n'est pas mis à jour régulièrement, il peut contenir une vulnérabilité entraînant un déni de service. Par exemple, lorsqu'un fichier malveillant est chargé à l'aide d'une bibliothèque vulnérable, il peut envoyer la bibliothèque dans une boucle infinie d'analyse du fichier.

5. Surveillance des ressources . Les ressources serveur épuisables ou limitées doivent être surveillées par le support applicatif ou l'équipe d'exploitation : la quantité globale d'espace disque restant, l'espace disque alloué au stockage des fichiers téléchargés, le nombre de connexions actives et durables au serveur Web, etc. Une fois que l'équipe des opérations est informée d'une activité suspecte, elle peut s'impliquer pour empêcher manuellement le DoS.

6. Stockage évolutif, par exemple via un cloud privé. Si le stockage est évolutif, il pourrait s'étendre si nécessaire, aidant à la fois en cas d'épuisement intentionnel et également lors de pics d'activité anormalement élevés.


## JAVA

```java
// application.properties
spring.servlet.multipart.max-file-size=5MB
spring.servlet.multipart.max-request-size=50MB

if (files.size() > 10) {throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Maximum 10 files can be uploaded");}
```

## PHP

```php
$request->validate([
    'files' => 'max:10',
    'files.*' => 'max:5000',
]);
```

## .NET

```java
if (files.Count >= 10) { return BadRequest("Maximum 10 files can be uploaded"); }
...
if (file.Length > 0 && file.Length <= 5000000)
```
