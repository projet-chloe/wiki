# Escalation privilège verticale

Une solution simple consiste à s'assurer que l'utilisateur n'est pas seulement authentifié, mais qu'il a également le rôle correct pour accéder à la fonctionnalité d'administration.

Cependant, plus une demande est volumineuse et complexe, plus grande est la probabilité qu'un seul contrôle d'autorisation soit manqué. Pour cette raison, une approche basée sur des modèles pour garantir l'autorisation dans n'importe quelle application doit être appliquée de manière cohérente.

Pour vous faciliter la vie, de nombreux frameworks fournissent des fonctionnalités intégrées pour vous aider. Par exemple, Java Spring Framework prend en charge le contrôle d'accès basé sur l'expression avec l'hasRole([role])expression.   
De plus, dans la version 3.0, le framework Spring Security a ajouté des expressions de sécurité de méthode qui ont introduit des annotations supplémentaires, y compris @Pre et @Post annotations.

Pour vous faciliter la vie, le framework .NET fournit l' [Authorize] attribut permettant de s'assurer que toute tentative d'accès à l'action sur le contrôleur est effectuée par l'utilisateur authentifié du système. L' [Authorize] attribut prend également en charge des contraintes supplémentaires en spécifiant des utilisateurs ou des rôles spécifiques, par exemple :
```java
[Authorize(Roles = "Admin, Manager")]
public class AdminController : Controller {}
```
Notez que l'[Authorize] attribut peut également être appliqué aux actions du contrôleur pour un contrôle plus granulaire.

## JAVA

```java
@PreAuthorize("hasRole('ROLE_USER')")
public void create(Contact contact);
```
ou 
```java
if (currentUser.isAuthenticated() && currentUser.hasRole("admin"))
```

## PHP

```php
if ($currentUser->isAuthenticated() && $currentuser->hasRole('Admin')) 
```

## .NET

nous utilisons la RolePrincipal.IsInRole méthode de Microsoft .NET qui renvoie true si l'utilisateur actuel a le rôle spécifié, garantissant que l'utilisateur doit d'abord passer les contrôles d'authentification et d'autorisation pour accéder à la page.

```java
if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))    
```