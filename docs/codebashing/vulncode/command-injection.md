# Injection de commande

Le moyen le plus efficace de prévenir les vulnérabilités d' injection de commande est d' éviter d'appeler des commandes du système d'exploitation à partir du code de l'application (peut-être que le même résultat pourrait être obtenu en utilisant une API ou une bibliothèque).   

Si l'exécution des commandes du système d'exploitation ne peut être évitée, n'utilisez pas les entrées fournies par l'utilisateur pour les exécuter.    

S'il n'est pas possible d'éviter l'entrée fournie par l'utilisateur lors de l'appel aux commandes du système d'exploitation, une validation d'entrée forte doit être implémentée. Il doit être basé sur une liste blanche de caractères autorisés . N'utilisez pas de listes noires car elles ont tendance à être contournées par un attaquant expérimenté.   

Veuillez noter que même si vous ne mettez en liste blanche que des symboles alphanumériques et que l'attaquant ne parvient pas à casser la syntaxe de la commande existante et à injecter sa commande malveillante, il pourrait toujours être en mesure d'ajouter un argument à la commande existante qui modifierait le résultat de son exécution .    

Une approche de défense en profondeur recommande également de définir avec soin les autorisations pour l'application et ses composants afin d'empêcher l'exécution des commandes du système d'exploitation.    


## JAVA

```java
if (okName.matches("[A-Za-z0-9/.]+"))
```

ou 

la méthode Java Pattern.matches() permet de contrôler la variable myUiD à l'aide d'une expression régulière, ce qui nous permet de détecter les caractères non alphanumériques comme ; , < , > , " , ' , ou & .

Dès qu'un caractère non alphanumérique est détecté, le test if s'achève sur un échec.

```java
if (!Pattern.matches("[0-9A-Za-z]+", myUiD)) {  
    return false;
}
```

## PHP

```php
if (!preg_match('/[^A-Za-z0-9]/', $filename))
```

ou 

méthode escapeshellcmd() de PHP : échapper la variable $username, ce qui permet d'identifier tous les caractères non alphanumériques, tels que ; , < , > , " , ' ou & .

```php
$cmd = "/usr/bin/statlab -".escapeshellcmd($username);
```

## .NET

```java
if (System.Text.RegularExpressions.Regex.IsMatch(filename, @"^[a-zA-Z0-9.]+$"))
```

ou 

la méthode NET Regex.IsMatch() permet de contrôler la variable username à l'aide d'une expression régulière, ce qui nous permet de détecter les caractères non alphanumériques comme ; , < , > , " , ' , ou & .

```java
string userRegex = @\"^[a-zA-Z0-9]*$\\";
if(!Regex.IsMatch(username, userRegex)) {
    ViewData[\"result\"] = \"Invalid user name\\";
} else {
    var proc = new Process
}
```