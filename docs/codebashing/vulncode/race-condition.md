# Race condition

Une Race Condition est une faiblesse logique extrêmement difficile à détecter. Plus le morceau de code exécuté par plusieurs threads est grand, plus il est difficile de déterminer exactement où quelque chose s'est mal passé.

Il existe différentes façons d'empêcher les Race Conditions , et dans chacune d'elles, l'idée principale est de ne laisser qu'un seul thread à la fois accéder au morceau de code critique.

1. Utilisez des mots-clés fournis par le langage de programmation (par exemple, synchronized )

2. Utilisez des bibliothèques qui garantissent l'atomicité des opérations souhaitées

3. Utilisez des bibliothèques qui fournissent des implémentations de verrouillage avancées (par exemple , la bibliothèque util.java.concurrency )

Essayez d'éviter d'implémenter votre propre verrou car ce n'est pas une chose triviale à faire. En tentant de lutter contre les conditions de concurrence, vous pouvez vous retrouver avec un blocage, c'est-à-dire avec une condition où deux threads attendent infiniment l'un l'autre pour libérer le verrou.

Utilisez des bibliothèques éprouvées et assurez-vous de ne pas introduire de blocages.

## JAVA

Synchronized : Le mot-clé spécifie qu'un seul thread peut accéder au bloc synchronisé à la fois.

Il peut être utilisé au niveau de la méthode ou au niveau du bloc de code. Ici, il est utilisé sur la méthode d'instance, ce qui signifie qu'un thread par instance de la classe peut exécuter la méthode. Verrouiller uniquement les parties critiques du code maintient à la fois la sécurité et les performances à un niveau suffisant.

```java
public synchronized ResponseEntity<Account> exchange( @RequestBody ExchangePoints exchangePoints) {
```

## PHP

La transaction DB s'assure qu'un seul thread accède à la base de données en même temps. Par conséquent, plusieurs threads ne peuvent pas entrer dans une condition de concurrence.

De plus, essayez de garder vos opérations atomiques. Par exemple, lorsque vous mettez à jour la base de données en plusieurs étapes, vous créez une fenêtre de temps entre les mises à jour suffisamment grande pour que la condition de concurrence se produise.

```php
use Illuminate\Support\Facades\DB;

DB::beginTransaction();

DB::commit();

DB::rollBack();
```

## .NET

Monitor.Enter() acquiert un verrou sur l' _exchangeLock objet. Notez que le choix d'un objet de verrouillage est un compromis entre performances et sécurité. Pour maintenir à la fois la sécurité et les performances à un niveau suffisant, il convient de ne verrouiller que les parties critiques du code.

Vous pouvez utiliser tryet finallyconstruire pour vous assurer que le verrou est libéré quel que soit le résultat de l'exécution du code.

```java
try{
    Monitor.Enter(_exchangeLock);
}...
finally { Monitor.Exit(_exchangeLock); }
```