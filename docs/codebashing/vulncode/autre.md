# Credentials in URL

Les applications doivent être configurées pour n'accepter que les paramètres d'identification (p. ex. la combinaison nom d'utilisateur/mot de passe, ou tout autre élément d'authentification) que par le biais d'un form envoyé dans une requête POST. Cette règle s'applique à toute donnée sensible transmise par l'utilisateur.

Transmettre des identifiants de connexion par le biais de requêtes GET n'est jamais une bonne idée, car les URL sont inévitablement stockées à plusieurs endroits sur lesquels le développeur de l'application n'a aucun contrôle. Lorsque les identifiants de connexion se trouvent dans l'URL et que cette dernière est stockée, cela augmente le risque d'une exposition accidentelle de ces identifiants.    
Par exemple, les URL sont fréquemment stockées dans :
1. L'historique du navigateur   
2. Les favoris du navigateur   
3. Les en-têtes de référents lorsque les ressources sont liées    
4. Les fichiers de log des proxies en amont    
5. Les fichiers de log de l'application Web    

Remarquez également que pour renforcer la protection des données sensibles fournies par l'utilisateur, l'application ne devrait que accepter les communications d'un utilisateur identifié que par HTTPS, et jamais par HTTP.

# Leftover debug code

Dans les applications Web, le code de débogage constitue une aide pour tester et modifier les propriétés, les informations de configuration et les fonctions de l'application. Si la fonctionnalité de débogage est laissée dans un système en production, elle risque d'être exploitée par des pirates.

Ne laissez pas dans le code source des instructions de débogage qui pourraient être exécutées. Assurez-vous que le processus de mise en production efface toutes les fonctionnalités et informations de débogage. Supprimez le code de débogage avant de déployer l'application.

D'autre part, le fait de laisser des commentaires écrits par les développeurs peut révéler aux candidats pirates des informations potentiellement utiles sur l'architecture de l'application, sa configuration, les numéros de version, etc. Assurez-vous donc de bien les supprimer eux aussi.

# Composants vulnérables et obsolètes

On peut penser que les serveurs qui fonctionnaient dans un environnement centré sur Microsoft étaient sûrs. Mais des composants non Microsoft pourraient exister dans un écosystème d'entreprise, par exemple, se placer devant la pile d'applications Microsoft et acheminer le trafic avant qu'il n'atteigne les serveurs Web.

De plus, de nombreux appareils IoT (y compris les routeurs) exécutent des distributions Linux embarquées avec des shells Bash et sont donc également vulnérables à ce type d'attaques (et peuvent l'être encore aujourd'hui). Qui peut jamais penser à mettre à jour une ampoule intelligente ?

Pour atténuer ces problèmes, un processus de gestion des correctifs doit être en place pour :

- Supprimez les dépendances inutilisées, les fonctionnalités inutiles, les composants, les fichiers et la documentation.   
- Gardez l'inventaire des versions des composants côté client et côté serveur (par exemple, les frameworks, les bibliothèques, etc.) et leurs dépendances. Surveillez en permanence les sources telles que CVE et NVD pour détecter les vulnérabilités des composants.   
- N'obtenez des composants que de sources officielles via des liens sécurisés. Préférez les packages signés pour réduire le risque d'inclure un composant malveillant modifié.   
- Surveillez les bibliothèques et les composants qui ne sont pas maintenus ou qui ne créent pas de correctifs de sécurité pour les anciennes versions.

# Session exposure 

Il y a une raison de ne pas transmettre les identifiants de session avec une requête GET et de n'utiliser pour cela que des requêtes POST : un utilisateur peut en effet accidentellement partager sa propre session avec d'autres personnes lorsqu'il copie et colle des liens Web dans une autre application.

Si l'utilisateur est actuellement connectée à l'application, n'importe quel personne cliquant sur ce lien sera connecté comme l'utilisateur !

Il y une autre raison pour laquelle la transmission d'identifiants de session avec des requêtes GET n'est pas une bonne idée : les URL peuvent en effet être stockées à plusieurs endroits sur lesquels le développeur de l'application n'a aucun contrôle. Lorsque l'identifiant de session est contenu dans l'URL, et que cette URL est stockée quelque part, cela augmente le risque d'exposition accidentelle de l'identifiant de session, première étape du vol de session.

Les URL sont ainsi fréquemment stockées dans :

1. L'historique du navigateur  
2. Les favoris du navigateur   
3. Les en-têtes de référent   
4. Les fichiers de log des proxies en amont    
5. Les logs de l'application Web    
6. Les points d'accès sans fil    
7. Les routeurs    

Ainsi, assurez-vous de ne pas transmettre les identifiants de session dans des requêtes GET, mais seulement dans des requêtes POST.   
Pour renforcer la protection de l'identifiant de session, l'application ne devrait accepter de communiquer avec un utilisateur identifié que par HTTPS, et jamais par HTTP.

L'ID de session ne doit pas être envoyé dans l'URL car celle-ci peut être divulguée à de nombreux endroits. Il ne doit également pas être envoyé dans un paramètre POST car il risque d'être volé dans le cadre d'une attaque XSS. Le moyen le plus sûr d'envoyer un ID de session est d'utiliser un cookie contenant les flags HttpOnly et Secure.

# User enumeration

Il est aisé de résoudre ce problème en effectuant une modification simple de l'application. Il suffit d'envoyer le meme message que l'application agisse de la meme manière peut importe que cela soit l'identifiant ou le mot de passe qui soit faux, qu'il y est une erreur ou non ... Cele ne permet pas de récuperer des informations.

# Privileged Interface Exposure

Les configurations incorrectes d'une application Web constituent un dangereux vecteur d'attaque, car elles peuvent mettre en péril de la totalité du système. Ce problème peut être dû à l'existence de comptes par défaut, de pages par défaut ou de dossiers non protégés et autorisés, qui peuvent contenir des informations sensibles ou fournir des interfaces d'attaque supplémentaires pouvant être exploitées par un pirate.

En règle générale, assurez-vous toujours de supprimer les contenus périmés tels que des interfaces d'administration, la logique applicative non utilisée et toutes les méthodes applicatives qui ont été activées à des fins de test et de développement.

D'autre part, tous les paquets Web, scripts et manuels installés par défaut doivent être supprimés avant de déployer un système d'entreprise dans son environnement de production.

# Insecure URL redirect

Lorsqu'une application s'appuie sur un paramètre fourni par l'utilisateur pour déterminer la cible d'une redirection, il est important de s'assurer que ce paramètre est correctement validé. Si ce n'est pas le cas, la logique de redirection de l'application peut être détournée afin que des utilisateurs légitimes de l'application soient redirigés à leur insu vers des sites Web malveillants (ce qui constitue la première étape d'une attaque par phishing).

Dans ce contexte, la validation correcte des entrées implique que la cible de redirection fournie par l'utilisateur est pertinente dans le contexte de l'application. L'utilisateur doit également être autorisé à accéder cette cible.

Une solution plus élégante et sûre serait d'utiliser un index pour obtenir une valeur contenue dans un tableau. L'application est ainsi responsable de la recherche de la cible de redirection d'URL en se basant sur une valeur d'index fournie par l'utilisateur. Par essence, cette technique interdit à l'utilisateur de fournir directement une URL.

Alternativement, on peut s'interdire d'utiliser les redirections dans notre application.

```java
if (!String.IsNullOrEmpty(url) && url.Equals("https://sso.codebashing.com"))
```
