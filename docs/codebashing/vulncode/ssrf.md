# SSRF

Une approche robuste de la prévention SSRF consiste à éviter d'utiliser des URL définies par l'utilisateur dans les demandes initiées par le serveur. Tout en-tête HTTP et le corps de la requête sont sous le contrôle total de l'utilisateur et peuvent contenir des données arbitraires (par exemple, du contenu malveillant).

S'il n'y a pas d'option pour éviter la saisie de l'utilisateur dans l' URL demandée , elle doit être correctement validée. L'approche de validation suivante doit être appliquée :

1. Vous devez utiliser une liste blanche de domaines auxquels le serveur est autorisé à accéder. Essayez d'éviter d'utiliser des redirections et des domaines génériques car ils pourraient être utilisés pour contourner la liste blanche.

2. Ne pas mettre sur liste noire domaines ou adresses IP. Il existe plusieurs façons valides de représenter le même nom de domaine ou les mêmes adresses IP, contournant ainsi la liste noire. Par exemple, il existe plusieurs façons de représenter localhost :

http://0/ = http://2130706433/ = http://127.0.1.3 = http://127.127.127.127 = http://[::] : 80/ = http://127.0.0.1/ = http://localhost/ = http://0177.0.0.1/

Certaines bibliothèques d'analyse d'URL peuvent également être amenées à analyser une URL malformée de la manière dont l'attaquant en a besoin.

Un attaquant pourrait utiliser le serveur DNS qui résout un nom de domaine spécifique en tant qu'adresse IP locale. Il existe des services en ligne qui fournissent ce type de DNS.

3. Utilisez une liste blanche de protocoles autorisés , par exemple HTTP ou HTTPS. N'essayez pas de les mettre sur liste noire car vous oublieriez certainement certains protocoles exotiques ou hérités qui pourraient être utilisés pour faire une requête (comme file:// , phar:// , gopher:// , data:// , php: // , dict:// , sftp:// , ldaps:// , ldapi:// , etc.)

4. Ne lancez pas de requêtes du serveur vers les URL du Referer , Host ou de tout autre en-tête HTTP . Les en-têtes HTTP sont sous le contrôle total de l'utilisateur, ce qui signifie qu'ils doivent être nettoyés avant d'être utilisés dans les requêtes.

5. Enfin, vous devez utiliser l'authentification entre tous les composants internes de l'application, les stockages tiers, les microservices, etc. Cela réduit considérablement les dommages qu'un attaquant pourrait infliger en exploitant le SSRF .


## JAVA

The URL for the specified platform ID is retrieved from the database.    
```java
String url = platformService.getPlatform(platformRegistrationParametersInSecure.getPlatformId()).getUrl();
```

## PHP

```php
 $url = $platformService->getPlatform($request->platformId);
 ```

 ## .NET

 ```java
//var request = new HttpRequestMessage(HttpMethod.Post, platformRegistrationParametersIn.URL);
var URL = _platformRepository.GetPlatform(platformRegistrationParametersInSecure.PlatformId).URL;
var request = new HttpRequestMessage(HttpMethod.Post, URL);
```