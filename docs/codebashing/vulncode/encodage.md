# Encodage s Hachage vs Chiffrement

## Encodage 

L'encodage est une technique permettant de transformer des données d'une forme à une autre, par exemple du binaire à l'hexadécimal, de l'hexadécimal à l'ASCII, de l'ASCII à la base64, etc.

Les données doivent être présentées sous une forme normalisée afin qu'elles puissent être consommées par différents systèmes. Ces systèmes communiquent entre eux et, afin de maintenir un échange de données fluide, ils doivent utiliser le codage pour transformer les données du format utilisé dans le système source au format utilisé dans le système cible. On pourrait également encoder des données car certains caractères ou octets peuvent casser les méta-caractères du conteneur de transport.

La transformation d'un format à un autre est effectuée à l'aide d'un schéma accessible au public - l'algorithme utilisé pour coder/décoder les données.

exemple encodage : UTF-8, base64, hexadécimal, unicode et binaire

### Comment est-ce utilisé?

L'encodage est utilisé pour représenter des données dans un format qui convient le mieux au système qui traite ces données. L'encodage peut également être utilisé pour compresser des données en transit ou en stockage, par exemple pour réduire la taille de fichiers audio ou vidéo à l'aide de certains logiciels de conversion audio/vidéo pour l'encodage et le décodage.

### Comment est-il mal utilisé ?

Le codage est parfois utilisé à la place du cryptage afin de protéger les données contre l'exposition. Le principal problème avec cela est que l'encodage est facilement réversible : étant donné que son schéma est disponible, il peut être décodé dans son état d'origine par tout tiers qui y a accès, sans aucune information supplémentaire requise.

Il ne faut jamais utiliser l'encodage pour garder les données confidentielles.

## Hachage

Le hachage est une transformation unidirectionnelle de données arbitraires en une valeur de longueur fixe (généralement appelée message ). Les fonctions de hachage peuvent être cryptographiques et non cryptographiques. Les fonctions non cryptographiques ne doivent pas être utilisées pour des opérations liées à la sécurité. 

Les algorithmes de hachage cryptographique ont plusieurs propriétés :   
1. Résistance à la préimage : cela signifie qu'il est impossible de dériver le message d'origine à partir de son hachage.    
2. Imprévisibilité : même le moindre changement à l'entrée de la fonction de hachage entraîne une sortie complètement différente. La sortie de la fonction de hachage doit être imprévisible.    
3. Résistance aux collisions : cela signifie qu'il est impossible de trouver deux messages distincts qui aboutissent au même hachage.

De plus, les fonctions de hachage ont les propriétés suivantes :   
- La même entrée correspond toujours à la même sortie.   
- Le calcul est rapide.

### Comment est-ce utilisé?

Du point de vue de la sécurité, le hachage est utilisé pour assurer l' intégrité des données , c'est-à-dire pour s'assurer qu'elles n'ont pas été modifiées entre les deux points arbitraires dans le temps, et si elles ont été modifiées que cette modification devient visible immédiatement après que vous l'avez hachée et comparée le hachage résultant avec le hachage d'origine. Par exemple, le hachage est un composant essentiel des signatures numériques , des certificats TLS ou des codes d'authentification de message.

L'exemple le plus simple de contrôle d'intégrité utilisant le hachage consiste à comparer les hachages d'un fichier stocké sur le serveur et d'un fichier téléchargé. Si ces hachages correspondent, le fichier n'a pas été falsifié pendant la transmission.

### Comment est-il mal utilisé ?

Le hachage est mal utilisé lorsque des hachages faibles qui ne sont bons que pour les contrôles d'intégrité sont utilisés pour la confidentialité (dans ce cas, l'attaquant peut déchiffrer les données en utilisant des tables arc-en-ciel - raimbow table). Un autre cas d'utilisation abusive est lorsque le hachage est utilisé pour le stockage du mot de passe mais se produit sur le client au lieu du serveur, ce qui permet des attaques pass-the-hash .

## Chiffrement 

Le cryptage est la transformation de données (communément appelées texte en clair, plaintext, cleartext) dans un format qui ne permet pas d'en tirer la signification initiale (appelé ciphertext ) de sorte que même si un attaquant y accède, il ne pourra pas pouvoir le lire. Pour récupérer le texte en clair d'origine, il doit être déchiffré . Le chiffrement et le déchiffrement sont effectués à l'aide de clés cryptographiques .

Il existe deux catégories d'algorithmes de chiffrement couramment utilisés : symétriques et asymétriques . Chaque type a ses avantages et ses inconvénients et est utilisé à des fins différentes.

- Algorithmes symétriques : utilise la même clé secrète pour le chiffrement et le déchiffrement. Seule la personne qui détient la clé secrète utilisée pour le chiffrement peut déchiffrer le message chiffré.

- Algorithmes asymétriques : utilise une paire de clés privées et publiques pour le chiffrement et le déchiffrement. Un message est chiffré à l'aide de la clé publique et déchiffré à l'aide de la clé privée.

### Comment est-ce utilisé?

L' objectif principal du cryptage est de maintenir la confidentialité des données. On aura toujours besoin d'une clé pour déchiffrer le message chiffré.

Les algorithmes symétriques sont largement utilisés pour le chiffrement d'une quantité arbitraire de données. Les algorithmes asymétriques , à leur tour, sont généralement utilisés pour la distribution de clés secrètes et les signatures numériques.

Le chiffrement symétrique peut être utilisé par deux terminaux qui partagent le même secret. Le chiffrement asymétrique peut être utilisé par un point de terminaison avec plusieurs clients qui font confiance au point de terminaison mais ne veulent pas que quelqu'un l'écoute.

Chiffrement asymétriqueest également utilisé pour l'authentification implicite - par exemple, si quelqu'un peut fournir un jeton que mon application peut déchiffrer avec la clé publique, alors il doit avoir été chiffré avec la clé privée, qui ne peut appartenir qu'à des points de terminaison de confiance (c'est essentiellement ainsi que fonctionne l' authentification Kerberos , par exemple).

### Comment est-il mal utilisé ?

Le cryptage doit être utilisé si la confidentialité des données est l'objectif. Ni le hachage ni le codage ne fournissent les capacités nécessaires pour garder les données secrètes. Le chiffrement est une opération qui prend du temps (bien que les algorithmes symétriques aient des performances nettement meilleures que les algorithmes asymétriques) et il ne doit pas être utilisé s'il n'est pas nécessaire d'assurer la confidentialité des données ou l'intégrité des jetons.