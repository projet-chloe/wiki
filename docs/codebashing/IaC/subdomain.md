# Reprise de sous domaine

## Intro

Le rachat de sous-domaine consiste à enregistrer un nom de domaine qui n'existe pas ou n'est pas utilisé afin de contrôler un autre nom de domaine.

Les attaquants ont trois façons d'effectuer Subdomain Takeover :

- Deuxième ordre     
Un lien brisé qui sert le contenu d'un sous-domaine pourrait être détourné

- Domaine expiré     
Le sous-domaine pointe vers un domaine expiré.

- Tierce partie     
Le sous-domaine pointe vers un service tiers supprimé ou abandonné.

## Risques potentiels

Une prise de contrôle de sous-domaine réussie peut permettre à l'adversaire de poursuivre son attaque contre l'application de plusieurs manières.

- Détournement de session : Voler des cookies à large portée

- CSRF : Effectuer une action non désirée sur un site de confiance lorsque l'utilisateur est authentifié.

- Défiguration : Changer les sous-domaines des victimes en une page malveillante

- Usurpation d'identité : Courriels de phishing, pages de connexion malveillantes, utilisation du sous-domaine des victimes.

## Risque dans le bucket S3

Amazon S3 stocke différents types de données : sauvegardes, archives, sites Web, etc.

S'il existe un enregistrement DNS actif pour un compartiment supprimé, un attaquant peut créer le compartiment avec le même nom. Mais cette fois, l'enregistrement DNS pointe vers le bucket sous le contrôle de l'attaquant.

## Utiliser IAC

Les enregistrements DNS sont souvent gérés par une application différente, de sorte que l'action de suppression d'un compartiment est doublée.

Cela provoque des problèmes si vous oubliez de mettre à jour les enregistrements DNS.

Pour éviter cela, vous pouvez les centraliser dans une seule application (par exemple, en utilisant Route 53 pour contrôler les enregistrements DNS des services basés sur AWS).

L'utilisation d'IaC peut aider à réduire le risque d'abandon des enregistrements DNS, lorsque toute la configuration, y compris les enregistrements DNS, est gérée via IaC.

## Prévention et remédiation

L'objectif des développeurs devrait être d'empêcher la prise de contrôle de se produire. Cependant, la correction doit être gérée par l'équipe de sécurité.

Prevention - Developers :     
- Les enregistrements DNS doivent être surveillés et vérifiés périodiquement.     
- Aucun des services ne doit utiliser de domaines expirés.     
- Les domaines doivent être associés aux adresses IP sous le contrôle de l'entreprise.     

Remediation - Security :    
- Mettre à jour l'enregistrement CNAME     
- Évaluer les dommages causés     