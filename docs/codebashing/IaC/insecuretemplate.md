# templates d'attaque et d'insécurité de la chaîne d'approvisionnement

## Intro

Le développement de logiciels contemporains repose souvent sur la réutilisation de code existant.

Les développeurs évitent d'écrire du code qui résout des tâches connues car il est très probable que quelqu'un ait déjà écrit et testé du code qui sert le même objectif, et l'ait publié pour que tous puissent l'utiliser.

Ce code peut être un modèle, une image de conteneur ou un plugin.

En utilisant cette approche, les développeurs peuvent gagner du temps et se concentrer sur les problèmes principaux. Cependant, cela peut également entraîner des problèmes de sécurité, car les développeurs vérifient rarement le code qu'ils importent. En utilisant cette approche, les développeurs peuvent gagner du temps et se concentrer sur les problèmes principaux. Cependant, cela peut également entraîner des problèmes de sécurité, car les développeurs vérifient rarement le code qu'ils importent. 

## Elements non sécurisés

Examinons trois composants qui peuvent être la cible d'attaques de la chaîne d'approvisionnement : 

### les templates

Les templates contiennent souvent des paramètres par défaut qui peuvent entraîner des failles de sécurité. Il y a différentes raisons pour lesquelles cela peut se produire.     
Dans certains cas, les auteurs de templates peuvent s'attendre à ce que les utilisateurs maîtrisent l'IaC et sachent exactement comment le configurer. Dans d'autres cas, ils peuvent simplement laisser des paramètres de sécurité inutiles dans un état non sécurisé, ou commettre eux-mêmes des erreurs.     
Par exemple, un modèle peut permettre un accès libre, puisque le créateur du modèle ne peut pas prédire l'adresse IP de l'utilisateur du modèle.

Les utilisateurs de modèles ne prêtent pas toujours suffisamment attention à toutes les configurations, car ils ont tendance à utiliser un modèle fonctionnel qui nécessite une interaction minimale, en s'en tenant à l'approche "si ce n'est pas cassé, ne le réparez pas". 

### les plug-ins

Les outils IaC permettent souvent d'installer des plugins tiers.      
Les plugins pour les fonctionnalités les plus populaires des fournisseurs de cloud sont généralement écrits par les créateurs du fournisseur de cloud ou de l'outil IaC, on peut donc supposer qu'ils sont sûrs.      
Cependant, les clients peuvent installer des plugins de n'importe quelle source. Les plugins écrits par la communauté peuvent contenir des erreurs ou des configurations erronées involontaires qui peuvent entraîner des problèmes de sécurité.     
Terraform, par exemple, compte plus de 1 000 fournisseurs communautaires qui ne sont pas vérifiés, et plusieurs milliers de modules dont le code réside dans les comptes des utilisateurs privés sur GitHub. 

Les modules sont écrits par la communauté et sont distribués sans contrôle de sécurité approprié. Par conséquent, l'utilisation de tels plugins peut laisser le code IaC exposé ; un plugin peut contenir une erreur ou une mauvaise configuration involontaire.

### les images de conteneur

Les conteneurs sont un moyen rapide de déployer un environnement spécifique. Les bibliothèques d'images de conteneurs stockent des millions d'images prêtes à l'emploi. Les plus populaires sont généralement fournies et maintenues par les entreprises qui créent le logiciel contenu dans un conteneur.    
La bibliothèque de conteneurs vérifie certains conteneurs comme les bases de données open-source, les environnements de langage de programmation, les systèmes d'exploitation, etc. Néanmoins, un grand pourcentage des conteneurs les plus téléchargés n'est pas maintenu et mis à jour. 

Un conteneur contenant un logiciel vulnérable obsolète est une cible facile pour les attaquants qui ont réussi à obtenir l'exploit correspondant.

## Attaque de la chaine d'approvisionnement

Une attaque de chaîne d'approvisionnement cherche à implanter un composant malveillant en amont d'une chaîne d'approvisionnement à partir de sa cible.

### Causes :

Si l'utilisation de modèles existants permet de gagner beaucoup de temps, elle peut introduire des failles de sécurité, que ce soit intentionnel de la part de l'éditeur ou dû à des paramètres par défaut.

- intentionnel :      
Les attaquants distribuent délibérément des logiciels très utilisés et connus pour être vulnérables. De plus en plus de bibliothèques de logiciels open source populaires finissent par comporter des portes dérobées ou des scripts malveillants.

- paramètres :     
L'utilisation de modèles peut entraîner des erreurs et des configurations non sécurisées, qui sont souvent publiées en ligne et réutilisées par d'autres. 

## Remédiation

### Template

- Obtenez vos modèles auprès d'une source fiable     
- Analysez l'ensemble du modèle et comprenez chaque élément de configuration    
- Ne copiez-collez pas le modèle sans comprendre clairement ce qu'il fait     
- Mettez les paramètres dans un état sécurisé     

### Plugins

- Utilisez des plugins officiels marqués comme vérifiés - Préférez les plugins publiés par les fournisseurs de cloud.     
- Si vous devez utiliser un plugin écrit par la communauté, examinez son code.   Vous pouvez bifurquer le code et l'exécuter à partir de votre dépôt, évitant ainsi toute modification future malveillante.     

### Container

- Obtenez les images de conteneurs uniquement auprès de sources fiables, par exemple directement sur les sites Web des mainteneurs de logiciels ou dans des bibliothèques d'images centralisées et vérifiées.     
- Veillez à utiliser une version qui ne contient pas de vulnérabilités connues. Soit la dernière version : bien que les mises à jour continues de la dernière version nécessitent un énorme effort de test. Soit une version stable qui n'a pas de vulnérabilités connues, cela nécessite des mises à jour moins fréquentes, ce qui signifie consacrer moins de temps aux tests.