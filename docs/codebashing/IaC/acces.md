# Gestion des accès

## Intro

La gestion des accès accorde aux utilisateurs autorisés un accès spécifique aux ressources, services, réseaux et applications. Il empêche également l'accès aux utilisateurs non autorisés ou aux parties externes.

Les environnements basés sur le cloud incluent souvent de nombreux composants réseau qui diffèrent par leurs objectifs commerciaux et leurs exigences de sécurité. Par conséquent, les autorisations d'accès ont un impact significatif sur la sécurité et doivent être planifiées et gérées de manière approfondie.

Les principaux sujets de préoccupation concernant la gestion des accès dans les environnements cloud natifs sont :

- Gestion des accès réseau     
- Gestion des accès au stockage objet    
- Utilisation d'IaC avec un utilisateur à privilèges élevés   

## Gestion accès au réseau

Les réseaux basés sur le cloud sont généralement divisés en plusieurs segments en fonction de leur utilisation et des exigences de sécurité.

Dans le cadre de l'approche de défense en profondeur, chaque segment dispose d'autorisations d'accès différentes, ce qui limite le nombre de cibles qu'un attaquant peut atteindre à partir d'un composant compromis. 

### AWS

AWS propose deux types de configurations d'accès réseau : les groupes de sécurité et les listes de contrôle d'accès réseau (NACL). Les deux sont complémentaires et visent à assurer la sécurité des différentes couches.

1. Groupes de sécurité    
Attribué à : Instances EC2 (Amazon Elastic Compute Cloud)     

Types de règles et ordre de traitement :    
Les groupes de sécurité prennent en charge uniquement les règles d'autorisation. Tout accès qui n'est pas explicitement défini est refusé par défaut.   

Toutes les règles sont évaluées avant d'autoriser le trafic. Par conséquent, l'ordre des règles n'a pas d'importance. 

2. listes de contrôle d'accès réseau    
Affectés à : Sous-réseaux VPC (Virtual Private Cloud), où résident les instances.      

Types de règles et ordre de traitement :     
Les listes de contrôle d'accès au réseau prennent en charge les règles "autoriser" et "refuser".    

Ordre de priorité :
Suivant la même approche de configuration que la plupart des pare-feu, les règles NACL sont traitées de haut en bas. La première règle applicable détermine le résultat de l'autorisation ou du refus et les règles suivantes sont ignorées.

## Ordre de priorité des régles

Lorsque le même ensemble de règles NACL est ordonné différemment, cela conduit à des décisions d'accès différentes.

Les règles s'applique dans l'ordre dont elles sont définies, ainsi, la première sera prioritaire ...

Pour atténuer ce problème et éviter une exposition involontaire, placez toujours les règles « Refuser » avant les règles « Autoriser ». 

## Gestion des accès au stockage des objects

Les services de stockage d'objets sont des solutions permettant de stocker et de récupérer des données du cloud vers des API, des sites Web et des utilisateurs finaux.

Ces données stockées peuvent inclure des informations sensibles, telles que des données spécifiques aux clients, aux employés ou à l'entreprise. Par conséquent, les autorisations d'accès aux données stockées doivent être gérées avec soin pour éviter toute perte, utilisation abusive, modification ou accès non autorisé à celles-ci.

### AWS

AWS dispose de plusieurs paramètres pour la gestion du stockage d'objets :    

#### ACL S3 

Les listes de contrôle d'accès (ACL) d'Amazon S3 sont utilisées pour gérer l'accès aux buckets et aux objets. Chaque buckets et objet a une ACL attachée, qui définit quels utilisateurs ou groupes AWS peuvent y accéder et le type d'accès accordé.    

Les principaux types d'autorisations dans les ACL S3 sont :   
- privé : l'ACL par défaut. Elle accorde des autorisations de contrôle complet au propriétaire de la ressource.    
- public en lecture : En plus des privilèges du propriétaire, tous les autres utilisateurs peuvent lire les données dans le stockage de l'objet.    
- public en lecture et écriture : permet à tout le monde de lire et d'écrire dans le stockage d'objets.   

#### stratégies de bucket 

Les politiques de bucket accordent des autorisations d'accès à un bucket et à ses objets. Par exemple, la politique du bucket peut accorder à un utilisateur spécifique l'autorisation de télécharger un fichier vers l'objet de stockage.     

Une politique de bucket ne peut être gérée que par le propriétaire du bucket concerné.  

Note : Par défaut, les changements d'autorisation de bucket n'affectent que les objets ajoutés par le propriétaire du bucket. Elles ne s'appliquent pas aux objets téléchargés et détenus par d'autres comptes AWS.    

Pour modifier ce comportement, utilisez le paramètre de propriété des objets ou modifiez directement les permissions des objets existants. 

#### stratégies IAM 

La gestion des identités et des accès, ou IAM, spécifie quels utilisateurs et quels groupes peuvent accéder à des services et à des ressources spécifiques, ainsi que les types d'actions qu'ils peuvent effectuer.  

Les politiques IAM sont attachées aux utilisateurs, groupes et rôles IAM.  

Contrairement aux autres paramètres du bucket, ces politiques sont configurées au niveau de l'identité plutôt qu'au niveau des ressources.   

Notez que le principe du moindre privilège doit être respecté lors de l'octroi d'un accès à l'aide de ces politiques. 

#### fonctionnalité de blocage de l'accès public 

La fonction Bloquer l'accès public bloque par défaut tout accès public à l'objet de stockage. 

Il est possible de conserver le paramètre par défaut qui bloque complètement l'accès public ou de choisir l'une des options suivantes :    
- Bloquer l'accès public accordé par les nouvelles ACL   
- Bloquer l'accès public accordé par toute ACL (nouvelle ou existante)      
- Bloquer l'accès public accordé par les nouvelles politiques de bucket public    
- Bloquer l'accès public et l'accès inter-comptes accordés par de toutes les politiques de bucket public    

#### CORS

Configuration du partage des ressources entre origines (CORS - Cross-origin resource sharing) autorise et refuse les requêtes HTTP vers l'objet de stockage provenant d'un code côté client qui s'exécute dans un domaine différent de celui de l'objet de stockage.    
Notez que les ACL S3 et les politiques de bucket s'appliquent même lorsque le CORS est activé. Si la configuration CORS est plus permissive que les politiques de bucket, la politique de bucket la plus restrictive est appliquée. 

#### Cas de conflit

En cas de conflit entre différents paramètres, certains sont plus puissants que d'autres.

politique IAM > Block Public Access feature > politique de bucket > ACL S3

### Sécurité 

#### Intro

La gestion des accès pour le stockage d'objets peut être complexe. Différents paramètres signifient différentes choses et peuvent se chevaucher ou entrer en conflit dans certains cas.

Une mauvaise configuration des paramètres de gestion des accès peut entraîner des problèmes de sécurité. Par conséquent, il est important de comprendre les différentes nuances et de faire preuve d'une extrême prudence.

#### Configuration d'accès mal alignés

Pour rendre un bucket ou un objet 'public' , il doit disposer à la fois d'une ACL autorisant l'accès public et d'un paramètre Bloquer l'accès public désactivé.

Pour rendre un bucket ou un objet 'private' , il suffit qu'un seul de ces paramètres soit défini sur private.

Cependant, définir un seul d'entre eux sur privé peut entraîner des erreurs difficiles à détecter.

Pour éviter des configurations d'accès mal alignées lors de la combinaison d'ACL avec le paramètre Bloquer l'accès public, il y a deux choses importantes dont vous devez vous souvenir :    

- Le moyen le plus sûr de définir des ACL est de passer par IaC. Cette méthode est moins sujette aux erreurs que l'utilisation de la console web ou du CLI. En effet, tous les paramètres de contrôle d'accès sont visibles dans la configuration IaC, ce qui réduit le risque d'oublier de supprimer une ACL d'un objet ou d'un bucket lorsqu'elle n'est plus nécessaire.     

- Pour s'assurer qu'un objet est privé, il ne suffit pas d'activer le paramètre Block Public Access. L'ACL doit également être définie comme privée.

#### Utilisation du MFA

L'authentification multifacteur (MFA) n'accorde l'accès à un site Web ou à une application qu'après avoir présenté deux éléments de preuve ou plus à un mécanisme d'authentification, offrant ainsi une autre couche de protection contre les accès non autorisés, en plus de la vérification des identifiants de connexion.

Le fait de ne pas protéger les actions critiques dans le système avec MFA peut conduire à une exploitation par un attaquant si un compte ayant accès à l'action pertinente est compromis. 

Pour éviter de tels cas, suivez ces directives :    
- Activez le MFA pour les actions critiques.    
- Notez que les outils IaC courants peuvent ne pas prendre en charge la fonction MFA. Dans ce cas, définissez-la manuellement ou par script.

#### Autoriser HTTP GET

Dans AWS, les stratégies de bucket sont superieur aux paramètres ACL.
Même si l'ACL d'un objet est définie sur privé, il peut toujours y avoir une stratégie qui autorise les requêtes GET des utilisateurs.

Bien que les requêtes GET semblent inoffensives, un attaquant peut les utiliser de manière malveillante pour récupérer des informations restreintes.

Pour éviter de tels cas, suivez ces directives :     
- Ne pas autoriser les requêtes HTTP GET vers des buckets qui devraient être privés.     
- Si les requêtes HTTP GET sont requises pour une raison quelconque, autorisez-les à être envoyées uniquement par des rôles ou des groupes de sécurité spécifiques.     

#### Autoriser HTTP PUT

Dans AWS, les requêtes HTTP PUT sont utilisées pour charger des fichiers dans le stockage d'objets. Si quelqu'un télécharge un fichier avec le nom d'un fichier existant, il supprime le fichier d'origine et le remplace par le nouveau.

Cela expose le stockage à la corruption malveillante des données et même à la prise de contrôle du système. 

Pour éviter de tels cas, suivez les directives suivantes :     
- N'autorisez pas les requêtes HTTP PUT vers des buckets qui devraient être privés ou en lecture seule.     
- Si les requêtes HTTP PUT sont requises pour une raison quelconque, autorisez-les à être envoyées uniquement par des rôles ou des groupes de sécurité spécifiques.    

## Using IaC With High-privileged User

L'administration de l'infrastructure comprend des tâches à grande échelle qui peuvent être effectuées à l'aide des outils IaC ou de l'interface du fournisseur de cloud. Ces actions, telles que la création de nouvelles instances ou objets et la gestion de leur accès, nécessitent des privilèges étendus.    
Par conséquent, les administrateurs d'infrastructure utilisent souvent des autorisations excessives ou des comptes root pour effectuer des tâches quotidiennes. 

L'utilisation d'un compte à privilèges élevés pour effectuer des tâches quotidiennes enfreint le principe du moindre privilège et peut entraîner des problèmes de sécurité. 

Utilisez le compte root uniquement pour les tâches qui nécessitent des informations d'identification root.    
Suivez le principe du moindre privilège : créez un compte distinct avec un ensemble spécifique de privilèges en fonction des changements de configuration requis. 