# Mise en réseau dans le cloud

## intro

La mise en réseau cloud est une infrastructure informatique qui héberge les fonctions et les ressources réseau de l'organisation. Il peut être géré en interne ou par des fournisseurs de services cloud externes.

La mise en réseau dans le cloud est une évolution de la mise en réseau traditionnelle avec du matériel sur site.

Ces approches de mise en réseau partagent principalement des principes et des meilleures pratiques. Il existe cependant quelques différences entre eux.

1. Il n'est pas nécessaire de connaître les fournisseurs d'appareils de mise en réseau spécifiques ou de maîtriser les logiciels de gestion de réseau.    
2. Les réseaux, VPC, VPN et autres sont créés facilement en quelques clics dans l'interface web du fournisseur de cloud ou en quelques lignes de configuration IaC.     
3. Les administrateurs système et les ingénieurs réseau sont moins impliqués, la responsabilité étant transférée vers les développeurs ou les DevOps.    

## Mise en plac d'un réseaux sécurisé 

Généralement, l'objectif principal de la création d'une solution d'architecture de réseau et de serveur est de fournir une haute disponibilité. Cependant, les problèmes de sécurité doivent également être pris en compte de manière approfondie.

La mise en place d'un réseau sécurisé peut sembler facile. Les composants réseau peuvent souvent être configurés en quelques clics seulement, ce qui peut donner la fausse impression que le fournisseur de cloud l'a sécurisé et infaillible par défaut. Ce n'est pas le cas.

La conception d'un réseau évolutif et sécurisé nécessite de l'expérience, des connaissances et une compréhension approfondie des architectures réseau.

Lors de la mise en place d'un réseau sécurisé, il y a quelques principes qui doivent être suivis : 

- Travaillez avec un ingénieur réseau : Les développeurs et les DevOps n'ont généralement pas beaucoup d'expérience pratique dans la construction de réseaux complexes. Collaborer avec un professionnel de la sécurité réseau pour planifier l'architecture du réseau permet de construire un réseau évolutif et sécurisé avec de nombreux segments, serveurs et routes. 

- S'en tenir à l'approche "Deny-by-Default" (refus par défaut) : Deny-by-Default signifie que tout accès entre les segments du réseau est initialement bloqué, et que chaque règle d'accès est déclarée explicitement. Cette approche élimine la possibilité d'un accès non comptabilisé et involontaire laissé par erreur.    

- Gérer l'accès sortant : Il est essentiel de limiter les connexions sortantes, car les attaquants peuvent les utiliser pour exfiltrer des données et pénétrer plus profondément dans le réseau de l'entreprise. Toutefois, les systèmes d'exploitation utilisent aussi régulièrement des connexions sortantes, de sorte que leur limitation doit être effectuée avec soin. Par exemple, en mettant en place un serveur de mise à jour interne, les systèmes d'exploitation n'auront pas besoin d'accéder à des emplacements publics.    

- Testez tout cela : Un réseau entièrement configuré peut être très complexe, et il est difficile d'anticiper totalement la manière dont ses paramètres affecteront la fonctionnalité des différents composants. Par conséquent, chaque configuration et chaque modification doit être testée de manière approfondie.    

- Définissez le flux de données à l'intérieur du réseau : Cartographiez les clients, les serveurs et les flux de données entre eux. Utilisez cette cartographie pour planifier la division et la structure des segments du réseau.  La conception d'un réseau qui reflète tous les besoins de l'entreprise et de la sécurité dès le départ est beaucoup moins coûteuse et plus efficace que la modification d'un réseau existant.     

- Donnez un nom cohérent : Donner des noms exacts, des descriptions détaillées et des étiquettes à chaque composant du réseau peut éviter de nombreux problèmes à l'avenir. Il peut être difficile de suivre l'évolution constante du réseau. Lorsque les administrateurs doivent prendre des décisions concernant des modifications du réseau (comme l'ajout d'un hôte ou d'une route), l'attribution de noms incohérents aux composants du réseau et des descriptions incomplètes peuvent entraîner des erreurs.   

- Ne conservez pas les paramètres par défaut : Les paramètres par défaut ne sont souvent pas assez sécurisés (par exemple, autoriser le trafic dans toutes les directions). La définition explicite de tous les paramètres de sécurité et la mise à jour des valeurs par défaut permettent d'éviter les failles de sécurité involontaires. Cela est particulièrement important pour les systèmes qui utilisent les valeurs par défaut lorsque les paramètres ne sont pas explicitement définis.    

- Séparez les réseaux de confiance des réseaux non fiables : Répartissez les serveurs ayant des exigences de sécurité différentes dans des segments de réseau différents. Les serveurs tournés vers Internet doivent résider dans un segment de réseau distinct (souvent appelé DMZ ou réseau périphérique). Les autres groupes de serveurs (par exemple, les bases de données) doivent avoir leurs propres segments de réseau et n'être joignables qu'à partir d'emplacements spécifiques nécessitant un accès.     

- Mettre en œuvre la segmentation du réseau : La segmentation du réseau divise un réseau en plusieurs segments, chacun agissant comme un plus petit réseau indépendant. La division se fait en fonction de l'utilisation et des exigences de sécurité de chaque composant du réseau.   Cette segmentation fait partie de l'approche de défense en profondeur et limite le nombre de cibles qu'un attaquant peut atteindre à partir d'un serveur compromis.    

## Gestion de l'accès au port

Les ports permettent d'accéder à différents types de systèmes sur le réseau. Bien qu'ils doivent être ouverts pour permettre la communication réseau, un attaquant peut les exploiter pour accéder à des données sensibles et même prendre le contrôle des systèmes.

Par conséquent, lors de la construction d'un réseau sécurisé, vous devez vous assurer que seuls les ports requis sont effectivement ouverts, et uniquement à ceux qui en ont besoin.

Les ports peuvent être divisés en deux catégories principales :

### Ports publics

Ces ports permettent de se connecter à des ressources qui doivent être accessibles publiquement.     
Par exemple, les ports qui permettent d'accéder aux serveurs hébergeant le site Web de l'organisation, ou les produits en ligne de l'organisation.

### Ports internes 

Ces ports permettent d'accéder à des données et des services internes, et ne doivent donc pas être accessibles depuis l'extérieur du réseau de l'organisation.    

Ces ports peuvent avoir des objectifs différents, par exemple :     
- Les ports utilisés pour contrôler des machines à distance, comme SSH (22), RDP (3389), WinRM (5985, 5986), Telnet (23), VNC (5800,5801, 5900, 5901), etc.    
- Ports permettant l'accès au stockage de données, comme les bases de données.    
- Ports utilisés pour les applications et les ressources partagées utilisées pour le travail quotidien, comme le courrier, SMB, FTP, etc. 
 
## Recommandation configuration des accès

Les configurations de contrôle de sécurité doivent être appliquées en fonction du rôle et de l'utilisation de chaque serveur. La décision de savoir qui peut accéder à chaque port et à partir de quel emplacement est basée sur l'évaluation des risques organisationnels.    

Par exemple, seul le segment des administrateurs doit avoir accès aux ports utilisés pour contrôler les machines à distance. D'autre part, le serveur HTTP organisationnel doit être accessible au monde entier.

L'objectif doit être de minimiser autant que possible l'accès aux ressources internes sans affecter les performances. 

- Effectuer une évaluation des risques     
- Effectuer la segmentation du réseau     
- Configurer l'accès en fonction de l'évaluation des risques, du rôle et de l'utilisation de chaque serveur.     
- N'autorisez l'accès aux interfaces d'administration qu'à partir d'un segment de réseau dédié aux administrateurs.   
- Éviter d'utiliser des configurations par défaut et des modèles non sécurisés    