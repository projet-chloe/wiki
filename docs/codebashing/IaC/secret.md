# Gestion des secrets

## Intro

Les secrets sont utilisés par les applications et d'autres outils non humains comme une forme d'informations d'identification privilégiées pour accéder aux données.

Ainsi, en bref, les secrets sont des clés qui déverrouillent des informations privées dans de nombreuses infrastructures, y compris les environnements natifs du cloud.

Exemples de secrets communs :

- Mots de passe     
- Certificats     
- Clés – SSH, API, chiffrement     
- Clés d'accès du fournisseur de cloud      

## Risque potentiels

Bien que les secrets soient une forme de sécurité, ils peuvent également devenir la cible d'attaques.

Un pirate qui a accès à ces secrets peut s'infiltrer profondément dans l'infrastructure très rapidement.

Les secrets sont partout, nous comptons sur les secrets dans la plupart des applications, serveurs, processus automatisés, plates-formes et même logiciels de sécurité.

Par conséquent, les organisations doivent protéger les secrets pour se défendre contre les attaques et atténuer les risques.

## Vulnérabilités liées aux secrets dans l'IAC

Il existe deux principales vulnérabilités dans IaC :

### Secrets codés en dur dans les configurations IaC

Les outils IaC accèdent à divers services, qui nécessitent des dizaines de secrets pour l'authentification. Ces secrets sont souvent stockés en clair dans les fichiers IaC, car c'est la manière la plus simple de les gérer. 

Ce qui se passe généralement, c'est que la configuration fait référence aux secrets qui sont stockés dans un fichier séparé. Bien que ce soit une façon d'éviter de télécharger les secrets vers les systèmes de contrôle de version, les secrets sont toujours stockés en clair sur la machine du développeur. 

Alternativement, si les secrets sont stockés sur un serveur, toute vulnérabilité permettant de lire les fichiers conduirait à leur exposition.

### Secrets dans le système de contrôle de version

Le système de contrôle de version conserve un historique des livraisons, y compris l'ajout et la suppression des secrets. Les développeurs peuvent oublier de supprimer les secrets avant de télécharger une configuration dans le système de contrôle de version. 

Ensuite, il ne suffit pas de supprimer les secrets du fichier et de valider les modifications. Les pirates peuvent facilement exécuter un outil qui recherche les secrets dans l'historique des validations. 

Par conséquent, une bonne façon d'effacer les secrets du VCS est de supprimer manuellement les commits contenant les secrets.

## Secret en clair

Le stockage de secrets en clair est un risque de sécurité courant. Les secrets en texte brut peuvent être exposés même si les développeurs les stockent dans un fichier séparé ou dans les variables d'environnement.

Ne jamais stocker des secret en clair même  dans :

- fichier séparé : le stockage des secrets en texte brut dans des fichiers séparés donne une illusion de sécurité, puisqu'ils ne se trouvent pas au même endroit que le code.     
Risque :  Les secrets sont toujours en texte clair et un risque supplémentaire est de les télécharger accidentellement vers le SVN et de les exposer.             

- vairable d'environnement      
Risque : Cela ne résout que la moitié du problème - il est vrai que les secrets ne seront pas téléchargés vers le SVN, mais ils sont toujours stockés dans le système d'exploitation en texte clair. Une fois qu'un attaquant peut lire des fichiers ou exécuter des commandes du système d'exploitation, il est en mesure d'accéder aux secrets.

## Gestion 

La gestion des secrets est une couche de sécurité supplémentaire qui vous aide à protéger les secrets nécessaires pour accéder à vos applications, services et ressources informatiques.

Les utilisateurs et les applications récupèrent les secrets en appelant l'API du logiciel Secret Management, éliminant ainsi le besoin de coder en dur les informations sensibles en texte brut.

L'utilisation d'un système de gestion des secrets vous permet de contrôler l'accès aux secrets à l'aide d'autorisations dans le cloud, de services tiers et sur site.

## Remédiation 

Nous suggérons les recommandations suivantes (classées de l'optimal au moins optimal) :

- Concevoir une solution sécurisée : Consultez des experts en sécurité, tels que l'équipe de sécurité résidente ou un cabinet de conseil externe, avant de chercher une solution.    
- Utiliser un système de gestion des secrets : De nombreux systèmes de gestion des secrets, commerciaux ou à code source ouvert, sont disponibles. Tant que vous stockez les secrets séparément de tout code ou configuration, vous êtes un peu plus en sécurité !      
- Localisez et identifiez les secrets dans votre IaC : Analysez votre IaC avec des scanners dédiés comme KICS qui montrent les problèmes de sécurité de l'IaC, et les secrets téléchargés dans VCS ou oubliés dans le code. Le meilleur moment pour exécuter un tel scan est avant de soumettre le code au système de contrôle de version ; de cette façon, aucun secret ne sera exposé.

## Secret dans Terraform state

La gestion de votre infrastructure et la configuration de Terraform nécessitent que Terraform stocke l'état.

Terraform utilise cet état pour mapper les ressources du monde réel à votre configuration, suivre les métadonnées et améliorer les performances des grandes infrastructures.

Il est généralement stocké dans un fichier local nommé "terraform.tfstate", mais il peut également être stocké à distance, ce qui est préférable si vous travaillez en équipe.

## Recommandation

L'utilisation de Terraform est une méthode recommandée pour éviter d'exposer des secrets sur la machine d'un développeur. Cependant, des secrets peuvent résider dans l'état de Terraforms.

Le fichier d'état peut contenir ces secrets même si le code IaC n'a pas d'informations d'identification et qu'ils sont ajoutés ou générés à la volée.

Utilisez ces deux recommandations pour sécuriser les secrets.

### Fonctions sensibles 

Terraform dispose d'une fonction " sensible " qui l'empêche d'afficher les valeurs marquées comme sensibles dans la sortie.   Il est recommandé d'utiliser cette fonction pour réduire le risque d'affichage de secrets pendant l'exécution des commandes Terraform. Cependant, elle n'empêche pas les secrets d'être exposés dans les fichiers d'état de Terraform.

### Fonctions distantes

La fonction d'état distant de Terraform permet de stocker l'état à distance dans un certain nombre de backends. En stockant l'état à distance, l'accent n'est plus mis sur la sécurisation de l'ordinateur du développeur mais sur la sécurisation du backend qui stocke l'état. 

Cela est nécessaire lorsque plusieurs ingénieurs configurent l'infrastructure, car le stockage de l'état localement sur la machine de chaque ingénieur augmente non seulement les risques de sécurité, mais peut également entraîner des incohérences de configuration et des erreurs de provisionnement de l'infrastructure. 

Un bucket S3 qui a des paramètres de contrôle d'accès au moindre privilège peut, par exemple, être utilisé pour stocker l'état.
