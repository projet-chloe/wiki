# Introduction

## Qu'est ce que c'est ?

L'infrastructure en tant que code (IaC) est le processus de gestion et d'approvisionnement de logiciels via des fichiers de définition lisibles par machine, plutôt qu'une configuration matérielle physique ou des outils de configuration interactifs.

L'objectif de l'infrastructure en tant que code est de permettre aux développeurs ou aux équipes d'exploitation d'automatiser la gestion, la surveillance et l'approvisionnement des ressources au lieu de la configuration manuelle des périphériques matériels et des systèmes d'exploitation discrets.

Permet de :   
- Accélérer la configuration et la mise en œuvre d'une nouvelle infrastructure informatique
- Réduire le coût et les ressources nécessaires à la mise à l'échelle et à la gestion d'une grande infrastructure    
- Éliminer les incohérences qui se produisent inévitablement lorsque plusieurs personnes configurent manuellement de nouveaux équipements ou applications.    

## Sécurité

Les infrastructures programmables et définies par logiciel sont des termes souvent utilisés pour décrire l'infrastructure en tant que code.

Comme pour les pratiques de conception de logiciels, l'automatisation de l'infrastructure basée sur le code implique de contrôler étroitement les versions de code, de tester les itérations et de limiter le déploiement jusqu'à ce que le logiciel soit éprouvé et approuvé.

Un modèle de responsabilité partagée en matière de sécurité est suivi par la plupart des fournisseurs de services cloud, ce qui signifie que votre équipe de sécurité conserve une certaine responsabilité, tandis que le fournisseur assume une part, mais pas la totalité, de la responsabilité.

Par conséquent, les développeurs sont tenus de mettre en œuvre leurs propres contrôles de sécurité à chaque étape du processus. 

Client : responsable de la sécurité des truc dans le cloud    
- Plateformes, applications, gestion des identités et des accès    
- Configuration du système d'exploitation, du réseau et du pare-feu    
- Cryptage côté serveur    
- Cryptage des données côté client et authentification de l'intégrité des données    
- Protection du trafic réseau    

Fournisseur de services cloud : responsale de la sécurité du cloud    
- Logiciel    
- Stockage   
- Matériel   
- Base de données   
- Calculateur    
- Données clients   
- Mise en réseau   
- Zones de disponibilité    
- Sites de périphérie     

### Top 5 misconfigurations

![misconfiguration](/images/top5misconfIAC.png)
