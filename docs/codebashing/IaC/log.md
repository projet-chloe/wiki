# Log

## Intro

La journalisation est l'un des éléments essentiels de tout système. Il est généralement considéré comme un mécanisme permettant d'assurer le bon fonctionnement des systèmes et de fournir des informations sur les événements qui se produisent à des fins de débogage et de détection d'erreurs.

Cependant, les journaux sont également une source précieuse d'informations qui peuvent aider à améliorer la sécurité, à détecter les menaces potentielles et à prévenir les attaques.

Les capacités de réponse aux incidents se sont considérablement améliorées ces dernières années, car les centres d'opérations de sécurité (SOC) peuvent désormais recevoir des journaux pour surveiller et corréler d'immenses quantités d'informations provenant de chaque niveau du système afin d'identifier les anomalies ou les incidents qui ont pu se produire.  En surveillant l'ensemble de la pile technologique de l'application et l'infrastructure sous-jacente, les équipes SOC peuvent détecter rapidement les attaques potentielles. 

1. Les journaux proviennent des applications et du cloud.    
2. Les événements sont collectés et traités par un système de gestion des informations et des événements de sécurité (SIEM).    
3. Si le système détecte une anomalie, une alerte est envoyée à l'analyste du SOC.

## Surveillance des logs

Les principaux fournisseurs de cloud offrent de puissants outils de journalisation et de surveillance.

Chaque service peut générer des journaux robustes avec tous les détails nécessaires ou fournir des outils de collecte de journaux pour les journaux de l'appareil, de l'application et du système d'exploitation (par exemple, les journaux du système d'exploitation de l'instance ou de l'application s'exécutant sur l'instance).

Après avoir enregistré vos données, il est essentiel de les agréger et de les surveiller efficacement.

La combinaison de journaux informatifs et d'une surveillance correctement configurée permet une détection précoce et une réponse à un large éventail de problèmes techniques et de sécurité.

## Objctifs

La journalisation et la surveillance efficaces sont une source de données clé qui remplit 5 objectifs principaux du point de vue de la sécurité.  

### Prévention des activités malveillantes dans le cloud

Avec de nombreuses instances dans plusieurs régions, il est difficile de suivre les changements et de mettre hors service les instances inutilisées en temps voulu.

Sans une journalisation et une surveillance appropriées, les attaquants peuvent prendre le contrôle d'instances individuelles et mener des actions néfastes telles que l'envoi d'e-mails de phishing ou le minage de cryptocurrences sans être détectés. 

Outre la mise en œuvre de mesures préventives de gestion des actifs, une collecte, une gestion des journaux et une surveillance minutieuses peuvent contribuer à prévenir et à atténuer les incidents de sécurité. 

Par exemple : rechercher des pics dans le montant payé par le fournisseur de cloud pour chaque instance.

### Prévention de la fraude et des intrusions

Les systèmes de prévention des fraudes et de détection des intrusions peuvent utiliser les données au niveau des applications pour identifier et prévenir les activités malveillantes.

### Non-Repudiation

Les données concernant les actions des utilisateurs dans l'application peuvent être une source de preuve dans les litiges juridiques, surtout si elles contiennent une signature numérique.

### Activités forensic

- Détection des incidents : Une réponse rapide aux alertes concernant les anomalies identifiées dans l'application peut prévenir des attaques graves. 

- Investigation : Les journaux détaillés permettent aux enquêteurs de reconstituer le flux de l'attaque et de trouver les faiblesses exploitées.

### Conformité

Les normes industrielles et les réglementations de certains pays exigent que les entreprises mènent des activités de détection des attaques de manière proactive. 

## Recommandations

### Modification

Les journaux sont utilisés comme preuve des événements, leur intégrité doit donc être garantie.    
Les journaux doivent être en lecture seule et il doit être impossible d'y apporter des modifications.

### Suppression du journal

La suppression des journaux doit être une procédure en plusieurs étapes. La suppression des journaux en une seule commande ou un seul clic doit être impossible. Les journaux peuvent être supprimés après la période de conservation, alors que la suppression manuelle doit être une procédure exceptionnelle en plusieurs étapes. 

### Opérations critiques

Les activités critiques telles que les tentatives d'authentification, les opérations financières, les transactions signées avec la signature numérique et d'autres opérations critiques doivent être enregistrées.    

L'objectif de l'enregistrement de ces opérations est de découvrir rapidement les attaques (par exemple, le forçage brutal des identifiants et des mots de passe ou le transfert d'argent à partir d'un compte bancaire Internet volé) et de vérifier qu'un événement s'est effectivement produit (par exemple, lorsqu'un utilisateur nie avoir effectué une transaction). 

### Adaptation des journaux

Les données enregistrées et le format de journalisation doivent être adaptés, car l'écriture de données excessives dans les journaux consomme beaucoup d'espace disque et rend impossible la recherche d'une aiguille dans la botte de foin.     

En revanche, l'omission de données rend la surveillance moins efficace car il n'y a pas assez de données à partir desquelles construire des règles.     

Par conséquent, le format de journalisation doit être adapté aux besoins des équipes de surveillance et de sécurité.  

Veillez à ce que toutes les données consignées soient utilisées par un autre système ou servent à quelque chose (par exemple, elles peuvent être utilisées dans le cadre d'enquêtes médico-légales). 

### Injection de journaux

Les journaux doivent être protégés contre l'injection de journaux. L'injection de journal se produit lorsque des données non fiables sont directement insérées dans l'entrée du journal. Les injections de journaux conduisent à la falsification de journaux (ajout de nouvelles entrées de journaux falsifiées), au XSS lorsque les données des journaux sont ajoutées aux pages Web et à l'exécution de code lorsque divers analyseurs traitent les journaux (par exemple, l'exécution de code PHP lorsque le contenu du journal est ajouté comme partie du code php).

### Gestion des accès

Les journaux contiennent souvent des informations critiques qui ne sont accessibles qu'à un cercle restreint d'employés. Par conséquent, l'accès au stockage des journaux et aux journaux bruts doit être limité.    

En outre, les procédures de l'entreprise impliquant l'analyse des journaux doivent garantir que les données confidentielles ne parviennent pas à des employés non privilégiés.     

Par exemple, si le service d'assistance résout le cas du client sur la base des journaux bruts, l'employé du service d'assistance doit être autorisé à accéder aux données, ou les données doivent être filtrées pour supprimer les données confidentielles. 

### Cryptage

Le chiffrement empêche les attaquants d'accéder aux données lorsqu'ils y accèdent involontairement (l'accès intentionnel décrypte généralement les données avant d'y accéder).     

Lorsqu'ils ne sont pas utilisés, les journaux doivent être chiffrés dans le cadre de l'approche de défense en profondeur.    

Sans la clé de chiffrement, l'attaquant ne peut pas accéder aux données elles-mêmes, même en ayant accès au stockage.

### Données sensibles

Bien que les journaux soient confidentiels, ils ne doivent pas contenir de données sensibles comme des cartes de crédit, des informations personnelles identifiables (PII), des mots de passe, etc.    

Les données sensibles doivent avoir leur propre stockage séparé. Vous pouvez résoudre ce risque de sécurité en évitant tout simplement d'enregistrer des données sensibles. 

### Dans AWS

#### Journal des flux VPC

Toute l'activité du réseau peut être facilement enregistrée grâce à la fonction d'enregistrement des flux.     
Cette fonction conserve la trace des demandes réseau adressées à un VPC (Virtual Private Clouds), un sous-réseau ou une interface réseau.    

L'analyse du journal des flux permet d'identifier les demandes inutiles et malveillantes, par exemple, les demandes de mise à jour d'un serveur externe, ou la commande et le contrôle du serveur d'un attaquant.

#### Conservation des journaux

Les différents fichiers journaux ont des fonctions différentes. Certains fichiers journaux peuvent être éliminés rapidement, tandis que d'autres doivent être soigneusement conservés.  

Les objectifs de la conservation des journaux doivent être décrits dans un document dédié, par exemple, une politique de conservation des journaux.  

L'utilisation d'une telle politique permet de s'assurer que les exigences en matière d'informatique (technologies de l'information), de sécurité et de conformité sont toutes respectées.

#### Intégrité des journaux

AWS permet d'ajouter la validation des fichiers journaux pour garantir leur intégrité. Grâce à cette fonctionnalité, vous pouvez vérifier si les fichiers journaux ont été modifiés en stockant un hachage de chaque fichier journal.

#### Stockage des journaux dans des seaux S3

Le stockage des journaux dans un bucket S3 dédié est une solution simple et efficace, mais elle nécessite une configuration de contrôle d'accès particulière.    

L'accès au bucket S3 doit être limité à un groupe restreint d'employés et aux systèmes automatisés de journalisation.    

La suppression des bucket S3 contenant les logs doit être protégée au moins par MFA (Multi Factor Authentication).

#### Cloud trail

AWS CloudTrail surveille et enregistre l'activité des comptes dans votre infrastructure AWS, ce qui vous permet de contrôler le stockage, l'analyse et les actions de remédiation.  

La journalisation des événements de l'infrastructure de cloud est importante mais n'est pas suffisante. Les événements des instances EC2, du système d'exploitation et doivent également être consignés.  

L'activation de CloudTrail dans toutes les régions permettra de détecter les attaquants s'ils accèdent à l'infrastructure de gestion du cloud et déploient silencieusement des instances porteuses d'activités malveillantes dans des régions non utilisées. 