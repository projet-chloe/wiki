# Welcome to my Wiki !!

Ce site est crée à partir de mon dépot [gitlab](https://gitlab.com/projet-chloe/wiki).
L'objectif est de grouper différentes informations que j'ai pu collecter au fur et à mesure. Cela me permettra d'avoir une base de notes au meme endroit et facile d'accès que je pourrais reutiliser au besoin.

## Présentation

Plusieurs thèmes serotn abordés dans ce wiki.
Une barre de navigation à gauche vous permet de sélectionner un sujet et ensuite sur chaque page il y a des sous partie qui sont facilement repérable avec l'onglet à droite.

* `Cyber` - aspect lié à la cybersécurité comme les vulnérabilités, les outils ...
* `GitLab` - les commandes de l'outil git et l'utilisation de la plateforme GitLab
* `Linux` - les commandes de bases sur un terminal linux, comment crée une paire de clé et un certificat pour le web
* `Installer une VM` -  indication et methodo pour installer une machine virtuelle et les outils utiles

## Qui-je suis ?

    Chloé Kammerlocher
    Je suis étudiante en master 2 spécialité cybersécurité dans une école d'ingenieur généraliste à l'ISEN de Nantes.
    J'ai envie d'en apprendre davantages sur le monde de la cyber et notamment d'améliorer mes compétences et enrichier mes connaissance sur le métier de pentester / auditeur.        

![image](/image/logo.png)