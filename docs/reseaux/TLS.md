# TLS

Nous découvrons une solution standard pour protéger la confidentialité et l'intégrité des paquets échangés. L'approche suivante peut protéger contre le reniflage de mot de passe et les attaques MITM .

SSL (Secure Sockets Layer) a commencé lorsque le World Wide Web a commencé à voir de nouvelles applications, telles que les achats en ligne et l'envoi d'informations de paiement. Netscape a introduit SSL en 1994, SSL 3.0 étant sorti en 1996. Mais finalement, plus de sécurité était nécessaire, et le protocole TLS (Transport Layer Security) a été introduit en 1999. Avant d'expliquer ce que TLS et SSL fournissent, voyons comment ils s'adaptent au modèle de mise en réseau.

Les protocoles courants que nous avons couverts jusqu'à présent envoient les données en clair ; cela permet à toute personne ayant accès au réseau de capter, sauvegarder et analyser les messages échangés. L'image ci-dessous montre les couches réseau ISO/OSI. Les protocoles que nous avons couverts jusqu'à présent dans cette salle se trouvent sur la couche application. Prenons le modèle ISO/OSI ; nous pouvons ajouter un cryptage à nos protocoles via la couche de présentation. Par conséquent, les données seront présentées dans un format crypté (texte chiffré) au lieu de leur forme originale.

En raison de la relation étroite entre SSL et TLS, l'un peut être utilisé à la place de l'autre. Cependant, TLS est plus sécurisé que SSL et a pratiquement remplacé SSL. Nous aurions pu abandonner SSL et simplement écrire TLS au lieu de SSL/TLS, mais nous continuerons à mentionner les deux pour éviter toute ambiguïté car le terme SSL est encore largement utilisé. Cependant, nous pouvons nous attendre à ce que tous les serveurs modernes utilisent TLS.

Un protocole en clair existant peut être mis à niveau pour utiliser le cryptage via SSL/TLS. Nous pouvons utiliser TLS pour mettre à niveau HTTP , FTP, SMTP, POP3 et IMAP, pour n'en nommer que quelques-uns. Le tableau suivant répertorie les protocoles que nous avons couverts et leurs ports par défaut avant et après la mise à niveau du chiffrement via SSL/TLS. La liste est non exhaustive; cependant, le but est de nous aider à mieux comprendre le processus.

## HTTPS

Considérant le cas de HTTP . Initialement, pour récupérer une page Web via HTTP, le navigateur Web devrait au moins effectuer les deux étapes suivantes :

- Établir une connexion TCP avec le serveur Web distant  
- Envoyez des requêtes HTTP au serveur Web, telles que des requêtes GET et POST   

HTTPS nécessite une étape supplémentaire pour chiffrer le trafic. La nouvelle étape a lieu après l'établissement d'une connexion TCP et avant l'envoi de requêtes HTTP. Cette étape supplémentaire peut être déduite du modèle ISO/OSI dans l'image présentée précédemment. Par conséquent, HTTPS nécessite au moins les trois étapes suivantes :

1. Établir une connexion TCP   
2. Établir une connexion SSL/TLS   
3. Envoyer des requêtes HTTP au serveur Web   

Après avoir établi une connexion TCP avec le serveur, le client établit une connexion SSL/TLS, comme illustré dans la figure ci-dessus. Les termes peuvent sembler compliqués selon vos connaissances en cryptographie, mais nous pouvons simplifier les quatre étapes comme suit :

1. Le client envoie un ClientHello au serveur pour indiquer ses capacités, telles que les algorithmes pris en charge.   
2. Le serveur répond par un ServerHello, indiquant les paramètres de connexion sélectionnés. Le serveur fournit son certificat si l'authentification du serveur est requise. Le certificat est un fichier numérique permettant de s'identifier ; il est généralement signé numériquement par un tiers. De plus, il peut envoyer des informations supplémentaires nécessaires pour générer la clé principale, dans son message ServerKeyExchange, avant d'envoyer le message ServerHelloDone pour indiquer que la négociation est terminée.   
3. Le client répond avec un ClientKeyExchange, qui contient des informations supplémentaires requises pour générer la clé principale. De plus, il passe à l'utilisation du chiffrement et informe le serveur à l'aide du message ChangeCipherSpec.   
4. Le serveur passe également au chiffrement et informe le client dans le message ChangeCipherSpec.   

Si cela semble toujours sophistiqué, ne vous inquiétez pas ; nous n'avons besoin que de l'essentiel. Un client a pu convenir d'une clé secrète avec un serveur disposant d'un certificat public. Cette clé secrète a été générée de manière sécurisée afin qu'un tiers surveillant la chaîne ne puisse pas la découvrir. La communication ultérieure entre le client et le serveur sera cryptée à l'aide de la clé générée.

Par conséquent, une fois qu'une poignée de main SSL/TLS a été établie, les requêtes HTTP et les données échangées ne seront plus accessibles à quiconque surveille le canal de communication.

Enfin, pour que SSL/TLS soit efficace, en particulier lorsque vous naviguez sur le Web via HTTPS, nous nous appuyons sur des certificats publics signés par des autorités de certification approuvées par nos systèmes. En d'autres termes, lorsque nous naviguons vers TryHackMe via HTTPS, notre navigateur s'attend à ce que le serveur Web TryHackMe fournisse un certificat signé d'une autorité de certification de confiance, comme dans l'exemple ci-dessous. De cette façon, notre navigateur s'assure qu'il communique avec le bon serveur et qu'une attaque MITM ne peut pas se produire.

## certificat 

Dans la figure ci-dessus, nous pouvons voir les informations suivantes :

- A qui est délivré le certificat ? C'est le nom de l'entreprise qui utilisera ce certificat.  
- Qui a délivré le certificat ? Il s'agit de l'autorité de certification qui a émis ce certificat.   
- Période de validité. Vous ne voulez pas utiliser un certificat qui a expiré, par exemple.    

Heureusement, nous n'avons pas à vérifier manuellement le certificat pour chaque site que nous visitons ; notre navigateur Web le fera pour nous. Notre navigateur Web s'assurera que nous parlons avec le bon serveur et s'assurera que notre communication est sécurisée, grâce au certificat du serveur.