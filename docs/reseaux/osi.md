# Modèle OSI

## Intro

Le modèle OSI (ou Open Systems Interconnection Model ) est un modèle fondamental absolu utilisé dans les réseaux. Ce modèle critique fournit un cadre dictant comment tous les appareils en réseau enverront, recevront et interpréteront les données.

L'un des principaux avantages du modèle OSI est que les appareils peuvent avoir différentes fonctions et conceptions sur un réseau tout en communiquant avec d'autres appareils. Les données envoyées sur un réseau qui suit l'uniformité du modèle OSI peuvent être comprises par d'autres appareils.

Le modèle OSI se compose de sept couches :
7. Application    
6. Presentation     
5. Session     
4. Transport     
3. Network     
2. Data link    
1. Physical       

Chaque couche a un ensemble différent de responsabilités.

À chaque couche individuelle traversée par les données, des processus spécifiques ont lieu et des éléments d'information sont ajoutés à ces données.

## Couche 7 : application

La couche application du modèle OSI est la couche avec laquelle vous serez le plus familier. Cette familiarité est due au fait que la couche application est la couche dans laquelle les protocoles et les règles sont en place pour déterminer comment l'utilisateur doit interagir avec les données envoyées ou reçues.

Les applications courantes telles que les clients de messagerie, les navigateurs ou les logiciels de navigation sur les serveurs de fichiers tels que FileZilla fournissent une interface utilisateur  graphique (GUI ) conviviale permettant aux utilisateurs d'interagir avec les données envoyées ou reçues. D' autres protocoles incluent DNS ( Domain Name System ) , qui est la façon dont les adresses de sites Web sont traduites en adresses IP.

## Couche 6 : presentation 

La couche 6 du modèle OSI est la couche dans laquelle la normalisation commence à avoir lieu. Étant donné que les développeurs de logiciels peuvent développer différemment n'importe quel logiciel, tel qu'un client de messagerie, les données doivent toujours être traitées de la même manière, quel que soit le fonctionnement du logiciel.

Cette couche agit comme un traducteur pour les données vers et depuis la couche application (couche 7). L'ordinateur récepteur comprendra également les données envoyées à un ordinateur dans un format destiné à un autre format. Par exemple, lorsque vous envoyez un e-mail, l'autre utilisateur peut avoir un autre client de messagerie pour vous, mais le contenu de l'e-mail devra toujours s'afficher de la même manière.

Les fonctionnalités de sécurité telles que le cryptage des données (comme HTTPS lors de la visite d'un site sécurisé) se produisent à cette couche.

## Couche 5: session

Une fois que les données ont été correctement traduites ou formatées à partir de la couche de présentation (couche 6), la couche de session (couche 5) commencera à créer une connexion avec l'autre ordinateur auquel les données sont destinées. Lorsqu'une connexion est établie, une session est créée. Tant que cette connexion est active, la session l'est également.

La couche session (couche 5) synchronise les deux ordinateurs pour s'assurer qu'ils sont sur la même page avant l'envoi et la réception des données. Une fois ces vérifications en place, la couche session commencera à diviser les données envoyées en plus petits morceaux de données et commencera à envoyer ces morceaux (paquets) un par un. Cette division est bénéfique car si la connexion est perdue, seuls les morceaux qui n'ont pas encore été envoyés devront être renvoyés - pas l'ensemble des données (pensez-y comme le chargement d'un fichier de sauvegarde dans un jeu vidéo).

Ce qu'il convient de noter, c'est que les sessions sont uniques, ce qui signifie que les données ne peuvent pas voyager sur différentes sessions, mais en fait, uniquement sur chaque session.

## Couche 4 : transport

La couche 4 du modèle OSI joue un rôle essentiel dans la transmission des données sur un réseau et peut être un peu difficile à saisir. Lorsque des données sont envoyées entre des appareils, elles suivent l'un des deux protocoles différents qui sont décidés en fonction de plusieurs facteurs : TCP et UDP.

### TCP

Le protocole de contrôle de la transmission ( TCP ) . Potentiellement évoqué par son nom, ce protocole est conçu dans un souci de fiabilité et de garantie. Ce protocole réserve une connexion constante entre les deux appareils pendant le temps nécessaire à l'envoi et à la réception des données.

Non seulement cela, mais TCP intègre la vérification des erreurs dans sa conception. La vérification des erreurs est la façon dont TCP peut garantir que les données envoyées à partir des petits morceaux de la couche session (couche 5) ont ensuite été reçues et réassemblées dans le même ordre.

Résumons les avantages : 

- Garantit l'exactitude des données.    
- Capable de synchroniser deux appareils pour éviter que l'autre ne soit inondé de données.    
- Effectue beaucoup plus de processus pour la fiabilité.    

Résumons les inconvénients : 

- Nécessite une connexion fiable entre les deux appareils. Si un petit bloc de données n'est pas reçu, le bloc de données entier ne peut pas être utilisé.     
- Une connexion lente peut engorger un autre appareil car la connexion sera réservée sur l'ordinateur récepteur tout le temps.    
- TCP est nettement plus lent que UDP car plus de travail doit être effectué par les périphériques utilisant ce protocole.     

TCP est utilisé pour des situations telles que le partage de fichiers, la navigation sur Internet ou l'envoi d'un e-mail. Cette utilisation est due au fait que ces services exigent que les données soient exactes et complètes.

### UDP

Passons maintenant au User Datagram Protocol. Ce protocole n'est pas aussi avancé que son frère - le protocole TCP . Il ne possède pas les nombreuses fonctionnalités offertes par TCP, telles que la vérification des erreurs et la fiabilité. En fait, toutes les données envoyées via UDP sont envoyées à l'ordinateur, qu'elles y parviennent ou non. Il n'y a pas de synchronisation entre les deux appareils ni de garantie ; espérons juste pour le mieux et croisons les doigts.

Avantages : 
- UDP est beaucoup plus rapide que TCP.    
- UDP laisse la couche application (logiciel utilisateur) décider s'il y a un contrôle sur la rapidité d'envoi des paquets.     
- UDP ne réserve pas une connexion continue sur un appareil comme le fait TCP.    

Inconvénients de l' UDP

- UDP ne se soucie pas de savoir si les données sont reçues.       
- Il est assez flexible pour les développeurs de logiciels dans ce sens.     
- Cela signifie que des connexions instables entraînent une expérience terrible pour l'utilisateur.     

## Couche 3 : réseau

La troisième couche du modèle OSI (couche réseau) est l'endroit où la magie du routage et du réassemblage des données a lieu (de ces petits morceaux au plus gros morceau). Premièrement, le routage détermine simplement le chemin le plus optimal dans lequel ces blocs de données doivent être envoyés.

Alors que certains protocoles de cette couche déterminent exactement quel est le chemin "optimal" que les données doivent emprunter pour atteindre un appareil, nous ne devrions connaître leur existence qu'à ce stade du module de mise en réseau. Brièvement, ces protocoles incluent OSPF ( Open S hortest P ath First ) et RIP ( R outing Information Protocol ) . Les facteurs qui décident de l'itinéraire emprunté sont déterminés par les éléments suivants :

- Quel chemin est le plus court ? C'est-à-dire qu'il a le moins d'appareils que le paquet doit traverser.     
- Quel chemin est le plus fiable ? C'est-à-dire que des paquets ont déjà été perdus sur ce chemin ?      
- Quel chemin a la connexion physique la plus rapide ? C'est-à-dire qu'un chemin utilise une connexion en cuivre (plus lent) ou une fibre (considérablement plus rapide) ?

À cette couche, tout est traité via des adresses IP telles que 192.168.1.100. Les périphériques tels que les routeurs capables de transmettre des paquets à l'aide d'adresses IP sont appelés périphériques de couche 3, car ils sont capables de fonctionner au niveau de la troisième couche du modèle OSI.

## Couche 2 : liaison de données

La couche liaison de données se concentre sur l'adressage physique de la transmission. Il reçoit un paquet de la couche réseau (y compris l'adresse IP de l'ordinateur distant) et ajoute l'adresse physique MAC ( M edia A ccess Control ) du  terminal récepteur. À l'intérieur de chaque ordinateur connecté au réseau se trouve une carte d' interface réseau ( NIC ) qui est fournie avec une adresse MAC unique pour l'identifier.  

Les adresses MAC sont définies par le fabricant et littéralement gravées dans la carte ; ils ne peuvent pas être modifiés, bien qu'ils puissent être usurpés. Lorsque des informations sont envoyées sur un réseau, c'est en fait l'adresse physique qui est utilisée pour identifier exactement où envoyer les informations.

De plus, c'est aussi le travail de la couche liaison de données de présenter les données dans un format adapté à la transmission.

## Couche 1 : physique

Cette couche est l'une des couches les plus faciles à saisir. En termes simples, cette couche fait référence aux composants physiques du matériel utilisé dans la mise en réseau et est la couche la plus basse que vous trouverez. Les appareils utilisent des signaux électriques pour transférer des données entre eux dans un système de numérotation binaire (1 et 0).