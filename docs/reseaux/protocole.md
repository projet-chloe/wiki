# Les protocoles courants

## telnet

Le protocole Telnet est un protocole de couche application utilisé pour se connecter à un terminal virtuel d'un autre ordinateur. À l'aide de Telnet, un utilisateur peut se connecter à un autre ordinateur et accéder à son terminal (console) pour exécuter des programmes, démarrer des processus par lots et effectuer des tâches d'administration système à distance.

Le protocole Telnet est relativement simple. Lorsqu'un utilisateur se connecte, il lui sera demandé un nom d'utilisateur et un mot de passe. Après une authentification correcte, l'utilisateur accédera au terminal du système distant. Malheureusement, toutes ces communications entre le client Telnet et le serveur Telnet ne sont pas cryptées, ce qui en fait une cible facile pour les attaquants.

Un serveur Telnet utilise le protocole Telnet pour écouter les connexions entrantes sur le port 23.

Telnet n'est plus considéré comme une option sécurisée, d'autant plus que toute personne capturant votre trafic réseau pourra découvrir vos noms d'utilisateur et mots de passe, ce qui leur donnerait accès au système distant.

## HTTP

Le protocole de transfert hypertexte ( HTTP ) est le protocole utilisé pour transférer des pages Web.

HTTP envoie et reçoit des données en clair (non cryptées) ; par conséquent, vous pouvez utiliser un outil simple, tel que Telnet (ou Netcat), pour communiquer avec un serveur Web et agir comme un "navigateur Web". La principale différence est que vous devez saisir les commandes liées à HTTP au lieu que le navigateur Web le fasse pour vous.

Exemple : comment nous pouvons demander une page à un serveur Web avec telnet :  

1. Tout d'abord, nous nous connectons au port 80 en utilisant telnet MACHINE_IP 80.   
2. Ensuite, nous devons taper GET /index.html HTTP/1.1 pour récupérer la page index.html.    
3. Enfin, vous devez fournir une valeur pour l'hôte comme host: telnet et appuyer deux fois sur la touche Entrée/Retour.   
Dans la sortie de la console ci-dessous, nous pourrions récupérer la page demandée avec une mine d'informations qui ne sont généralement pas affichées par le navigateur Web. Si la page que nous avons demandée n'est pas trouvée, nous obtenons l'erreur 404.   

Les trois choix populaires pour les serveurs HTTP sont : apache, Services d'information sur Internet (IIS) et nginx.   

## FTP

Le protocole de transfert de fichiers ( FTP ) a été développé pour rendre efficace le transfert de fichiers entre différents ordinateurs avec différents systèmes.

FTP envoie et reçoit également des données en texte clair ; par conséquent, nous pouvons utiliser Telnet (ou Netcat) pour communiquer avec un serveur FTP et agir en tant que client FTP. Les serveurs FTP écoutent sur le port 21 par défaut.

Il est possible de se connecter aussi avec telnet. Cependant, nous ne pouvons pas transférer un fichier à l'aide d'un simple client tel que Telnet car FTP crée une connexion distincte pour le transfert de fichiers.  

commande intéressantes pour le telnet avec ftp :   

- USER : pour mettre le nom d'utilisateur (quand on se connecte avec telnet par exemple)    
- PASS : pour mettre le mot de passe   
- STAT : donnera des inforamtions supplémentaires   
- SYST : affiche le type de système de la cible   
- PASV : passe en mode passif   
- TYPE A : bascule le mode de transfert de fichier en ASCII   
- TYPE I : bascule le mode de transfert de fichier en binaire   

Il est à noter qu'il existe deux modes pour le FTP :  

- Actif : les données sont envoyées sur un canal séparé provenant du port 20 du serveur FTP.   
- Passif : les données sont envoyées sur un canal séparé provenant du port d'un client FTP au-dessus du numéro de port 1023.   

On peut aussi se connecter en utilisant l'outil ftp directement. Après s'être connecté avec succès, nous obtenons l'invite FTP, ftp>, pour exécuter diverses commandes FTP. Enfin, get FILENAME permet de récupérer un fichier et le télécharger sur notre local.

Voici des exemples de logiciels de serveur FTP : vsftpd, ProFTPD, uFTP. 

Étant donné que FTP envoie les identifiants de connexion avec les commandes et les fichiers en texte clair, le trafic FTP peut être une cible facile pour les attaquants.

## Serveur demessagerie

### Rappel

Le courrier électronique est l'un des services les plus utilisés sur Internet.

La distribution d'e-mails sur Internet nécessite les composants suivants :  

- Agent de soumission de courrier (MSA)
- Agent de transfert de courrier (MTA)
- Agent de livraison du courrier (MDA)
- Agent d'utilisateur de messagerie (MUA)

Voici les cinq étapes qu'un e-mail doit suivre pour atteindre la boîte de réception du destinataire :

1. Un agent utilisateur de messagerie ( MUA ), ou simplement un client de messagerie, a un message électronique à envoyer. Le MUA se connecte à un agent de soumission de courrier (MSA) pour envoyer son message.   
2. Le MSA reçoit le message, recherche d'éventuelles erreurs avant de le transférer au serveur de l'agent de transfert de courrier ( MTA ), généralement hébergé sur le même serveur.   
3. Le MTA enverra le message électronique au MTA du destinataire. Le MTA peut également fonctionner comme un agent de soumission de courrier (MSA).   
4. Dans une configuration typique, le serveur MTA fonctionnerait également comme un agent de distribution de courrier (MDA).    
5. Le destinataire récupérera son e-mail auprès du MDA à l'aide de son client de messagerie.   
   
### SMTP 

Le protocole SMTP (Simple Mail Transfer Protocol ) est utilisé pour communiquer avec un serveur MTA. Étant donné que SMTP utilise du texte clair, où toutes les commandes sont envoyées sans cryptage, nous pouvons utiliser un client Telnet de base pour se connecter à un serveur SMTP et agir comme un client de messagerie (MUA) envoyant un message.

Le serveur SMTP écoute sur le port 25 par défaut. 

étapes telnet : helo hostname > mail from : pour indiquer l'expediteur > rcpt to : pour indiquer le destinataire > data : pour écrire notre message > nous finissons par un . pour mettre fin au message.

Le serveur SMTP met maintenant le message en file d'attente.

### POP3

Post Office Protocol version 3 (POP3) est un protocole utilisé pour télécharger les messages électroniques à partir d'un serveur Mail Delivery Agent ( MDA ), comme illustré dans la figure ci-dessous. Le client de messagerie se connecte au serveur POP3, s'authentifie, télécharge les nouveaux messages électroniques. Le port POP3 par défaut est 110.

exemple telnet :   
- l'utilisateur s'authentifie avec USER et PASS.   
- On utilise STAT pour avoir plus d'info : une réponse positive à STAT a le format +OK nn mm, où nn est le nombre de messages électroniques dans la boîte de réception et mm est la taille de la boîte de réception en octets (octet).   
- La commande LIST fourni une liste de nouveaux messages sur le serveur   
- RETR 1 récupéré le premier message de la liste.   

### IMAP

Le protocole IMAP (Internet Message Access Protocol) est plus sophistiqué que POP3. IMAP permet de synchroniser vos e-mails sur plusieurs appareils (et clients de messagerie). En d'autres termes, si vous marquez un e-mail comme lu lors de la vérification de votre messagerie sur votre smartphone, la modification sera enregistrée sur le serveur IMAP ( MDA ) et répliquée sur votre ordinateur portable lorsque vous synchroniserez votre boîte de réception. Le port par défaut IMAP est 143.

exemple telnet : l'utilisateur s'authentifie avec LOGIN username password. IMAP exige que chaque commande soit précédée d'une chaîne aléatoire pour pouvoir suivre la réponse. Nous avons donc ajouté c1, puis c2, et ainsi de suite. Ensuite, nous avons répertorié nos dossiers de messagerie en utilisant LIST "" "*", avant de vérifier si nous avons de nouveaux messages dans la boîte de réception en utilisant EXAMINE INBOX.

Il est clair qu'IMAP envoie les identifiants de connexion en clair. Toute personne observant le trafic réseau pourrait connaître le nom d'utilisateur et le mot de passe.

## SSH 

Secure Shell (SSH) a été créé pour fournir un moyen sécurisé d'administration du système à distance. En d'autres termes, il vous permet de vous connecter en toute sécurité à un autre système sur le réseau et d'exécuter des commandes sur le système distant. La sécrutité se fait via du chiffrement et des algorithme cryptographique, cela apporte des protections :  

- Vous pouvez confirmer l'identité du serveur distant  
- Les messages échangés sont chiffrés et ne peuvent être déchiffrés que par le destinataire prévu  
- Les deux côtés peuvent détecter toute modification dans les messages  

Pour utiliser SSH, vous avez besoin d'un serveur SSH et d'un client SSH. Le serveur SSH écoute sur le port 22 par défaut. 

Le client SSH peut s'authentifier en utilisant : soit un nom d'utilisateur et un mot de passe, soit ne clé privée et publique (après que le serveur SSH est configuré pour reconnaître la clé publique correspondante). 

### commandes 

`ssh username@MACHINE_IP` : pour se connecter à la machine_ip avec l'utilisateur user   

Notez que si c'est la première fois que nous nous connectons à ce système, nous devrons confirmer l'empreinte de la clé publique du serveur SSH pour éviter les attaques de l'homme du milieu ( MITM ).
   
Nous pouvons utiliser SSH pour transférer des fichiers en utilisant SCP (Secure Copy Protocol) basé sur le protocole SSH.   
`scp mark@MACHINE_IP:/home/mark/archive.txt ~` : récupérer des fichier, copiera un fichier nommé archive.txt du système distant situé dans /home/mark vers notre répertoire racine   
`scp backup.txt mark@MACHINE_IP:/home/mark/` : copiera le fichier backup.txt du système local vers le répertoire /home/mark/ du système distant.

FTP peut également être sécurisé à l'aide du protocole SSH qui est le protocole SFTP. Par défaut, ce service écoute sur le port 22, tout comme SSH.

# Attaque Renifler les paquets

L'attaque par reniflement fait référence à l'utilisation d'un outil de capture de paquets réseau pour collecter des informations sur la cible. Lorsqu'un protocole communique en clair, les données échangées peuvent être captées par un tiers pour être analysées. Une simple capture de paquets réseau peut révéler des informations, telles que le contenu des messages privés et les identifiants de connexion, si les données ne sont pas chiffrées en transit.

Une attaque par sniffing peut être menée à l'aide d'une carte réseau Ethernet (802.3). Il existe de nombreux programmes disponibles pour capturer les paquets réseau :    

- Tcpdump est un programme d'interface de ligne de commande (CLI) open source gratuit qui a été porté pour fonctionner sur de nombreux systèmes d'exploitation.  
- Wireshark est un programme d'interface utilisateur graphique (GUI) open source gratuit disponible pour plusieurs systèmes d'exploitation, dont Linux , macOS et MS Windows.   
- Tshark est une alternative CLI à Wireshark.   

Tout protocole utilisant une communication en clair est susceptible de subir ce type d'attaque. La seule condition pour que cette attaque réussisse est d'avoir accès à un système entre les deux systèmes communicants.

L'atténuation de cette attaque réside dans l'ajout d'une couche de chiffrement au-dessus de tout protocole réseau. En particulier, Transport Layer Security (TLS) a été ajouté à HTTP , FTP, SMTP, POP3, IMAP et bien d'autres. Pour l'accès à distance, Telnet a été remplacé par l'alternative sécurisée Secure Shell (SSH).

## tcpdump 

l'attaque necessite un accès au trafic réseau, par exemple via une écoute électronique ou un switch avec port mirroring. Alternativement, nous pouvons accéder au trafic échangé si nous lançons une attaque Man-in-the-Middle ( MITM ) réussie.   

`sudo tcpdump port 110 -A`   
sudo car il faut les privilèges root   
port 110 permet de limiter les paquets capturé à ceux qui sont échangés avec le serveur POP3 = un filtre   
-A afficher au format ASCII

L'atténuation de cette attaque réside dans l'ajout d'une couche de chiffrement au-dessus de tout protocole réseau. En particulier, Transport Layer Security (TLS) a été ajouté à HTTP , FTP, SMTP, POP3, IMAP et bien d'autres. Pour l'accès à distance, Telnet a été remplacé par l'alternative sécurisée Secure Shell (SSH).

# Resume

| Protocole | Port TCP | Application | Sécurité |
| :-------: | :------: | :---------: | :------: |
| FTP | 21 | Tranfert de fichier | données en clair |
| HTTP | 80 | World Wide Web | données en clair |
| IMAP | 143 | Email | données en clair |
| POP3 | 110 | Email | données en clair |
| SMTP | 25 | Email | données en clair |
| Telnet | 23 | Accès à distance | données en clair |
| FTPS | 990 | Tranfert de fichier | crypté |
| HTTPS | 443 | World Wide Web | crypté |
| IMAPS | 993 | Email | crypté |
| POP3S | 995 | Email | crypté |
| SMTPS | 465 | Email | crypté |
| SSH | 22 | Accès à distance | crypté |
| SFTP | 22 | Transfert de fichier | crypté |
