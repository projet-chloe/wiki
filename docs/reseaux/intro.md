# Introduction

## Qu'est ce que le réseau ?

Les réseaux sont simplement des choses connectées. Par exemple, votre cercle d'amitié : vous êtes tous connectés en raison d'intérêts, de passe-temps, de compétences et de genres similaires.

Les réseaux peuvent être trouvés dans tous les domaines de la vie :

- Le système de transport en commun d'une ville    
- Des infrastructures telles que le réseau électrique national pour l'électricité     
- Rencontrer et saluer vos voisins     
- Systèmes postaux pour l'envoi de lettres et de colis    

Mais plus précisément, en informatique, la mise en réseau est la même idée, juste dispersée sur des appareils technologiques. Prenez votre téléphone comme exemple ; la raison pour laquelle vous l'avez est d'accéder aux choses.

En informatique, un réseau peut être formé de 2 appareils à des milliards. Ces appareils incluent tout, de votre ordinateur portable et de votre téléphone aux caméras de sécurité, aux feux de circulation et même à l'agriculture !

Les réseaux sont intégrés à notre quotidien. Qu'il s'agisse de recueillir des données météorologiques, de fournir de l'électricité aux maisons ou même de déterminer qui a le droit de passage sur une route. Parce que les réseaux sont tellement ancrés dans la modernité, le réseautage est un concept essentiel à saisir dans la cybersécurité.

## Qu'est ce que l'internet ?

Maintenant que nous avons appris ce qu'est un réseau et comment il est défini en informatique (uniquement les appareils connectés), explorons Internet.

Internet est un réseau géant composé de très nombreux petits réseaux en son sein. 

La première itération d'Internet a eu lieu dans le cadre du projet ARPANET à la fin des années 1960. Ce projet a été financé par le Département de la Défense des États-Unis et a été le premier réseau documenté en action. Cependant, ce n'est qu'en 1989 que l'Internet tel que nous le connaissons a été inventé par Tim Berners-Lee avec la création du World Wide Web ( WWW ). Ce n'est qu'à ce moment-là qu'Internet n'a pas été utilisé comme référentiel pour stocker et partager des informations (comme c'est le cas aujourd'hui).

Comme indiqué précédemment, Internet est composé de nombreux petits réseaux tous reliés entre eux. Ces petits réseaux sont appelés réseaux privés, tandis que les réseaux connectant ces petits réseaux sont appelés réseaux publics ou Internet ! Donc, pour récapituler, un réseau peut être de deux types :

- Un réseau privé     
- Un réseau public    

Les appareils utiliseront un ensemble d'étiquettes pour s'identifier sur un réseau.

## Identification 

Pour communiquer et maintenir l'ordre, les appareils doivent être à la fois identifiants et identifiables sur un réseau. À quoi ça sert si vous ne savez pas à qui vous parlez à la fin de la journée ?

Les appareils sur un réseau sont très similaires aux humains dans le fait que nous avons deux façons d'être identifiés : notre nom et nos empreintes digitales.Maintenant, nous pouvons changer notre nom par acte de vote, mais nous ne pouvons cependant pas changer nos empreintes digitales. Chaque être humain possède un ensemble individuel d'empreintes digitales, ce qui signifie que même s'il change de nom, il y a toujours une identité derrière. 

Les appareils ont la même chose : deux moyens d'identification, dont un perméable. Ceux-ci sont: une adresse IP et une adresse MAC (Media Access Control )

### Adresses IP

En bref, une adresse IP (ou une adresse de protocole Internet ) peut être utilisée comme moyen d'identifier un hôte sur un réseau pendant un certain temps, où cette adresse IP peut ensuite être associée à un autre appareil sans que l'adresse IP ne change. 

Une adresse IP  est un ensemble de nombres divisés en quatre octets. La valeur de chaque octet se résumera à l'adresse IP de l'appareil sur le réseau. Ce nombre est calculé grâce à une technique connue sous le nom d'adressage IP et de sous-réseaux, mais c'est pour un autre jour. Ce qu'il est important de comprendre ici, c'est que les adresses IP peuvent changer d'un appareil à l'autre, mais ne peuvent pas être actives simultanément plus d'une fois au sein du même réseau.

Les adresses IP suivent un ensemble de normes connues sous le nom de protocoles. Ces protocoles sont l'épine dorsale de la mise en réseau et obligent de nombreux appareils à communiquer dans le même langage, ce que nous reviendrons une autre fois. Cependant, rappelons que les appareils peuvent être à la fois sur un réseau privé et public. L'endroit où ils se trouvent déterminera le type d'adresse IP dont ils disposent : une adresse IP publique ou privée.

Une adresse publique est utilisée pour identifier l'appareil sur Internet, tandis qu'une adresse privée est utilisée pour identifier un appareil parmi d'autres appareils.

Sur un réseau privé, les appareils pourront utiliser leurs adresses IP privées pour communiquer entre eux. Cependant, toutes les données envoyées sur Internet à partir de l'un ou l'autre de ces appareils seront identifiées par la même adresse IP publique. Les adresses IP publiques sont fournies par votre fournisseur de services Internet (ou FAI ) moyennant des frais mensuels (votre facture ! )

À mesure que de plus en plus d'appareils sont connectés, il devient de plus en plus difficile d'obtenir une adresse publique qui n'est pas déjà utilisée. Par exemple, Cisco, un géant de l'industrie dans le monde des réseaux, a estimé qu'il y aurait environ 50 milliards d'appareils connectés sur Internet d'ici la fin de 2021. (Cisco., 2021) . Entrez les versions d'adresse IP. Jusqu'à présent, nous n'avons discuté que d'une seule version du schéma d'adressage du protocole Internet connu sous le nom d'IPv4, qui utilise un système de numérotation de 2^32 adresses IP (4,29 milliards) - vous pouvez donc comprendre pourquoi il y a une telle pénurie !

IPv6 est une nouvelle itération du schéma d'adressage du protocole Internet pour aider à résoudre ce problème. Bien qu'il soit apparemment plus intimidant, il présente quelques avantages :

- Prend en charge jusqu'à 2 ^ 128 adresses IP (plus de 340 000 milliards), résolvant les problèmes rencontrés avec IPv4    
- Plus efficace grâce aux nouvelles méthodologies      

### Adresses MAC

Les appareils d'un réseau auront tous une interface réseau physique, qui est une carte à puce située sur la carte mère de l'appareil. Cette interface réseau se voit attribuer une adresse unique à l'usine où elle a été construite, appelée adresse MAC ( M edia A ccess C ontrol ). L' adresse MAC est un nombre hexadécimal à douze caractères ( un système de numérotation en base seize utilisé en informatique pour représenter les nombres ) divisé en deux et séparés par deux points. Ces deux-points sont considérés comme des séparateurs. Par exemple, a4:c3:f0:85:ac:2d . Les six premiers caractères représentent la société qui a créé l'interface réseau et les six derniers sont un numéro unique.

Cependant, une chose intéressante avec les adresses MAC est qu'elles peuvent être falsifiées ou "usurpées" dans un processus connu sous le nom d'usurpation d'identité. Cette usurpation se produit lorsqu'un appareil en réseau prétend s'identifier comme un autre en utilisant son adresse MAC. Lorsque cela se produit, cela peut souvent casser des conceptions de sécurité mal implémentées qui supposent que les appareils qui communiquent sur un réseau sont dignes de confiance. Prenez le scénario suivant : Un pare-feu est configuré pour autoriser toute communication vers et depuis l'adresse MAC de l'administrateur. Si un appareil devait prétendre ou "usurper" cette adresse MAC, le pare-feu penserait maintenant qu'il reçoit une communication de l'administrateur alors que ce n'est pas le cas.

Des lieux tels que les cafés, les cafés et les hôtels utilisent souvent le contrôle d'adresse MAC lors de l'utilisation de leur Wi-Fi "Invité" ou "Public". Cette configuration pourrait offrir de meilleurs services, c'est-à-dire une connexion plus rapide pour un prix si vous êtes prêt à payer les frais par appareil. 

## PING (ICMP)

Ping est l'un des outils réseau les plus fondamentaux à notre disposition. Ping utilise des paquets ICMP ( Internet Control Message Protocol ) pour déterminer les performances d'une connexion entre des périphériques, par exemple, si la connexion existe ou est fiable.

Le temps nécessaire pour que les paquets ICMP voyagent entre les appareils est mesuré par ping, comme dans la capture d'écran ci-dessous. Cette mesure est effectuée à l'aide du paquet d'écho d'ICMP, puis de la réponse d'écho d'ICMP du périphérique cible.

Les pings peuvent être effectués sur des appareils d'un réseau, tels que votre réseau domestique ou des ressources telles que des sites Web. Cet outil peut être facilement utilisé et est installé sur des systèmes d'exploitation (OS) tels que Linux et Windows. La syntaxe pour faire un ping simple est `ping` IP address or website URL .

## LAN

Au fil des ans, diverses conceptions de réseau ont été expérimentées et mises en œuvre. En référence à la mise en réseau, lorsque nous nous référons au terme "topologie", nous nous référons en fait à la conception ou à l'apparence du réseau à portée de main. Discutons ci-dessous des avantages et des inconvénients de ces topologies.

### Topologie en étoile

Le principe principal d'une topologie en étoile est que les périphériques sont connectés individuellement via un périphérique réseau central tel qu'un commutateur ou un concentrateur. Cette topologie est la plus courante aujourd'hui en raison de sa fiabilité et de son évolutivité, malgré son coût.

Toute information envoyée à un équipement dans cette topologie est envoyée via l'équipement central auquel il se connecte. Explorons ci-dessous certains de ces avantages et inconvénients de cette topologie :

Parce que plus de câblage et l'achat d'équipements réseau dédiés sont nécessaires pour cette topologie, elle est plus chère que toutes les autres topologies. Cependant, malgré le coût supplémentaire, cela offre des avantages significatifs. Par exemple, cette topologie est de nature beaucoup plus évolutive, ce qui signifie qu'il est très facile d'ajouter d'autres appareils à mesure que la demande pour le réseau augmente.

Malheureusement, plus le réseau évolue, plus la maintenance est nécessaire pour maintenir le réseau fonctionnel. Cette dépendance accrue vis-à-vis de la maintenance peut également rendre le dépannage des défauts beaucoup plus difficile. De plus, la topologie en étoile est toujours sujette aux pannes - quoique réduites. Par exemple, si le matériel centralisé qui connecte les appareils tombe en panne, ces appareils ne pourront plus envoyer ni recevoir de données. Heureusement, ces périphériques matériels centralisés sont souvent robustes.

### Topologie en bus

Ce type de connexion repose sur une connexion unique appelée câble de dorsale. Ce type de topologie est similaire à la feuille d'un arbre dans le sens où les dispositifs (feuilles) proviennent de l'endroit où se trouvent les branches sur ce câble.

Étant donné que toutes les données destinées à chaque appareil voyagent le long du même câble, il est très rapidement susceptible de devenir lent et goulot d'étranglement si les appareils de la topologie demandent simultanément des données. Ce goulot d'étranglement entraîne également un dépannage très difficile car il devient rapidement difficile d'identifier quel appareil rencontre des problèmes avec des données voyageant toutes sur le même itinéraire.

Cependant, cela dit, les topologies de bus sont l'une des topologies les plus faciles et les plus rentables à mettre en place en raison de leurs dépenses, telles que le câblage ou l'équipement réseau dédié utilisé pour connecter ces appareils.

Enfin, un autre inconvénient de la topologie en bus est qu'il y a peu de redondance en place en cas de panne. Cet inconvénient est dû au fait qu'il existe un seul point de défaillance le long du câble de la dorsale. En cas de rupture de ce câble, les appareils ne peuvent plus recevoir ni transmettre de données sur le bus.

### Topologie en anneau

La topologie en anneau (également connue sous le nom de topologie à jeton) présente certaines similitudes. Les appareils tels que les ordinateurs sont connectés directement les uns aux autres pour former une boucle, ce qui signifie qu'il y a peu de câblage requis et moins de dépendance à l'égard du matériel dédié, comme dans une topologie en étoile. 

Une topologie en anneau fonctionne en envoyant des données à travers la boucle jusqu'à ce qu'elles atteignent le périphérique de destination, en utilisant d'autres périphériques le long de la boucle pour transmettre les données. Fait intéressant, un appareil n'enverra les données reçues d'un autre appareil dans cette topologie que s'il n'en a pas à s'envoyer lui-même. Si l'appareil a des données à envoyer, il enverra d'abord ses propres données avant d'envoyer les données d'un autre appareil.

Étant donné que les données ne circulent que dans une seule direction dans cette topologie, il est assez facile de dépanner les défauts qui surviennent. Cependant, il s'agit d'une épée à double tranchant, car ce n'est pas un moyen efficace de transmettre les données sur un réseau, car il peut être nécessaire de visiter plusieurs appareils avant d'atteindre l'appareil prévu.

Enfin, les topologies en anneau sont moins sujettes aux goulots d'étranglement, comme dans une topologie en bus, car de grandes quantités de trafic ne traversent pas le réseau à un moment donné. La conception de cette topologie signifie cependant qu'un défaut tel qu'un câble coupé ou un périphérique cassé entraînera la rupture de l'ensemble du réseau. 

## commutateur 

Les commutateurs sont des périphériques dédiés au sein d'un réseau qui sont conçus pour regrouper plusieurs autres périphériques tels que des ordinateurs, des imprimantes ou tout autre périphérique compatible réseau utilisant Ethernet. Ces différents appareils se branchent sur le port d'un commutateur. Les commutateurs se trouvent généralement dans les grands réseaux tels que les entreprises, les écoles ou les réseaux de taille similaire, où il existe de nombreux appareils à connecter au réseau. Les commutateurs peuvent connecter un grand nombre d'appareils en ayant des ports de 4, 8, 16, 24, 32 et 64 pour les appareils à brancher.

Les commutateurs sont beaucoup plus efficaces que leurs équivalents moindres (hubs/répéteurs). Les commutateurs gardent une trace de quel appareil est connecté à quel port. De cette façon, lorsqu'ils reçoivent un paquet, au lieu de répéter ce paquet sur chaque port comme le ferait un concentrateur, il l'envoie simplement à la cible prévue, réduisant ainsi le trafic réseau.

Les commutateurs et les routeurs peuvent être connectés les uns aux autres. La possibilité de le faire augmente la redondance (la fiabilité) d'un réseau en ajoutant plusieurs chemins pour les données à emprunter. Si un chemin descend, un autre peut être utilisé. Bien que cela puisse réduire les performances globales d'un réseau car les paquets doivent prendre plus de temps pour voyager, il n'y a pas de temps d'arrêt - un petit prix à payer compte tenu de l'alternative.

## routeur 

C'est le travail d'un routeur de connecter des réseaux et de transmettre des données entre eux. Pour ce faire, il utilise le routage (d'où le nom de routeur !).

Le routage est l'étiquette donnée au processus de transmission des données sur les réseaux. Le routage consiste à créer un chemin entre les réseaux afin que ces données puissent être livrées avec succès.

Le routage est utile lorsque les périphériques sont connectés par de nombreux chemins.

## Sous réeaux

Les réseaux peuvent être trouvés dans toutes les formes et tailles - allant du plus petit au plus grand. Le sous-réseautage est le terme donné à la division d'un réseau en réseaux miniatures plus petits à l'intérieur de lui-même. Pensez-y comme couper un gâteau pour vos amis. Il n'y a qu'une certaine quantité de gâteau à distribuer, mais tout le monde en veut un morceau. Le sous-réseau vous permet de décider qui obtient quelle tranche et de réserver une telle tranche de ce gâteau métaphorique.

Les administrateurs réseau utilisent les sous-réseaux pour catégoriser et attribuer des parties spécifiques d'un réseau afin de refléter cela.

La création de sous-réseaux est obtenue en divisant le nombre d'hôtes pouvant tenir dans le réseau, représenté par un nombre appelé masque de sous-réseau. 

Pour rappel, une adresse IP est composée de quatre sections appelées octets. Il en va de même pour un masque de sous-réseau qui est également représenté par un nombre de quatre octets (32 bits), allant de 0 à 255 (0-255).

Les sous-réseaux utilisent les adresses IP de trois manières différentes :

- Identifier l'adresse réseau : cette adresse identifie le début du réseau réel et est utilisée pour identifier l'existence d'un réseau.    
- Identifier l'adresse de l'hote : une adresse IP ici est utilisée pour identifier un périphérique sur le sous-réseau     
- Identifier la passerelle par défaut : l'adresse de la passerelle par défaut est une adresse spéciale attribuée à un appareil sur le réseau qui est capable d'envoyer des informations à un autre réseau      

## ARP 

Le protocole ARP ou A ddress Resolution Protocol en abrégé , est la technologie qui permet aux appareils de s'identifier sur un réseau.

Simplement, le protocole ARP permet à un appareil d'associer son adresse MAC à une adresse IP sur le réseau. Chaque appareil sur un réseau conservera un journal des adresses MAC associées à d'autres appareils.

Lorsque des appareils souhaitent communiquer avec un autre, ils enverront une diffusion à l'ensemble du réseau à la recherche de l'appareil spécifique. Les appareils peuvent utiliser le protocole ARP pour trouver l'adresse MAC (et donc l'identifiant physique) d'un appareil pour la communication.

Comment fonctionne ARP ?

Chaque appareil au sein d'un réseau dispose d'un registre pour stocker des informations, appelé cache. Dans le cadre du  protocole ARP  , ce cache stocke les identifiants des autres équipements du réseau.

Afin de mapper ces deux identifiants ensemble (adresse IP et adresse MAC ), le protocole ARP envoie deux types de messages : demande ARP et réponse ARP.     

Lorsqu'une requête ARP est envoyée, un message est diffusé à tous les autres appareils trouvés sur un réseau par l'appareil, demandant si l' adresse MAC de l'appareil correspond ou non à l'adresse IP demandée. Si l'appareil possède l'adresse IP demandée, une réponse ARP est renvoyée à l'appareil initial pour en accuser réception. L'appareil initial va maintenant s'en souvenir et le stocker dans son cache (une entrée ARP ). 

## DHCP

Les adresses IP peuvent être attribuées soit manuellement, en les saisissant physiquement dans un appareil, soit automatiquement et le plus souvent en utilisant un serveur DHCP ( protocole de configuration dynamique de l'hôte ). Lorsqu'un appareil se connecte à un réseau, s'il n'a pas déjà été manuellement attribué une adresse IP, il envoie une demande (DHCP Discover) pour voir si des serveurs DHCP sont sur le réseau. Le serveur DHCP répond ensuite avec une adresse IP que l'appareil pourrait utiliser (offre DHCP). L'appareil envoie ensuite une réponse confirmant qu'il veut l'adresse IP offerte (requête DHCP), puis enfin, le serveur DHCP envoie une réponse confirmant que cela a été effectué, et l'appareil peut commencer à utiliser l'adresse IP (DHCP ACK).

