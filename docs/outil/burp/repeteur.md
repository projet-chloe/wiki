# REPEATER

## Introduction

Burp Suite Repeater nous permet de créer et/ou de relayer les requêtes interceptées vers une cible à volonté. En termes simples, cela signifie que nous pouvons prendre une requête capturée dans le proxy, la modifier et envoyer la même requête à plusieurs reprises autant de fois que nous le souhaitons. Alternativement, nous pourrions créer des requêtes à la main, un peu comme nous le ferions en utilisant un outil tel que cURL pour créer et envoyer des requêtes.

Cette capacité à modifier et à renvoyer la même requête plusieurs fois rend Repeater idéal pour tout type de recherche manuelle sur un point de terminaison, nous fournissant une interface utilisateur graphique agréable pour écrire la charge utile de la requête et de nombreuses vues (y compris un moteur de rendu pour une vue graphique) de la réponse afin que nous puissions voir les résultats de notre travail manuel en action.

## Interface

1. Tout en haut à gauche de l'onglet, nous avons une liste de demandes de répéteur. Nous pouvons avoir de nombreuses requêtes différentes passant par Repeater : chaque fois que nous envoyons une nouvelle requête à Repeater, elle apparaîtra ici. 

2. Directement sous la liste des requêtes, nous avons les contrôles pour la requête en cours. Ceux-ci nous permettent d'envoyer une demande, d'annuler une demande suspendue et d'avancer/reculer dans l'historique des demandes.  

3. Toujours sur le côté gauche de l'onglet, mais occupant la majeure partie de la fenêtre, nous avons la vue de la demande et de la réponse.  

4. Au-dessus de la section requête/réponse, sur le côté droit, se trouve un ensemble d'options nous permettant de modifier la disposition des vues de requête et de réponse.   

5. Sur le côté droit de la fenêtre, nous avons l'inspecteur, qui nous permet de séparer les requêtes pour les analyser et les éditer de manière un peu plus intuitive qu'avec l'éditeur brut. Il nous donne une liste des composants de la requête et de la réponse  et complète entièrement les champs de demande et de réponse de la fenêtre du répéteur.  
    - request Attributes : les parties de la requête qui traitent de l'emplacement, de la méthode et du protocole  
    - Query Parameters : référence aux données envoyées au serveur dans l'URL.  
    - Body Parameters : la même chose que Query Parameters, mais pour les requêtes POST.  
    - Request Cookies :  liste modifiable des cookies qui sont envoyés avec chaque demande  
    - Request Headers : permettent de visualiser, d'accéder et de modifier l'un des en-têtes envoyés avec nos demandes. Leur modification peut être très utile lorsque vous essayez de voir comment un serveur Web répondra à des en-têtes inattendus.  
    - Response Headers : montrent les en-têtes que le serveur a renvoyés en réponse à notre requête. Ceux-ci ne peuvent pas être modifiés ! Notez que cette section n'apparaîtra qu'après que nous ayons envoyé la demande et reçu une réponse.  

6. Enfin, au-dessus de l'inspecteur, nous avons notre cible. Il s'agit tout simplement de l'adresse IP ou du domaine auquel nous envoyons les requêtes.

## Utilisation

Le répéteur est le mieux adapté au type de tâche où nous devons envoyer la même demande plusieurs fois, généralement avec de petits changements entre les demandes.   

1. Avec une demande capturée dans le proxy, nous pouvons envoyer au répéteur soit en cliquant avec le bouton droit sur la demande et en choisissant "Envoyer au répéteur" ou en appuyant sur Ctrl + R.

2. En revenant à Repeater, nous pouvons voir que notre requête est désormais disponible. Les éléments cible et inspecteur affichent désormais également des informations ; cependant, nous n'avons pas encore de réponse. 

3. Lorsque nous cliquons sur le bouton "Envoyer", la section Réponse se remplit rapidement. Repeater nous propose différentes manières de présenter les réponses à nos demandes.  
    - Pretty : option par défaut. Il prend la réponse brute et tente de l'embellir légèrement, ce qui la rend plus facile à lire.  
    - Raw : la réponse pure et non embellie du serveur.  
    - Hex : cette vue prend la réponse brute et nous en donne une vue en octets (utile si la réponse est un fichier binaire)   
    - Rendu : la vue de rendu rend la page telle qu'elle apparaîtrait dans votre navigateur. 
    - Le bouton "Afficher les caractères non imprimables" (\n). Ce bouton nous permet d'afficher des caractères qui n'apparaîtraient généralement pas dans les vues Jolie ou Brute.  

4. Si nous voulons changer quoi que ce soit à propos de la demande, nous pouvons simplement taper dans la fenêtre de demande et appuyer à nouveau sur "Envoyer" ; cela mettra à jour la réponse sur la droite. Nous pouvons aussi utiliser Inspector pour modifier notre requete.  

5. Nous pouvons également utiliser les boutons d'historique à droite du bouton Envoyer pour avancer et reculer dans notre historique des modifications.

## Astuce

Utiliser le repeater pour tester des injection sql  