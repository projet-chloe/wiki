# Extender

les aspects modulaires de Burp Suite : la fonctionnalité exposée, qui permet aux développeurs de créer des modules supplémentaires supplémentaires pour le framework.  

## Interface

La vue par défaut dans l'interface Extender nous donne un aperçu des extensions que nous avons chargées dans Burp Suite.   

La première case (vers le haut de l'interface) nous fournit une liste des extensions que nous avons installées et nous permet de les activer ou de les désactiver pour ce projet.  

Les options à gauche de cette case nous permettent de désinstaller les extensions avec le bouton Supprimer ou d'en installer de nouvelles à partir de fichiers sur notre disque avec le bouton Ajouter. Il peut s'agir de modules que nous avons codés ou de modules qui ont été mis à disposition sur Internet mais qui ne se trouvent pas dans la boutique BApp.   
Les boutons Haut et Bas de cette section contrôlent l'ordre dans lequel les extensions installées sont répertoriées. Les extensions sont appelées dans l'ordre décroissant en fonction de cette liste. En d'autres termes : tout le trafic passant par Burp Suite sera acheminé par chaque extension dans l'ordre, en commençant par le haut de la liste et en descendant. Cela peut être très important lorsqu'il s'agit d'extensions qui modifient les demandes, car certaines peuvent se contrecarrer ou se gêner d'une autre manière.  

Vers le bas de la fenêtre, nous avons les détails, la sortie et les erreurs pour le module actuellement sélectionné. Ceux-ci peuvent être utilisés pour afficher les informations du module, ainsi que pour le débogage.   

## Store

Le Burp App Store (ou BApp Store en abrégé) nous permet de répertorier facilement les extensions officielles et de les intégrer de manière transparente à Burp Suite. Les extensions peuvent être écrites dans une variété de langages - le plus souvent Java (qui s'intègre automatiquement dans le framework) ou Python (qui nécessite l'interpréteur Jython).  

Exemple : Le minuteur de requêteextension (Écrit par Nick Taylor) nous permet d'enregistrer le temps que chaque demande que nous envoyons prend pour recevoir une réponse ; cela peut être extrêmement utile pour découvrir la présence (et exploiter) des vulnérabilités temporelles. Par exemple, si un formulaire de connexion prend une seconde de plus pour traiter les demandes contenant un nom d'utilisateur valide que pour les comptes qui n'existent pas, nous pouvons rapidement générer une liste de noms d'utilisateur possibles et utiliser la différence de temps pour voir quels noms d'utilisateur sont valide.   
--> autre exemple : Le Logger++ pour une fonctionnalité de journalisation étendue est un très bon choix !   

Comment faire ?   
Passez au sous-onglet "BApp Store", puis recherchez "Request Timer". Il ne devrait y avoir qu'un seul résultat. Cliquez sur l'extension renvoyée, puis cliquez sur "Installer".   
Notez qu'un nouvel onglet est apparu dans le menu principal en haut de l'écran. Différentes extensions ont des comportements différents : certaines ajoutent simplement un nouvel élément aux menus contextuels du clic droit ; d'autres créent des onglets entièrement nouveaux dans la barre de menu principale.   

## Module Python

Si nous voulons utiliser des modules Python dans Burp Suite, nous devons avoir téléchargé et inclus le fichier JAR Jython Interpreter séparé. L'interpréteur Jython est une implémentation Java de Python. Le site Web nous donne la possibilité d'installer Jython sur notre système ou de le télécharger en tant qu'archive Java autonome (JAR). Nous en avons besoin en tant qu'archive autonome pour l'intégrer à Burp.   

Remarque : nous pouvons faire la même chose avec les modules Ruby et l'intégration JRuby   

1. Tout d'abord, nous devons télécharger une copie à jour de l'archive Jython JAR à partir du [site Web de Jython] (https://www.jython.org/download). Nous recherchons l'option Jython Standalone.   
2. Enregistrez le fichier JAR quelque part sur votre disque   
3. Puis passez au sous-onglet "Options" dans Extender.  
4. Faites défiler jusqu'à la section "Environnement Python" et définissez "Emplacement du fichier JAR autonome Jython" sur le chemin de l'archive   

## API

Extender expose un grand nombre de points de terminaison API auxquels les nouveaux modules peuvent se connecter lors de l'intégration à Burp Suite.

Nous pouvons les voir dans le sous-onglet "APIs".   

Chaque élément de la liste à gauche de ce sous-onglet documente un point de terminaison d'API différent, qui peuvent tous être appelés à partir des extensions. Les points de terminaison ici donnent aux développeurs beaucoup de puissance lors de l'écriture d'extensions pour interagir de manière transparente avec les fonctionnalités existantes de Burp Suite. Comme vous vous en doutez, nous pouvons interagir avec ces points de terminaison dans n'importe lequel des langages pris en charge par Burp Suite pour une utilisation dans les extensions : Java (en mode natif), Python (via Jython) et Ruby (via JRuby).

Pour plus d'info, PortSwigger fournit une merveilleuse référence qui peut être trouvée [ici] (https://portswigger.net/burp/extender/writing-your-first-burp-suite-extension).   