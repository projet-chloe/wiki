
# PROXY

Le Burp Proxy est le plus fondamental (et le plus important !) des outils disponibles dans Burp Suite. Il nous permet de capter les demandes et les réponses entre nous et notre cible. Ceux-ci peuvent ensuite être manipulés ou envoyés à d'autres outils pour un traitement ultérieur avant d'être autorisés à continuer vers leur destination.   
Cette capacité à intercepter les requêtes signifie en fin de compte que nous pouvons prendre le contrôle total de notre trafic Web - une capacité inestimable lorsqu'il s'agit de tester des applications Web.   

## Options

- Interception   
Lorsque "l'intercept is on" est activé, le trafic est capturé par Burp. La requete s'affiche donc. Nous pouvons alors choisir de transférer ou de supprimer la demande (éventuellement après l'avoir modifiée). Nous pouvons également faire diverses autres choses ici, comme envoyer la requête à l'un des autres modules Burp, la copier en tant que commande cURL, l'enregistrer dans un fichier, et bien d'autres.

- HTTP history et WebSockets history
Burp Suite enregistrera toujours (par défaut) les requêtes effectuées via le proxy lorsque l'interception est désactivée. Cela peut être très utile pour revenir en arrière et analyser les demandes antérieures, même si nous ne les avons pas saisies spécifiquement lorsqu'elles ont été faites.  

Burp capturera et enregistrera également la communication WebSocket, ce qui, encore une fois, peut être extrêmement utile lors de l'analyse d'une application Web.   

- Options   
Ces options nous donnent beaucoup de contrôle sur le fonctionnement du proxy. 
Par exemple, le proxy n'interceptera pas les réponses du serveur par défaut. Nous pouvons donc remplacer le paramètre par défaut en cochant la case "Intercepter les réponses en fonction des règles suivantes" et en choisissant une ou plusieurs règles. La "Or Request Was Intercepted" règle est utile pour intercepter les réponses à toutes les requêtes qui ont été interceptées par le proxy.  
La section "Match and Replace" permet d'effectuer des regex sur les requêtes entrantes et sortantes. Par exemple, vous pouvez modifier automatiquement votre agent utilisateur pour émuler un navigateur Web différent dans les requêtes sortantes ou supprimer tous les cookies définis dans les requêtes entrantes.   
De plus, Vous pouvez créer vos propres règles pour la plupart des options de proxy.  

## Configuration

1. Proxy  
Le Burp Proxy fonctionne en ouvrant une interface web sur 127.0.0.1:8080(par défaut). Comme l'implique le fait qu'il s'agit d'un "proxy", nous devons rediriger tout le trafic de notre navigateur via ce port avant de pouvoir commencer à l'intercepter avec Burp.  Pour cela, utiliser l'extension FoxyProxy.  

2. Certificat : Si nous cosnultons un site avec TLS activé, nous obtenons uen erreur. Firefox nous dit que l'autorité de certification Portswigger ( CA ) n'est pas autorisée à sécuriser la connexion. nous allons donc ajouter manuellement le certificat CA à notre liste d'autorités de certification de confiance. 
    - Tout d'abord, avec le proxy activé, dirigez-vous vers http://burp/cert ; cela téléchargera un fichier appelé cacert.der, enregistrez-le quelque part sur votre machine.
    - Importer ce certificat sur votre navigateur. Indiquez faire confiane à cette autorité de certification.

3. Browser intégré : Burp Suite comprend également un navigateur Chromium intégré qui est préconfiguré pour utiliser le proxy sans aucune des modifications que nous devions faire. Nous pouvons démarrer le navigateur Burp avec le bouton "Ouvrir le navigateur" dans l'onglet proxy. Si on est connecté ent ant qu'utilisateur root, Burp Suite est incapable de créer un environnement sandbox pour démarrer le navigateur Burp, ce qui provoque une erreur et sa mort. Il existe deux solutions simples à cela :  
    - créer un nouvel utilisateur et exécuter Burp Suite sous un compte à faibles privilèges.
    - aller à Project options -> Misc -> Embedded Browser et vérifier l'option Allow the embedded browser to run without a sandbox. Cocher cette option permettra au navigateur de démarrer, mais sachez qu'il est désactivé par défaut pour des raisons de sécurité.  

## Target

### Présentation

Il y a 3 sous onglets :  

- Site Map : permet de cartographier les applications que nous ciblons dans une arborescence. Chaque page que nous visitons s'affichera ici, ce qui nous permettra de générer automatiquement un plan du site pour la cible simplement en parcourant l'application Web. Burp Pro nous permettrait également d'araignéer les cibles automatiquement (c'est-à-dire de parcourir chaque page à la recherche de liens).  
Le plan du site peut être particulièrement utile si nous voulons cartographier une API, car chaque fois que nous visitons une page, tous les points de terminaison de l'API dont la page récupère les données lors du chargement s'afficheront ici.   
- Scope : permet de contrôler la portée cible de Burp pour le projet.  
- Issues definitions : donne une énorme liste de vulnérabilités Web (avec des descriptions et des références) à partir de laquelle nous pouvons puiser si nous avons besoin de citations pour un rapport ou d'aide décrivant une vulnérabilité. 

### Utilisation
Cela peut devenir extrêmement fastidieux d'avoir Burp capturant tout notre trafic. Lorsqu'il enregistre tout (y compris le trafic vers des sites que nous ne ciblons pas), il brouille les journaux que nous souhaiterons peut-être envoyer ultérieurement aux clients.   

Définir une portée pour le projet nous permet de définir ce qui est proxy et enregistré. Nous pouvons restreindre Burp Suite pour cibler uniquement la ou les applications Web que nous voulons tester. 

Comment ?   

1. La façon la plus simple de le faire est de passer à l'onglet "Target", de cliquer avec le bouton droit sur notre cible dans notre liste de gauche dans le sous onglet 'Site Map", puis de choisir "Add to scope". Burp nous demandera alors si nous voulons arrêter de consigner tout ce qui n'est pas dans la portée, nous choisissons oui.  
2. Nous pouvons maintenant vérifier notre portée en passant au sous-onglet "Scope"   
3. Nous avons simplement choisi de désactiver la journalisation pour le trafic hors de portée, mais le proxy interceptera toujours tout. Pour désactiver cela, nous devons aller dans le sous-onglet Options de proxy et sélectionner "And URL Is in target scope" dans la section Intercept Client Requests.  
Avec cette option sélectionnée, le proxy ignorera complètement tout ce qui n'est pas dans la portée, nettoyant considérablement le trafic passant par Burp.

## Astuce 

- encoder nos charges utiles en URL avec le Ctrl + U pour sécuriser l'envoie.