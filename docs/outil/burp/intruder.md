# INTRUDER

## Introduction 

Intruder est l'outil de fuzzing intégré à Burp Suite. Cela nous permet de prendre une requête et de l'utiliser comme modèle pour envoyer automatiquement de nombreuses autres requêtes avec des valeurs légèrement modifiées. Cette fonctionnalité est très similaire à celle fournie par les outils en ligne de commande tels que Wfuzz ou Ffuf.  

Bref, en tant que méthode d'automatisation des requêtes, Intruder est extrêmement puissant -- il n'y a qu'un seul problème : pour accéder à toute la vitesse d'Intruder, nous avons besoin de Burp Professional. Nous pouvons toujours utiliser Intruder avec Burp Community, mais le débit est fortement limité.  

Il existe 5 sous-onglets :  

- Target : permet d choisir notre cible. Si nous avons envoyé uen requete à partir du proxy cette partie est déjà remplie.  
- Positions : permettent de sélectionner un type d'attaque, ainsi que de configurer où dans le modèle de demande nous souhaitons insérer nos charges utiles.  
- Payloads : permettent de sélectionner des valeurs à insérer dans chacune des positions que nous avons définies. Par exemple, nous pouvons choisir de charger des éléments à partir d'une liste de mots pour servir de charges utiles. Ce sous onglet nous permet également de modifier le comportement d'Intruder en ce qui concerne les charges utiles ; par exemple, nous pouvons définir des règles de prétraitement à appliquer à chaque charge utile.  
- Ressource Pool : permet de répartir nos ressources entre les tâches, principalement avec Burp Pro car on peut exécuter divers types de tâches automatisées en arrière-plan.  
- Options : permet de configurer le comportement d'attaque. Les paramètres ici s'appliquent principalement à la façon dont Burp gère les résultats et à la façon dont Burp gère l'attaque elle-même.  

## Type d'attaque

Dans l'onglet Positions, nous pouvons choisir le type d'attaques avec un menu déroulants.   

1. Tireur d'élite / Snipper  
Le tireur d'élite est le premier type d'attaque et le plus courant.  
Lors d'une attaque de tireur d'élite, nous fournissons un ensemble de charges utiles, par exmeple une liste de mot. Intruder prendra chaque charge utile et la placera dans chaque position définie à tour de rôle. Il commence par la première position et essaie chacune de nos charges utiles, puis passe à la deuxième position et essaie à nouveau les mêmes charges utiles.   
Nous pouvons calculer le nombre de requêtes que fera Intruder Sniper : requests = numberOfWords * numberOfPositions.  
--> Cette qualité rend Sniper très bon pour les attaques à position unique.  

2. Bélier / Battering Ram  
Il prend aussi un ensemble de charges utiles. Contrairement au Sniper, le Bélier met la même charge utile dans chaque position plutôt que dans chaque position à tour de rôle. Chaque élément de notre liste de charges utiles est placé dans chaque position pour chaque requête en même temps. Le nombre de requete correspond à numberOfWords.  

3. Fourche / PitchFork  
Pitchfork est le type d'attaque que vous êtes le plus susceptible d'utiliser. Il peut être utile de penser à Pitchfork comme si de nombreux tireurs d'élite s'exécutaient simultanément. Là où Sniper utilise UN SEUL ensemble de charges utiles (qu'il utilise simultanément sur toutes les positions), Pitchfork utilise un ensemble de charges utiles par position (jusqu'à un maximum de 20) et les parcourt tous en même temps. Idéalement, nos ensembles de charges utiles devraient être de longueurs identiques lorsque vous travaillez dans Pitchfork, car Intruder arrêtera les tests dès que l'une des listes sera épuisé. Le nombre de requete correspond à numberOfWords.  

4. Bombe à fragmentation / Cluster Bomb  
La bombe à fragmentation nous permet de choisir plusieurs ensembles de charges utiles : un par position, jusqu'à un maximum de 20 ; cependant, alors que Pitchfork parcourt simultanément chaque ensemble de charges utiles, la bombe à fragmentation parcourt chaque ensemble de charges utiles individuellement, en s'assurant que chaque combinaison possible de charges utiles est testée. Ce type d'attaque peut créer une énorme quantité de trafic. Le nombre de requete est égale au nombre de lignes dans chaque ensemble de charges utiles multipliées ensemble.  

## Utilisation

### Positions

Lorsque nous cherchons à effectuer une attaque avec Intruder, la première chose que nous devons faire est de regarder les positions. Les positions indiquent à Intruder où insérer les charges utiles.  

Burp tentera de déterminer les endroits les plus probables où nous pourrions souhaiter insérer automatiquement une charge utile - ceux-ci sont surlignés en vert et entourés de silcrows (§).  

Sur le côté droit de l'interface, nous avons les boutons :   

- Add § : permet de définir de nouveaux postes en les mettant en surbrillance dans l'éditeur et en cliquant sur le bouton.  
- Clear § : supprime toutes les positions définies, nous laissant avec une toile vierge pour définir la nôtre.  
- Auto § : tente de sélectionner automatiquement les positions les plus probables  

### Payload

Il est divisé en 4 sections :   

- Payloads Set : permet de choisir la position pour laquelle nous voulons configurer un ensemble ainsi que le type de charge utile que nous aimerions utiliser.  
- Payload Options : permet de spécifier les données de la charge utile, diffèrent selon le type de charge utile que nous sélectionnons pour l'ensemble de charge utile actuel.  
- Payload Processing : permet de définir des règles à appliquer à chaque charge utile de l'ensemble avant d'être envoyées à la cible. Par exemple, nous pourrions mettre en majuscule chaque mot ou ignorer la charge utile si elle correspond à une expression régulière.  
- Payload Encoding : permet de remplacer les options de codage d'URL par défaut qui sont appliquées automatiquement pour permettre la transmission en toute sécurité de notre charge utile  

Lorsqu'elles sont combinées, ces sections nous permettent d'adapter parfaitement nos ensembles de charges utiles à toute attaque que nous souhaitons mener.  

## Macro pour CSRF

Maintenant il y a aussi un cookie de session défini dans la réponse, ainsi qu'un jeton CSRF inclus dans le formulaire en tant que champ caché. Si nous actualisons la page, nous devrions voir que ces deux éléments changent à chaque demande : cela signifie que nous devrons extraire des valeurs valides pour les deux à chaque fois que nous ferons une demande. En d'autres termes, chaque fois que nous tentons de nous connecter, nous aurons besoin de valeurs uniques.    

Pour cela, nous devons définir une "macro" (c'est-à-dire un ensemble court d'actions répétées) à exécuter avant chaque requête. Cela saisira un cookie de session unique et un jeton de connexion correspondant, puis les remplacera dans chaque demande de notre attaque.  

1. Capturer la requete et définir le type d'attack souhaité ainsi que les positions et les charges utiles voulu  

2. Configuration de la macro. 
    - Aller à l'onglet Project Option puis au sous onglet Sessions   
    - Faites défiler vers le bas du sous-onglet jusqu'à la section "Macros" et cliquez sur le bouton "Ajouter".  
    - Le menu qui apparaît nous montrera l'historique de nos demandes. S'il n'y a pas encore de demande souhaité (qu'on veut brute forcer) accédez à cet emplacement dans votre navigateur et vous devriez voir une demande appropriée apparaître dans la liste.  
    - Cliquez sur la demande puis sur OK.  
    - Enfin, donnez à la macro un nom approprié, puis cliquez à nouveau sur "Ok" pour terminer le processus.  

3. Définition des règles de gestion de session qui définissent comment la macro doit être utilisée  
    - Toujours dans le sous-onglet "Sessions" des Options du projet, faites défiler jusqu'à la section "Règles de gestion de session" et choisissez "Ajouter" une nouvelle règle.  
    - Une nouvelle fenêtre apparaîtra avec deux onglets : "Détails" et "Scope".  
    - Remplissez une description appropriée, puis passez à l'onglet Scope.  
    - Dans la section "Tools Scope", décochez toutes les cases autres que Intruder (nous n'avons pas besoin que cette règle s'applique ailleurs)  
    - Dans la section "URL Scope", choisissez "Use suite scope" ; cela définira la macro pour qu'elle ne fonctionne que sur les sites qui ont été ajoutés au scope global (onglet Target). Si vous n'avez pas défini de portée globale, conservez l'option "Use custom scope" par défaut et ajoutez votre url à la portée dans cette section.  
    - Revenez maintenant à l'onglet Détails et consulter la section Rules actions  
    - Cliquez sur le bouton "Ajouter" - cela fera apparaître un menu déroulant avec une liste d'actions que nous pouvons ajouter.  
    - Sélectionnez "Exécuter une macro" dans cette liste.  
    - Dans la nouvelle fenêtre qui apparaît, sélectionnez la macro que nous avons créée précédemment.  
    - Sélectionnez "Mettre à jour uniquement les paramètres suivants", puis cliquez sur le bouton "Edit" à côté de la zone de saisie.  
    - Dans le champ de texte "Enter e new item", saisissez "loginToken". Appuyez sur "Ajouter", puis sur "Fermer".  
    - Sélectionnez "Mettre à jour uniquement les cookies suivants", puis cliquez sur le bouton "Modifier" correspondant.  
    - Entrez « session » dans le champ de texte, appuyez sur « Ajouter », puis sur « Fermer ».
    - Enfin, appuyez sur "Ok" pour confirmer notre action.  

Vous devriez maintenant avoir défini une macro qui remplacera le jeton CSRF et le cookie de session. Tout ce qu'il reste à faire est de revenir à Intruder et de lancer l'attaque !  

## Astuce

Une fois l'attaque terminée, une nouvelle fenêtre nous présentera les résultats. Cependant, il est difficile de savoir quelles lignes nous interessent.  

Pour cela, nous pouvons trier les resultats. 

- selon le status de la réponse pour identifier les connexions réussies  
- selon la longueur des réponses et regarder celle qui sortent du lots