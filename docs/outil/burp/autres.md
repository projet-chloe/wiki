# Autres modules

## Decoder

Le module Burp Suite Decoder nous permet de manipuler des données. Comme son nom l'indique, nous pouvons décoder les informations que nous capturons lors d'une attaque, mais nous pouvons également encoder nos propres données, prêtes à être envoyées à la cible. Decoder nous permet également de créer des hashsums de données, ainsi que de fournir une fonction Smart Decode qui tente de décoder les données fournies de manière récursive jusqu'à ce qu'elles redeviennent du texte en clair (comme la fonction "Magic" de Cyberchef ).  

L'interface de l'onglet Decoder nous offre un certain nombre d'options :  

1. La boîte de gauche est l'endroit où nous collerions ou taperions le texte à encoder ou à décoder. Nous pouvons également envoyer des données ici à partir d'autres sections du framework en cliquant avec le bouton droit de la souris et en choisissant Envoyer au décodeur.  
2. Nous avons la possibilité de choisir entre traiter l'entrée comme du texte ou des valeurs d'octets hexadécimaux en haut de la liste à droite. 
--> La saisie de données au format ASCII est excellente, mais nous devons parfois pouvoir modifier notre entrée octet par octet. Pour cela, nous pouvons utiliser "Hex View" en choisissant "Hex" au-dessus des options de décodage  
3. Plus bas dans la liste, nous avons des menus déroulants pour encoder , décoder ou hacher l'entrée.  
4. Enfin, nous avons la fonction "Smart Decode", qui tente de décoder automatiquement l'entrée.  
--> Cette fonctionnalité est loin d'être parfaite, mais elle peut être très utile pour décoder rapidement des morceaux de données inconnues.   

Au fur et à mesure que nous ajoutons des données dans le champ d'entrée, l'interface se dupliquera pour contenir la sortie de notre transformation. On peut alors choisir d'opérer dessus en utilisant les mêmes options.  

### Méthodes de décodage/encodage 

- Plain : le texte en clair est ce que nous avons avant d'effectuer toute transformation.  
- URL : le codage d'URL est utilisé pour sécuriser le transfert des données dans l'URL d'une requête Web. Il s'agit d'échanger des caractères contre leur code de caractère ASCII au format hexadécimal, précédé d'un symbole de pourcentage ( %). L'encodage d'URL est une méthode extrêmement utile à connaître pour tout type de test d'application Web.  
- HTML : l'encodage de texte en tant qu'entités HTML implique le remplacement des caractères spéciaux par une esperluette (&) suivie d'un nombre hexadécimal ou d'une référence au caractère échappé, puis d'un point-virgule (;). Cette méthode d'encodage permet aux caractères spéciaux du langage HTML d'être rendus en toute sécurité dans les pages HTML et a l'avantage supplémentaire d'être utilisé pour empêcher les attaques telles que XSS.   
- Base64 : est utilisée pour coder toutes les données dans un format compatible ASCII. Il a été conçu pour prendre des données binaires (par exemple, des images, des médias, des programmes) et les encoder dans un format qui conviendrait pour un transfert sur pratiquement n'importe quel support.  
- ASCII Hex : cette option convertit les données entre la représentation ASCII et la représentation hexadécimale.   
- Hex , Octal et Binary : Ces méthodes de codage s'appliquent toutes uniquement aux entrées numériques. Ils convertissent entre décimal, hexadécimal, octal (base huit) et binaire.   
- Gzip : Gzip permet de compresser les données. Il est largement utilisé pour réduire la taille des fichiers et des pages avant qu'ils ne soient envoyés à votre navigateur. Des pages plus petites signifient des temps de chargement plus rapides, ce qui est hautement souhaitable pour les développeurs qui cherchent à augmenter leur score SEO et à éviter d'ennuyer leurs clients. Decoder nous permet d'encoder et de décoder manuellement les données gzip, bien que cela puisse être difficile à traiter car il ne s'agit souvent pas d'ASCII/Unicode valide.   

--> Nous pouvons superposer n'importe lequel d'entre eux.   

### Hachage 

1. Théorie   
Le hachage est un processus unidirectionnel utilisé pour transformer les données en une signature unique. Pour être un algorithme de hachage, la sortie résultante doit être impossible à inverser. Un bon algorithme de hachage garantira que chaque élément de données saisi aura un hachage complètement unique. Pour cette raison, les hachages sont fréquemment utilisés pour vérifier l'intégrité des fichiers et des documents, car même une très petite modification du fichier entraînera une modification significative de la somme de hachage.  

Remarque : l'algorithme MD5 est obsolète et ne doit pas être utilisé pour les applications modernes.  

De même, les hachages sont également utilisés pour stocker en toute sécurité les mots de passe car (en raison du processus de hachage unidirectionnel, ce qui signifie que les hachages ne peuvent jamais être inversés), les mots de passe seront (relativement) sécurisés même en cas de fuite de la base de données. Lorsqu'un utilisateur crée un mot de passe, il est haché et stocké par l'application. Lorsque l'utilisateur essaie de se connecter, l'application hache alors le mot de passe qu'il soumet et le compare au hachage stocké ; si les hachages correspondent, alors le mot de passe était correct. Lors de l'utilisation de cette méthodologie, une application n'a jamais à stocker le mot de passe d'origine (en clair).   

2. Decoder   
Decoder nous permet de générer des hashsums pour les données directement dans Burp Suite ; cela fonctionne à peu près de la même manière que les options d'encodage/décodage que nous avons vues dans la tâche précédente. Plus précisément, nous cliquons sur le menu déroulant "Hash" et sélectionnons un algorithme dans la liste.  
L'application nous envoie automatiquement dans la vue Hex. En effet, la sortie d'un algorithme de hachage ne renvoie pas de texte ASCII/Unicode pur. En tant que tel, il est courant de prendre la sortie résultante de l'algorithme et de la transformer en une chaîne hexadécimale ; c'est la forme de "hachage" que vous connaissez peut-être. Terminons cela en appliquant un encodage "ASCII Hex" à la somme de hachage pour créer la chaîne hexadécimale.   

## Comparer

Comparer nous permet de comparer deux données, soit par mots ASCII, soit par octets.  

Cette interface peut être divisée en trois parties principales :  

1. Sur la gauche, nous avons les éléments comparés. Lorsque nous chargeons des données dans Comparer, elles apparaissent sous forme de lignes dans ces tables ; nous sélectionnons alors deux ensembles de données à comparer.  
2. En haut à droite, nous avons des options pour coller des données à partir du presse-papiers (Paste), charger des données à partir d'un fichier (Load), supprimer la ligne actuelle (Remove) et effacer tous les ensembles de données (Clear).  
3. Enfin, en bas à droite, nous avons la possibilité de comparer nos ensembles de données par mots ou par octets. Ce sont les boutons sur lesquels nous cliquons lorsque nous sommes prêts à comparer les données que nous avons sélectionnées.  

Nous pouvons également charger des données dans Comparer à partir d'autres modules en cliquant avec le bouton droit de la souris et en choisissant "Envoyer au comparateur".  

Lorsque nous avons chargé des données à comparer, nous obtenons une fenêtre contextuelle nous montrant la comparaison.   
Encore une fois, il y a trois parties distinctes dans cette fenêtre :  

1. Les données comparées occupent la majeure partie de la fenêtre ; cela peut être visualisé au format texte ou hexadécimal. Le format initial est déterminé selon que nous avons choisi de comparer par mots ou par octets dans la fenêtre précédente, mais cela peut être écrasé en utilisant les boutons au-dessus des cases de comparaison.  
2. La clé de comparaison se trouve en bas à gauche, indiquant les couleurs indiquant les données modifiées, supprimées et ajoutées entre les deux ensembles de données.  
3. En bas à droite de la fenêtre se trouve la case à cocher "Synchroniser les vues". Lorsqu'il est sélectionné, cela signifie que les deux ensembles de données synchroniseront les formats - c'est-à-dire que si vous changez l'un d'eux en vue Hex, l'autre fera de même pour correspondre.  
--> On nous donne le nombre total de différences trouvées dans le titre de la fenêtre.  

## Sequencer

Sequencer nous permet de mesurer l'entropie (ou le caractère aléatoire, en d'autres termes) des "tokens" - des chaînes qui sont utilisées pour identifier quelque chose et qui devraient, en théorie, être générées de manière cryptographiquement sécurisée. Par exemple, nous pouvons souhaiter analyser le caractère aléatoire d'un cookie de session ou d'un jeton CSRF (Cross-Site Request F orgery ) protégeant une soumission de formulaire. S'il s'avère que ces jetons ne sont pas générés de manière sécurisée, nous pouvons (en théorie) prédire les valeurs des jetons à venir. Imaginez simplement les implications de cela si le jeton en question est utilisé pour les réinitialisations de mot de passe...   

Il existe deux méthodes principales que nous pouvons utiliser pour effectuer une analyse de jeton avec Sequencer :  

- Live Capture : La capture en direct est la plus courante des deux méthodes - il s'agit du sous-onglet par défaut pour Sequencer. La capture en direct nous permet de transmettre une demande à Sequencer, qui, nous le savons, créera un jeton à analyser. Par exemple, nous pouvons souhaiter transmettre une demande POST à ​​un point de terminaison de connexion dans Sequencer, car nous savons que le serveur répondra en nous donnant un cookie. Une fois la demande transmise, nous pouvons dire à Sequencer de démarrer une capture en direct : il fera alors la même demande des milliers de fois automatiquement, en stockant les échantillons de jetons générés pour analyse. Une fois que nous avons accumulé suffisamment d'échantillons, nous arrêtons Sequencer et lui permettons d'analyser les jetons capturés.  
- Manual Load: Le chargement manuel nous permet de charger une liste d'échantillons de jetons pré-générés directement dans Sequencer pour analyse. L'utilisation du chargement manuel signifie que nous n'avons pas à faire des milliers de requêtes à notre cible (ce qui est à la fois bruyant et gourmand en ressources), mais cela signifie que nous devons obtenir une grande liste de jetons pré-générés !  

### Live Capture

Dans la section "Emplacement du jeton dans la réponse", nous avons la possibilité de choisir entre Cookie, Champ de formulaire et Emplacement personnalisé.   
Une nouvelle fenêtre apparaîtra maintenant nous indiquant que nous effectuons une capture en direct et nous montrant combien de jetons nous avons capturés jusqu'à présent. Nous devons attendre d'avoir un nombre raisonnable de jetons capturés (environ 10 000 devraient suffire) ; plus nous avons de jetons, plus notre analyse est précise.   
Une fois que vous avez environ 10 000 jetons capturés, cliquez sur "Pause", puis sélectionnez le bouton "Analyser maintenant"  
Remarque : Nous aurions également pu choisir de "Stop" la capture ; cependant, en choisissant de le mettre en pause à la place, nous laissons l'option reprendre la capture ouverte, si le rapport n'a pas assez d'échantillons pour calculer l'entropie du jeton avec précision.  
Si nous voulions recevoir des mises à jour périodiques sur l'analyse, nous aurions également pu cocher la case "Analyse automatique". Cela indiquerait à Burp d'effectuer l'analyse d'entropie toutes les 2000 requêtes environ, ce qui nous donnerait des mises à jour fréquentes qui deviendront de plus en plus précises à mesure que davantage d'échantillons seront chargés dans Sequencer.  
Il convient de noter qu'à ce stade, nous pouvons également choisir de copier ou d'enregistrer les jetons capturés pour une analyse ultérieure ultérieure. 

### Analyse

Burp effectue des dizaines de tests sur les échantillons de jetons qu'il a capturés.  

Le rapport d'analyse d'entropie généré est divisé en quatre sections principales, la première d'entre elles étant un résumé des résultats. Le résumé nous donne un résultat global ; l'entropie effective ; une analyse de la fiabilité des résultats ; et un résumé de l'échantillon prélevé.  

Collectivement, ceux-ci seront souvent suffisants pour déterminer si le jeton est généré en toute sécurité ou non ; cependant, dans certains cas, nous devrons peut-être consulter directement les résultats du test - cela peut être fait dans les onglets "Analyse au niveau du caractère" et "Analyse au niveau du bit". En bref, avec une probabilité estimée à 1 % d'être incorrecte sur la base des données fournies (niveau de signification : 1 %).  

Exemple : Burp a calculé que l'entropie effective de notre jeton devrait être d'environ 117 bits. C'est un excellent niveau d'entropie à avoir dans un jeton sécurisé, même s'il convient de noter qu'il est impossible de dire avec une assurance absolue que ce calcul est entièrement exact, simplement en raison de la nature du sujet.
