# Burp Suite

## Qu'est ce que Burp Suite ?

Burp Suite est un framework écrit en Java qui vise à fournir un guichet unique pour les tests de pénétration des applications Web. Burp Suite est également très couramment utilisée lors de l'évaluation des applications mobiles, car les mêmes fonctionnalités qui la rendent si attrayante pour les tests d'applications Web se traduisent presque parfaitement par le test des API ( interfaces de programmation d' applications) qui alimentent la plupart des applications mobiles.

Au niveau le plus simple, Burp peut capturer et manipuler tout le trafic entre un attaquant et un serveur Web : c'est le cœur du framework. Après avoir capturé les demandes, nous pouvons choisir de les envoyer à diverses autres parties du cadre Burp Suite. Cette capacité à intercepter, afficher et modifier les requêtes Web avant qu'elles ne soient envoyées au serveur cible (ou, dans certains cas, les réponses avant qu'elles ne soient reçues par notre navigateur), rend Burp Suite parfait pour tout type de test manuel d'applications Web.

Il existe différentes éditions de Burp Suite disponibles :  

- Burp Suite Community : elle est gratuite pour toute utilisation (légale) non commerciale
- Burp Suite Professional : payant, version sans restriction de Burp Suite Community avec foncitonnalité supplémentaire  
- Burp Suite Enterprise : payant, est utilisé pour l'analyse continue, scanner de vulnérabilité automatisé   

Ensemble de fonctionnalité relativement limité par rapport à l'édition professionnelle :  

- Proxy : le Burp Proxy nous permet d'intercepter et de modifier les requêtes/réponses lors de l'interaction avec les applications Web  
- Répéteur : nous permet de capturer, de modifier, puis de renvoyer la même demande plusieurs fois. Cette fonctionnalité peut être absolument inestimable, en particulier lorsque nous devons créer une charge utile par essais et erreurs (par exemple, dans une injection SQLi) ou lors du test de la fonctionnalité d'un point de terminaison pour les défauts.  
- Intruder : nous permet de pulvériser un point de terminaison avec des requêtes. Ceci est souvent utilisé pour les attaques par force brute ou pour fuzzer les terminaux.  
- Décodeur : fournit toujours un service précieux lors de la transformation des données, soit en termes de décodage des informations capturées, soit de codage d'une charge utile avant de l'envoyer à la cible.  
- Comparer : nous permet de comparer deux données au niveau du mot ou de l'octet. Etre capable d'envoyer des données (potentiellement très volumineuses) directement dans un outil de comparaison avec un seul raccourci clavier peut accélérer considérablement les choses.  
- Séquenceur : pour évaluer le caractère aléatoire des jetons tels que les valeurs des cookies de session ou d'autres données générées soi-disant aléatoires. Si l'algorithme ne génère pas de valeurs aléatoires sécurisées, cela pourrait ouvrir des voies d'attaque dévastatrices.

En plus de la myriade de fonctionnalités intégrées, la base de code Java facilite également l'écriture d'extensions à ajouter aux fonctionnalités du framework Burp. Ceux-ci peuvent être écrits en Java, Python (en utilisant l'interpréteur Java Jython) ou Ruby (en utilisant l'interpréteur Java JRuby). Le module Burp Suite Extender peut charger rapidement et facilement des extensions dans le framework, ainsi que fournir un marché pour télécharger des modules tiers (appelé "BApp Store").  

## Tableau de bord 

1. Le menu Tâches nous permet de définir les tâches d'arrière-plan que Burp Suite exécutera pendant que nous utilisons l'application.   
2. Le journal des événements nous indique ce que fait Burp Suite (par exemple, le démarrage du proxy), ainsi que des informations sur toutes les connexions que nous établissons via Burp.   
3. La section Activité de problème est exclusive à Burp Pro. Cela ne nous donnera rien en utilisant Burp Community, mais dans Burp Professional, il listerait toutes les vulnérabilités trouvées par le scanner automatisé. Ceux-ci seraient classés par gravité et filtrables en fonction de la certitude de Burp que le composant est vulnérable.  
4. La section Conseils donne plus d'informations sur les vulnérabilités trouvées, ainsi que des références et des suggestions de corrections. Ceux-ci pourraient ensuite être exportés dans un rapport.   

## Navigation

La navigation dans l'interface graphique Burp Suite par défaut se fait entièrement à l'aide des barres de menus supérieures. Les onglets peuvent également être affichés dans des fenêtres séparées si vous préférez afficher plusieurs onglets séparément.  

En plus de la barre de menus, Burp Suite dispose également de raccourcis clavier qui permettent une navigation rapide vers les onglets clés :   

- Ctrl + Shift + D : Passer au tableau de bord   
- Ctrl + Shift + T : Passer à l'onglet Target   
- Ctrl + Shift + P : Passez à l'onglet Proxy   
- Ctrl + Shift + I : Basculer vers l'onglet Intruder   
- Ctrl + Shift + R : Passez à l'onglet Repeateur    

## Configuration

Les options fournies dans l'onglet Options utilisateur s'appliqueront à chaque fois que nous ouvrirons Burp Suite. En revanche, les options du projet ne s'appliqueront qu'au projet en cours. Étant donné que nous ne pouvons pas enregistrer de projets dans Burp Community, cela signifie que nos options de projet seront réinitialisées à chaque fois que nous fermerons Burp.  

Les paramètres globaux peuvent être trouvés dans l'onglet Options utilisateur le long de la barre de menu supérieure.  

- Connexions nous permettent de contrôler la manière dont Burp établit des connexions avec les cibles. Par exemple, nous pouvons définir un proxy pour que Burp Suite se connecte ; ceci est très utile si nous voulons utiliser Burp Suite via un pivot réseau.   
- TLS nous permet d'activer et de désactiver diverses options TLS, ainsi que de nous donner un endroit pour télécharger des certificats clients si une application Web nous oblige à en utiliser un pour les connexions.  
- Display nous permet de changer l'apparence de Burp Suite.  
- Divers contient une grande variété de paramètres, y compris le tableau des raccourcis clavier (HotKeys).   

Les paramètres spécifiques au projet se trouvent dans l' onglet Options du projet.  

- Connections contient bon nombre des mêmes options que la section équivalente de l'onglet Options utilisateur : elles peuvent être utilisées pour remplacer les paramètres à l'échelle du projet. Il existe cependant quelques différences, par exemple, l'option "Résolution du nom d'hôte" (vous permettant de mapper des domaines à des adresses IP directement dans Burp Suite) peut être très pratique, tout comme les paramètres "Requêtes hors champ", qui nous permettent de déterminer si Burp enverra demandes à tout ce que vous ne ciblez pas explicitement.  
- HTTP définit la manière dont Burp gère divers aspects du protocole HTTP : par exemple, s'il suit les redirections ou comment gérer les codes de réponse inhabituels.  
- TLS nous permet de remplacer les options TLS à l'échelle de l'application, ainsi que de nous montrer une liste de certificats de serveur public pour les sites que nous avons visités.   
- Sessions nous offre des options pour gérer les sessions. Il nous permet de définir comment Burp obtient, enregistre et utilise les cookies de session qu'il reçoit des sites cibles. Cela nous permet également de définir des macros que nous pouvons utiliser pour automatiser des choses.   
- Divers : moins d'options et certaines disponibles que si vous avez accès à Burp Pro (comme Collaborator). Cela dit, il existe quelques options liées à la journalisation et au navigateur intégré qui valent la peine d'être lues.     
