# Meterpreter

Meterpreter est une charge utile Metasploit qui prend en charge le processus de test d'intrusion avec de nombreux composants précieux. Meterpreter s'exécutera sur le système cible et agira en tant qu'agent au sein d'une architecture de commande et de contrôle. Vous interagirez avec le système d'exploitation et les fichiers cibles et utiliserez les commandes spécialisées de Meterpreter.

Meterpreter a de nombreuses versions qui fourniront différentes fonctionnalités en fonction du système cible.

## Comment fonctionne Meterpreter ?

Meterpreter s'exécute sur le système cible mais n'y est pas installé. Il s'exécute en mémoire et ne s'écrit pas sur le disque de la cible. Cette fonctionnalité vise à éviter d'être détecté lors des analyses antivirus. Par défaut, la plupart des logiciels antivirus analysent les nouveaux fichiers sur le disque (par exemple, lorsque vous téléchargez un fichier sur Internet). Meterpreter s'exécute en mémoire (RAM - Random Access Memory) pour éviter d'avoir un fichier qui doit être écrit sur le disque sur le système cible (par exemple meterpreter.exe). De cette façon, Meterpreter sera considéré comme un processus et n'aura pas de fichier sur le système cible.

Meterpreter vise également à éviter d'être détecté par les solutions IPS (Intrusion Prevention System) et IDS (Intrusion Detection System) basées sur le réseau en utilisant une communication cryptée avec le serveur sur lequel Metasploit s'exécute (généralement votre machine attaquante). Si l'organisation cible ne décrypte pas et n'inspecte pas le trafic crypté (par exemple HTTPS) entrant et sortant du réseau local, les solutions IPS et IDS ne seront pas en mesure de détecter ses activités.

Bien que Meterpreter soit reconnu par les principaux logiciels antivirus, cette fonctionnalité offre un certain degré de discrétion.

- `getpid` : renvoie l'ID de processus avec lequel Meterpreter est en cours d'execution   
L'ID de processus (ou identifiant de processus) est utilisé par les systèmes d'exploitation pour identifier les processus en cours d'exécution. Tous les processus exécutés sous Linux ou Windows auront un numéro d'identification unique ; ce numéro est utilisé pour interagir avec le processus lorsque le besoin s'en fait sentir (par exemple, s'il doit être arrêté).

- `ps` : lister les processus   
remarque :le processsus qui est sur l'id de meterpreter est spoolsv.exe et non Meterpreter.exe, comme on pourrait s'y attendre. Même en cherchant plus loin (egarder les DLL (Dynamic-Link Libraries)) nous ne trouverions toujours rien qui nous saute aux yeux et nous indique meterpreter.

Il convient également de noter que Meterpreter établira un canal de communication crypté (TLS) avec le système de l'attaquant.   

## Choisir la charge utile

Les charges utiles Meterpreter sont également divisées en versions échelonnées et en ligne. Cependant, Meterpreter propose une large gamme de versions différentes parmi lesquelles vous pouvez choisir en fonction de votre système cible. 

Le moyen le plus simple d'avoir une idée des versions disponibles de Meterpreter pourrait être de les répertorier en utilisant msfvenom.   
`msfvenom --list payloads | grep meterpreter` 

Votre décision sur la version de Meterpreter à utiliser sera principalement basée sur trois facteurs :

- Le système d'exploitation cible (le système d'exploitation cible est-il Linux ou Windows ? Est-ce un appareil Mac ? Est-ce un téléphone Android ? etc.)   
- Composants disponibles sur le système cible (Python est-il installé ? Est-ce un site Web PHP ? etc.)   
- Types de connexion réseau que vous pouvez avoir avec le système cible (Autorisent-ils les connexions TCP brutes ? Pouvez-vous uniquement avoir une connexion inverse HTTPS ? Les adresses IPv6 ne sont-elles pas aussi étroitement surveillées que les adresses IPv4 ? etc.)   

Si vous n'utilisez pas Meterpreter en tant que charge utile autonome générée par Msfvenom, votre choix peut également être limité par l'exploit avec les payload.

## Commandes

Chaque version de Meterpreter aura différentes options de commande, donc exécuter la commande `help` est toujours une bonne idée.

Meterpreter vous fournira trois principales catégories d'outils :

- Commandes intégrées   
- Outils Meterpreter   
- Script Meterpreter    

Si vous exécutez la help commande, vous verrez que les commandes Meterpreter sont répertoriées dans différentes catégories.

- Commandes principales   
- Commandes du système de fichiers   
- Commandes de mise en réseau   
- Commandes système   
- Commandes de l'interface utilisateur   
- Commandes de webcam   
- Commandes de sortie audio   
- Commandes d'élévation   
- Commandes de la base de données de mots de passe   
- Commandes d'horodatage   

### Exemple pour Windows

Veuillez noter que la liste ci-dessus provient de la sortie de la help commande sur la version Windows de Meterpreter (windows/x64/meterpreter/reverse_tcp). Celles-ci seront différentes pour les autres versions de Meterpreter.

Commandes principales : 

- background: Arrière-plan de la session en cours   
- exit: Terminer la session Meterpreter   
- guid: Obtenir le GUID de la session (Globally Unique Identifier)   
- help: Affiche le menu d'aide   
- info: Affiche des informations sur un module Post   
- irb: Ouvre un shell Ruby interactif sur la session en cours   
- load: Charge une ou plusieurs extensions Meterpreter   
- migrate: Permet de migrer Meterpreter vers un autre processus. La migration vers un autre processus aidera Meterpreter à interagir avec lui. Par exemple, si vous voyez un traitement de texte en cours d'exécution sur la cible (par exemple word.exe, notepad.exe, etc.), vous pouvez migrer vers celui-ci et commencer à capturer les frappes envoyées par l'utilisateur à ce processus. Certaines versions de Meterpreter vous offriront les options de commande keyscan_start, keyscan_stopet keyscan_dumppour que Meterpreter agisse comme un enregistreur de frappe. La migration vers un autre processus peut également vous aider à avoir une session Meterpreter plus stable.      
- run: Exécute un script Meterpreter ou un module Post   
- sessions: Passer rapidement à une autre session   

Commandes du système de fichiers :     

- cd: Changera de répertoire   
- ls: listera les fichiers dans le répertoire courant (dir fonctionnera également)   
- pwd: Imprime le répertoire de travail courant   
- edit: vous permettra d'éditer un fichier   
- cat: affichera le contenu d'un fichier à l'écran   
- rm: supprimera le fichier spécifié   
- search: recherchera les fichiers   
- upload: va télécharger un fichier ou un répertoire   
- download: va télécharger un fichier ou un répertoire   

Commandes de mise en réseau :   

- arp: Affiche le cache ARP (Address Resolution Protocol) de l'hôte   
- ifconfig: affiche les interfaces réseau disponibles sur le système cible  
- netstat: Affiche les connexions réseau   
- portfwd: redirige un port local vers un service distant  
- route: Permet de visualiser et de modifier la table de routage  

Commandes système :  

- clearev: Efface les journaux d'événements   
- execute: Exécute une commande  
- getpid: Affiche l'identifiant du processus actuel   
- getuid: Affiche l'utilisateur sous lequel Meterpreter s'exécute, Cela vous donnera une idée de votre niveau de privilège possible sur le système cible      
- kill: Termine un processus   
- pkill: Termine les processus par leur nom   
- ps: répertorie les processus en cours d'exécution, La colonne PID vous donnera également les informations PID dont vous aurez besoin pour migrer Meterpreter vers un autre processus.     
- reboot: Redémarre l'ordinateur distant   
- shell: Tombe dans un shell de commande système   
- shutdown: Arrête l'ordinateur distant   
- sysinfo: Obtient des informations sur le système distant, telles que le système d'exploitation   

Autres commandes :

- idletime: Renvoie le nombre de secondes pendant lesquelles l'utilisateur distant a été inactif   
- keyscan_dump: Vide le tampon de frappe   
- keyscan_start: Commence à capturer les frappes   
- keyscan_stop: Arrête de capturer les frappes   
- screenshare: Vous permet de regarder le bureau de l'utilisateur distant en temps réel  
- screenshot: Capture une capture d'écran du bureau interactif  
- record_mic: Enregistre l'audio du microphone par défaut pendant X secondes   
- webcam_chat: Démarre un chat vidéo   
- webcam_list: Répertorie les webcams   
- webcam_snap: Prend un instantané à partir de la webcam spécifiée   
- webcam_stream: Lit un flux vidéo à partir de la webcam spécifiée   
- getsystem: Tente d'élever votre privilège à celui du système local   
- hashdump: Vide le contenu de la base de données SAM, listera le contenu de la base de données SAM. La base de données SAM (Security Account Manager) stocke les mots de passe des utilisateurs sur les systèmes Windows. Ces mots de passe sont stockés au format NTLM ( New Technology LAN Manager) .   

Bien que toutes ces commandes puissent sembler disponibles dans le menu d'aide, elles peuvent ne pas toutes fonctionner. Par exemple, le système cible peut ne pas avoir de webcam, ou il peut être exécuté sur une machine virtuelle sans environnement de bureau approprié.

## Post exploitation

