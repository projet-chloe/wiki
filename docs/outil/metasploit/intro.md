# Introduction Metasploit

## Définission

Metasploit est le framework d'exploitation le plus utilisé. Metasploit est un outil puissant qui peut prendre en charge toutes les phases d'un engagement de test d'intrusion, de la collecte d'informations à la post-exploitation. Il facilite le processus d'exploitation. Le processus d'exploitation comprend trois étapes principales ; trouver l'exploit, personnaliser l'exploit et exploiter le service vulnérable.

Metasploit a deux versions principales :

- Metasploit Pro : La version commerciale qui facilite l'automatisation et la gestion des tâches. Cette version a une interface utilisateur graphique (GUI).   
- Metasploit Framework : La version open-source qui fonctionne depuis la ligne de commande. Ell est installé sur les distributions Linux les plus couramment utilisées pour les tests d'intrusion.   

Le framework Metasploit est un ensemble d'outils qui permettent la collecte d'informations, l'analyse, l'exploitation, le développement d'exploits, la post-exploitation, etc. Bien que l'utilisation principale de Metasploit Framework se concentre sur le domaine des tests d'intrusion, il est également utile pour la recherche de vulnérabilités et le développement d'exploits.

Les principaux composants du Metasploit Framework peuvent être résumés comme suit :   

- msfconsole : L'interface de ligne de commande principale.   
- Modules : modules de support tels que les exploits, les scanners, les charges utiles, etc.   
- Outils : outils autonomes qui faciliteront la recherche de vulnérabilités, l'évaluation des vulnérabilités ou les tests d'intrusion. Certains de ces outils sont msfvenom, pattern_create et pattern_offset (outils utiles dans le développement d'exploits).

## Les modules

La console sera votre interface principale pour interagir avec les différents modules du Framework Metasploit. Les modules sont de petits composants du framework Metasploit qui sont conçus pour effectuer une tâche spécifique.

Les modules se trouve à /usr/share/matasploit-framework. Voici une liste des modules :  

- Auxiliary : tous les modules de support, tels que les scanners, les crawlers et les fuzzers.  
- Encoders : pour encoder l'exploit et la charge utile dans l'espoir qu'une solution antivirus basée sur les signatures puisse les manquer. Les solutions antivirus et de sécurité basées sur les signatures disposent d'une base de données des menaces connues. Ils détectent les menaces en comparant les fichiers suspects à cette base de données et déclenchent une alerte en cas de correspondance. Ainsi, les encodeurs peuvent avoir un taux de réussite limité car les solutions antivirus peuvent effectuer des vérifications supplémentaires.   
- Évasion : bien que les encodeurs encodent la charge utile, ils ne doivent pas être considérés comme une tentative directe d'échapper aux logiciels antivirus. En revanche, les modules « évasion » tenteront cela, avec plus ou moins de succès.   
- Exploits : exploits, soigneusement organisés par système cible.   
- nops : les NOP (No OPeration) ne font rien, littéralement. Ils sont souvent utilisés comme tampon pour obtenir des tailles de charge utile cohérentes.   
- payloads : les charges utiles sont des codes qui s'exécuteront sur le système cible. Vous verrez trois répertoires différents sous les charges utiles : singles, stagers et stages.  

    - Singles : charges utiles autonomes (ajouter un utilisateur, lancer notepad.exe, etc.) qui n'ont pas besoin de télécharger un composant supplémentaire pour s'exécuter.   
    - Stagers : responsables de la mise en place d'un canal de connexion entre Metasploit et le système cible. Utile lorsque vous travaillez avec des charges utiles étagées. Les "charges utiles mises en scène" téléchargent d'abord un stager sur le système cible, puis téléchargent le reste de la charge utile (étape). Cela offre certains avantages car la taille initiale de la charge utile sera relativement petite par rapport à la charge utile complète envoyée en une seule fois.   
    - Stages : téléchargés par le stager. Cela vous permettra d'utiliser des charges utiles de plus grande taille.   

    Metasploit a une manière subtile de vous aider à identifier les charges utiles uniques (on un _ dans le nom exemple shell_reverse_tcp) et les charges utiles étagées (on un / exemple shell/reverse_tcp).   

- Post : Les modules Post seront utiles lors de la dernière étape du processus de test d'intrusion après l'exploitation.   

## Commande de base 

Lors de l'utilisation de Metasploit Framework, vous interagirez principalement avec la console Metasploit. 

`msfconsole`: lance metasploit

Une fois lancé, vous verrez la ligne de commande changer en msf6 (ca depénde de la version). 

Il est possible d'utiliser la plus part des commandes linux par exemple `ls` répertorie le contenu du dossier à partir duquel Metasploit a été lancé mais ne prend pas toutes le sfonctionnalité (exemple la reidrectin de sortie > ne fonctionne pas).    

Msfconsole est géré par contexte ; cela signifie qu'à moins d'être défini comme une variable globale, tous les réglages de paramètres seront perdus si vous changez le module que vous avez décidé d'utiliser.

`use [fichier d'exploit]` : permet d'utiliser un exploit/un module, de lancer le contexte, l'invite de commande change (maintenant on est dans l'exploit). ne pas mettre l'extension du fichier. à a place on peut aussi mettre le numéro au début de la ligne de résultat (après une recherche)

`show options` permet de voir le contexte de notre exploits ou autre code qu'on a lancé. on put mettre n'importe quels nom de modules à la place d'options pour savoir ce qui est disponible dans notre contexte

`info` pour avoir plus d'informations détaillées, soit on tape juste info quand on est dans le contexte soit info [chemin du fichier]   

`show` dans l'invite classqieu listera tous les modules

`back` quitter le contexte (l'exploit par exemple)

`search` recherche les modules pertinents pour les paramètres données. on peut utiliser des mot clé comme type:auxiliary ou platform. la colonne rang indique la fiabilité 

`set [parametre]` permet de définir un parametre quand on est dans un contexte    

- RPORT : remote port, « Port distant », le port sur le système cible sur lequel l'application vulnérable s'exécute   
- RHOSTS : adresse ip système cible, une seule adresse IP ou une plage réseau peut être définie : avec la notation CIDR (/24, /16, ..) ou une plage réseau (10.10.10.x – 10.10.10.y). Vous pouvez également utiliser un fichier où les cibles sont répertoriées, une cible par ligne en utilisant file:/path/of/the/target_file.txt  
- PAYLOAD : la charge utile que vous utiliserez avec l'exploit.   
- LHOST : « Localhost », l'adresse IP de la machine attaquante (votre AttackBox ou Kali Linux).   
- LPORT : "Port local", le port que vous utiliserez pour que le shell inverse se reconnecte. Il s'agit d'un port sur votre machine attaquante.      
- SESSION : chaque connexion établie avec le système cible à l'aide de Metasploit aura un ID de session. Vous l'utiliserez avec des modules de post-exploitation qui se connecteront au système cible à l'aide d'une connexion existante.   

`unset [parametre]` effacer un parametre, si on met all ca efface tous les parametres   

`setg [parametre]` définir les valeurs qui seront utilisées par tous les modules afin qu'elle puisse être utilisée par défaut dans différents modules jusqu'à ce que vous quittiez Metasploit ou que vous l'effaciez à l'aide de unsetg

`exploit` executera l'exploit, on peut aussi utiliser run à la place. on peut mettre le parametre -z en plus. l'option check permet de vérifier si le syteme cible est vulnérable sans l'exploiter   
-j indique à metasploit de lancer l'execution en arriere plan directement   

`background` mettre en arrière-plan l'invite de session et revenir à l'invite msfconsole   

`sessions` voir les sessions existantes

`sessions -[id]` remet la session avec l'id (on le voit après avoir fait sessions) en avant pour q'uon puisse interagir avec elle

