# Analyse avancée des ports

Nous allons étudié les types avancés d'analyses et les options d'analyse. Certains de ces types d'analyse peuvent être utiles contre des systèmes spécifiques, tandis que d'autres sont utiles dans des configurations réseau particulières.

## Null scan, FIN Scan et Xmas Scan

- Analyse nulle  

L'analyse nulle ne définit aucun indicateur ; les six bits indicateurs sont mis à zéro. Vous pouvez choisir cette analyse à l'aide de l'-sN option.  
Un paquet TCP sans indicateur défini ne déclenchera aucune réponse lorsqu'il atteindra un port ouvert. Cependant, nous nous attendons à ce que le serveur cible réponde avec un paquet RST si le port est fermé. Par conséquent, nous pouvons utiliser l'absence de réponse RST dans une analyse nulle pour déterminer les ports qui ne sont pas fermés : ouverts ou filtrés (un pare-feu bloque le paquet).

- Balayage FIN   

L'analyse FIN envoie un paquet TCP avec l'indicateur FIN défini. Vous pouvez choisir ce type d'analyse à l'aide de l' -sF option. De même, aucune réponse ne sera envoyée si le port TCP est ouvert. Cependant, le système cible doit répondre avec un RST si le port est fermé. Par conséquent, nous pourrons savoir quels ports sont fermés et utiliser cette connaissance pour déduire les ports ouverts ou filtrés. Il convient de noter que certains pare-feu suppriment « silencieusement » le trafic sans envoyer de RST.

- Numérisation de Noël   

Le balayage de Noël tire son nom des lumières de l'arbre de Noël. Un balayage Xmas définit simultanément les indicateurs FIN, PSH et URG. Vous pouvez sélectionner Xmas scan avec l'option -sX. Comme l'analyse Null et l'analyse FIN, si un paquet RST est reçu, cela signifie que le port est fermé. Sinon, il sera signalé comme ouvert|filtré.

Le scénario où ces trois types d'analyse peuvent être efficaces est lors de l'analyse d'une cible derrière un pare-feu sans état (sans état). Un pare-feu sans état vérifiera si le paquet entrant a le drapeau SYN défini pour détecter une tentative de connexion. L'utilisation d'une combinaison d'indicateurs qui ne correspond pas au paquet SYN permet de tromper le pare-feu et d'atteindre le système derrière lui. Cependant, un pare-feu avec état bloquera pratiquement tous ces paquets spécialement conçus et rendra ce type d'analyse inutile.

## Analyse Maimon TCP

Uriel Maimon a décrit ce scan pour la première fois en 1996. Dans ce scan, les bits FIN et ACK sont définis. La cible doit envoyer un paquet RST en réponse. Cependant, certains systèmes dérivés de BSD abandonnent le paquet s'il s'agit d'un port ouvert exposant les ports ouverts. Cette analyse ne fonctionnera pas sur la plupart des cibles rencontrées dans les réseaux modernes ; cependant, nous l'incluons dans cette salle pour mieux comprendre le mécanisme d'analyse des ports et l'état d'esprit du piratage. Pour sélectionner ce type d'analyse, utilisez l' -sM option.

La plupart des systèmes cibles répondent avec un paquet RST, que le port TCP soit ouvert ou non. Dans un tel cas, nous ne pourrons pas découvrir les ports ouverts. La figure ci-dessous montre le comportement attendu dans les cas de ports TCP ouverts et fermés.

Ce type d'analyse n'est pas la première analyse que l'on choisirait pour découvrir un système ; cependant, il est important de le savoir car vous ne savez pas quand cela pourrait être utile.

## Analyse ACK, Windows et Custom scan

- Balayage TCP ACK  

Comme son nom l'indique, une analyse ACK enverra un paquet TCP avec l'indicateur ACK défini. Utilisez l' -sA option pour choisir cette analyse. La cible répondrait à l'ACK avec RST quel que soit l'état du port. Ce comportement se produit car un paquet TCP avec l'indicateur ACK doit être envoyé uniquement en réponse à un paquet TCP reçu pour accuser réception de certaines données, contrairement à notre cas. Par conséquent, cette analyse ne nous dira pas si le port cible est ouvert dans une configuration simple. Ce type d'analyse serait utile s'il y a un pare-feu devant la cible. Par conséquent, en fonction des paquets ACK qui ont donné lieu à des réponses, vous saurez quels ports n'ont pas été bloqués par le pare-feu. En d'autres termes, ce type d'analyse est plus adapté pour découvrir les ensembles de règles et la configuration du pare-feu.

- Balayage de fenêtre  

Une autre analyse similaire est l'analyse de la fenêtre TCP. L'analyse de la fenêtre TCP est presque la même que l'analyse ACK ; cependant, il examine le champ TCP Window des paquets RST renvoyés. Sur des systèmes spécifiques, cela peut révéler que le port est ouvert. Vous pouvez sélectionner ce type d'analyse avec l'option -sW. Nous nous attendons à recevoir un paquet RST en réponse à nos paquets ACK "non invités", que le port soit ouvert ou fermé. De même, lancer une analyse de fenêtre TCP sur un système Linux sans pare-feu ne fournira pas beaucoup d'informations.  

- Analyse personnalisée  
  
Si vous souhaitez expérimenter une nouvelle combinaison d'indicateurs TCP au-delà des types d'analyse TCP intégrés, vous pouvez le faire en utilisant --scanflags. Par exemple, si vous souhaitez définir simultanément SYN, RST et FIN, vous pouvez le faire en utilisant --scanflags RSTSYNFIN. Comme le montre la figure ci-dessous, si vous développez votre analyse personnalisée, vous devez savoir comment les différents ports se comporteront pour interpréter correctement les résultats dans différents scénarios. 

Enfin, il est essentiel de noter que l'analyse ACK et l'analyse de la fenêtre ont été très efficaces pour nous aider à définir les règles du pare-feu. Cependant, il est essentiel de se rappeler que le fait qu'un pare-feu ne bloque pas un port spécifique ne signifie pas nécessairement qu'un service écoute sur ce port. Par exemple, il est possible que les règles de pare-feu doivent être mises à jour pour refléter les modifications récentes du service. Par conséquent, les analyses ACK et de fenêtre exposent les règles du pare-feu, pas les services.

## Spoofing et decoy

- usurpation   

Dans certaines configurations réseau, vous pourrez analyser un système cible à l'aide d'une adresse IP usurpée et même d'une adresse MAC usurpée. Une telle analyse n'est bénéfique que dans une situation où vous pouvez garantir de capturer la réponse. Si vous essayez d'analyser une cible à partir d'un réseau aléatoire à l'aide d'une adresse IP usurpée, il est probable que vous ne receviez aucune réponse et que les résultats de l'analyse ne soient pas fiables.

En bref, l'analyse avec une adresse IP usurpée se fait en trois étapes :

1. L'attaquant envoie un paquet avec une adresse IP source usurpée à la machine cible.  
2. La machine cible répond à l'adresse IP usurpée comme destination.   
3. L'attaquant capture les réponses pour découvrir les ports ouverts.   

En général, vous vous attendez à spécifier l'interface réseau à l'aide de -e et à désactiver explicitement l'analyse ping -Pn. Par conséquent, vous devrez émettre `nmap -e NET_INTERFACE -Pn -S SPOOFED_IP MACHINE_IP`.    
Il convient de répéter que cette analyse sera inutile si le système attaquant ne peut pas surveiller le réseau pour les réponses.

Lorsque vous êtes sur le même sous-réseau que la machine cible, vous pouvez également usurper votre adresse MAC. Vous pouvez spécifier l'adresse MAC source à l'aide de --spoof-mac SPOOFED_MAC. Cette usurpation d'adresse n'est possible que si l'attaquant et la machine cible sont sur le même réseau Ethernet (802.3) ou le même WiFi (802.11).

- leurre    

L'usurpation d'identité ne fonctionne que dans un nombre minimal de cas où certaines conditions sont remplies. Par conséquent, l'attaquant peut recourir à l'utilisation de leurres pour qu'il soit plus difficile d'être identifié. Le concept est simple, faire en sorte que l'analyse semble provenir de plusieurs adresses IP afin que l'adresse IP de l'attaquant soit perdue parmi elles. L'analyse de la machine cible semblera provenir de 3 sources différentes, et par conséquent, les réponses iront également aux leurres.

Vous pouvez lancer une analyse leurre en spécifiant une adresse IP spécifique ou aléatoire après -D. Par exemple, `nmap -D 10.10.0.1,10.10.0.2,ME MACHINE_IP` fera apparaître le scan de MACHINE_IP comme provenant des adresses IP 10.10.0.1, 10.10.0.2, puis ME pour indiquer que votre adresse IP doit apparaître en troisième ordre. On peut utiliser RND pour attribuer des adresses IP de manière aléatoire, tandis que la cinquième source sera l'adresse IP de l'attaquant. exemple : `nmap -D 10.10.0.1,10.10.0.2,RND,RND,ME MACHINE_IP`

## Fragmented packets

- rappel   

Pare-feu   
Un pare-feu est un logiciel ou un matériel qui autorise le passage des paquets ou les bloque. Il fonctionne sur la base de règles de pare-feu, résumées comme bloquant tout le trafic avec des exceptions ou autorisant tout le trafic avec des exceptions. Par exemple, vous pouvez bloquer tout le trafic vers votre serveur, à l'exception de ceux qui arrivent sur votre serveur Web. Un pare-feu traditionnel inspecte au moins l'en-tête IP et l'en-tête de la couche de transport. Un pare-feu plus sophistiqué essaierait également d'examiner les données transportées par la couche de transport.

IDS   
Un système de détection d'intrusion (IDS) inspecte les paquets réseau pour sélectionner des modèles de comportement ou des signatures de contenu spécifiques. Il déclenche une alerte chaque fois qu'une règle malveillante est rencontrée. En plus de l'en-tête IP et de l'en-tête de la couche de transport, un IDS inspecterait le contenu des données dans la couche de transport et vérifierait s'il correspond à des schémas malveillants. Comment pouvez-vous réduire la probabilité qu'un pare-feu/IDS traditionnel détecte votre activité Nmap ? Il n'est pas facile de répondre à cela; cependant, selon le type de pare-feu/IDS, il peut être avantageux de diviser le paquet en paquets plus petits.

- Paquets fragmentés   

Nmap offre la possibilité de fragmenter les paquets avec -f. Une fois choisies, les données IP seront divisées en 8 octets ou moins. L'ajout d'un autre -f(-f -f ou -ff) divisera les données en fragments de 16 octets au lieu de 8. Vous pouvez modifier la valeur par défaut en utilisant le --mtu; cependant, vous devez toujours choisir un multiple de 8.

Pour bien comprendre la fragmentation, nous devons examiner l'en-tête IP. Notez que l'adresse source prend 32 bits (4 octets), tandis que l'adresse de destination prend encore 4 octets. Ce sont les data qui seront fragmenté. Pour faciliter le réassemblage côté destinataire, IP utilise l'identification (ID) et le décalage de fragment (fragment offset).

D'autre part, si vous préférez augmenter la taille de vos paquets pour les rendre inoffensifs, vous pouvez utiliser l'option --data-length NUM, où num spécifie le nombre d'octets que vous souhaitez ajouter à vos paquets.   

## Zombie 

L'usurpation de l'adresse IP source peut être une excellente approche pour analyser furtivement. Cependant, l'usurpation d'identité ne fonctionnera que dans des configurations réseau spécifiques. Il vous oblige à être dans une position où vous pouvez surveiller le trafic. Compte tenu de ces limitations, l'usurpation de votre adresse IP peut avoir peu d'utilité ; cependant, nous pouvons lui donner une mise à niveau avec l'analyse inactive.

L'analyse inactive, ou analyse zombie, nécessite un système inactif connecté au réseau avec lequel vous pouvez communiquer. Pratiquement, Nmap fera apparaître chaque sonde comme si elle provenait de l'hôte inactif (zombie), puis il vérifiera si l'hôte inactif (zombie) a reçu une réponse à la sonde usurpée. Ceci est accompli en vérifiant la valeur d'identification IP (ID IP) dans l'en-tête IP. Vous pouvez exécuter une analyse inactive à l'aide de nmap -sI ZOMBIE_IP MACHINE_IP, où ZOMBIE_IP est l'adresse IP de l'hôte inactif (zombie).

L'analyse inactive (zombie) nécessite les trois étapes suivantes pour découvrir si un port est ouvert :

1. Déclenchez l'hôte inactif pour qu'il réponde afin que vous puissiez enregistrer l'ID IP actuel sur l'hôte inactif.   
2. Envoyez un paquet SYN à un port TCP sur la cible. Le paquet doit être usurpé pour apparaître comme s'il provenait de l'adresse IP de l'hôte inactif (zombie).   
3. Déclenchez à nouveau la machine inactive pour qu'elle réponde afin de pouvoir comparer le nouvel ID IP avec celui reçu précédemment.   
