# Découverte d'hôte en direct

## Introduction

Nmap a été créé par Gordon Lyon (Fyodor), un expert en sécurité réseau et programmeur open source. Il est sorti en 1997. Nmap, abréviation de Network Mapper, est un logiciel open source gratuit publié sous licence GPL. Nmap est un outil standard de l'industrie pour cartographier les réseaux, identifier les hôtes actifs et découvrir les services en cours d'exécution. Le moteur de script de Nmap peut encore étendre ses fonctionnalités, des services d'empreintes digitales à l'exploitation des vulnérabilités.

Comment sauriez-vous quels hôtes sont opérationnels ?   
Il est essentiel d'éviter de perdre notre temps à scanner un port hors ligne ou une adresse IP non utilisée. Il existe différentes manières de découvrir les hébergeurs en ligne. Lorsqu'aucune option de découverte d'hôte n'est fournie, Nmap suit les approches suivantes pour découvrir les hôtes en direct :  

1. Lorsqu'un utilisateur privilégié essaie de scanner des cibles sur un réseau local (Ethernet), Nmap utilise des requêtes ARP . Un utilisateur privilégié est rootou un utilisateur qui appartient à sudoerset peut exécuter sudo.  
2. Lorsqu'un utilisateur privilégié essaie d'analyser des cibles en dehors du réseau local, Nmap utilise les requêtes d'écho ICMP, TCP ACK (accusé de réception) sur le port 80, TCP SYN (synchronisation) sur le port 443 et la requête d'horodatage ICMP.  
3. Lorsqu'un utilisateur non privilégié essaie de scanner des cibles en dehors du réseau local, Nmap recourt à une poignée de main TCP à 3 voies en envoyant des paquets SYN aux ports 80 et 443.  

On peut découper un scan nmap en 9 étapes :   

- Enumerate targets  
- Discover live hosts  
- Reverse-DNS   
- Scan ports   
- Detect versions   
- Detect OS   
- Traceroute   
- Scripts   
- Write output   

ici nous allons voir les 3 premières étapes

## ARP

ARP a un seul but : envoyer une trame à l'adresse de diffusion sur le segment de réseau et demander à l'ordinateur avec une adresse IP spécifique de répondre en fournissant son adresse MAC (matérielle).

L'analyse ARP n'est possible que si vous êtes sur le même sous-réseau que les systèmes cibles. Sur un réseau Ethernet (802.3) et Wi-Fi (802.11), vous devez connaître l'adresse MAC de tout système avant de pouvoir communiquer avec lui. L'adresse MAC est nécessaire pour l'en-tête de la couche liaison ; l'en-tête contient l'adresse MAC source et l'adresse MAC de destination entre autres champs. Pour obtenir l'adresse MAC, le système d'exploitation envoie une requête ARP. Un hôte qui répond aux requêtes ARP est actif. ARP est un protocole de couche liaison et les paquets ARP sont liés à leur sous-réseau.

Dans le cadre de la reconnaissance active, nous voulons découvrir plus d'informations sur un groupe d'hôtes ou sur un sous-réseau. Si vous êtes connecté au même sous-réseau, vous vous attendez à ce que votre scanner s'appuie sur les requêtes ARP (Address Resolution Protocol) pour découvrir les hôtes actifs. Une requête ARP vise à obtenir l'adresse matérielle (adresse MAC) afin que la communication sur la couche liaison devienne possible ; cependant, nous pouvons l'utiliser pour déduire que l'hôte est en ligne.

`nmap -PR -sn TARGETS` : effectue uniquement une analyse ARP sans analyse de port   
Nmap envoie des requêtes ARP à tous les ordinateurs cibles, et ceux en ligne doivent renvoyer une réponse ARP.

autre outil :  
[arp-scan] (http://www.royhills.co.uk/wiki/index.php/Main_Page) : offre de nombreuses options pour personnaliser votre analyse. Un choix populaire est arp-scan --localnet ou simplement `arp-scan -l`. Cette commande enverra des requêtes ARP à toutes les adresses IP valides sur vos réseaux locaux. De plus, si votre système possède plusieurs interfaces et que vous souhaitez découvrir les hôtes actifs sur l'une d'entre elles, vous pouvez spécifier l'interface à l'aide de -I . Par exemple, sudo arp-scan -I eth0 -l enverra des requêtes ARP pour toutes les adresses IP valides sur l'eth0 interface.    
--> il faut peut etre executer la commande en sudo 

## ICMP

ICMP a [plusieurs types] (https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml). Le ping ICMP utilise le type 8 (écho) et le type 0 (réponse d'écho).

Nous pouvons cingler chaque adresse IP sur un réseau cible et voir qui répondrait à nos ping requêtes avec une réponse ping. Simple, n'est-ce pas ? Bien que ce soit l'approche la plus simple, elle n'est pas toujours fiable. De nombreux pare-feu bloquent l'écho ICMP ; les nouvelles versions de MS Windows sont configurées avec un pare-feu hôte qui bloque les requêtes d'écho ICMP par défaut. N'oubliez pas qu'une requête ARP précédera la requête ICMP si votre cible se trouve sur le même sous-réseau.

Pour utiliser la requête d'écho ICMP pour découvrir les hôtes actifs, ajoutez l'option -PE. Une analyse d'écho ICMP fonctionne en envoyant une demande d'écho ICMP et s'attend à ce que la cible réponde avec une réponse d'écho ICMP si elle est en ligne.

Étant donné que les demandes d'écho ICMP ont tendance à être bloquées, vous pouvez également envisager des demandes d'horodatage ICMP ou de masque d'adresse ICMP pour savoir si un système est en ligne. Nmap utilise une requête d'horodatage (ICMP Type 13) et vérifie s'il recevra une réponse d'horodatage (ICMP Type 14). L'ajout de l' -PP option indique à Nmap d'utiliser les requêtes d'horodatage ICMP.

De même, Nmap utilise des requêtes de masque d'adresse (ICMP Type 17) et vérifie s'il reçoit une réponse de masque d'adresse (ICMP Type 18). Cette analyse peut être activée avec l'option -PM. 

--> Comme certains paquets sont bloqué, il est essentiel d'apprendre plusieurs approches pour obtenir le même résultat. Si un type de paquet est bloqué, nous pouvons toujours en choisir un autre pour découvrir le réseau et les services cibles.

## TCP et UDP

Bien que TCP et UDP soient des couches de transport, à des fins d'analyse du réseau, un analyseur peut envoyer un paquet spécialement conçu aux ports TCP ou UDP communs pour vérifier si la cible répondra. Cette méthode est efficace, surtout lorsque ICMP Echo est bloqué.

### Ping de synchronisation TCP

Nous pouvons envoyer un paquet avec le drapeau SYN (Synchronize) défini sur un port TCP , 80 par défaut, et attendre une réponse. Un port ouvert doit répondre par un SYN/ACK (accusé de réception) ; un port fermé entraînerait un RST (Reset). Dans ce cas, nous vérifions uniquement si nous obtiendrons une réponse pour déduire si l'hôte est opérationnel. L'état spécifique du port n'est pas significatif ici. 

Pour rappel, le fonctionnement habituel d'une poignée de main TCP à trois requete : client envoie un SYN, le serveur répond SYN,ACK et le client lui répond ACK.

Si vous souhaitez que Nmap utilise le ping TCP SYN, vous pouvez le faire via l'option -PS suivie du numéro de port, de la plage, de la liste ou d'une combinaison de ceux-ci. Par exemple :   
- -PS21 ciblera le port 21  
- -PS21-25 ciblera les ports 21, 22, 23, 24 et 25   
- -PS80,443,8080 ciblera les trois ports 80, 443 et 8080    

Remarque : Les utilisateurs privilégiés (root et sudoers) peuvent envoyer des paquets TCP SYN et n'ont pas besoin de terminer la poignée de main TCP à 3 voies même si le port est ouvert. Les utilisateurs non privilégiés n'ont d'autre choix que de terminer la poignée de main à trois si le port est ouvert.

### Ping TCP ACK

Cela envoie un paquet avec un drapeau ACK défini. Tout paquet TCP avec un indicateur ACK doit récupérer un paquet TCP avec un indicateur RST défini. La cible répond avec l'indicateur RST défini car le paquet TCP avec l'indicateur ACK ne fait partie d'aucune connexion en cours. La réponse attendue est utilisée pour détecter si l'hôte cible est actif.

Par défaut, le port 80 est utilisé. La syntaxe est similaire au ping TCP SYN. L'option est -PA qui doit être suivi d'un numéro de port, d'une plage, d'une liste ou d'une combinaison de ceux-ci.

Remarque : Vous devez exécuter Nmap en tant qu'utilisateur privilégié pour pouvoir accomplir cela. Si vous l'essayez en tant qu'utilisateur non privilégié, Nmap tentera une poignée de main à trois voies.

### Ping UDP

Enfin, nous pouvons utiliser UDP pour découvrir si l'hôte est en ligne. Contrairement au ping TCP SYN, l'envoi d'un paquet UDP à un port ouvert ne devrait pas conduire à une réponse, cela indique que le système cible est opérationnel et disponible. Cependant, si nous envoyons un paquet UDP à un port UDP fermé, nous nous attendons à recevoir un paquet ICMP port inaccessible.

La syntaxe pour spécifier les ports est similaire à celle du ping TCP SYN et du ping TCP ACK ; Nmap utilise -PU pour le ping UDP.

Nmap envoie des paquets UDP aux ports UDP qui sont très probablement fermés.

autre outil : 
masscan : utilise une approche similaire pour découvrir les systèmes disponibles. Cependant, pour finir son scan réseau rapidement, Masscan est assez agressif avec le taux de paquets qu'il génère. La syntaxe est assez similaire : -p peut être suivi d'un numéro de port, d'une liste ou d'une plage.