# Analyses de ports de base

La prochaine étape serait de vérifier quels ports sont ouverts et à l'écoute et quels ports sont fermés donc l'étape 4.

## rappel port TCP et UDP

Dans le même sens qu'une adresse IP spécifie un hôte sur un réseau parmi tant d'autres, un port TCP ou un port UDP est utilisé pour identifier un service réseau exécuté sur cet hôte. Un serveur fournit le service réseau et adhère à un protocole réseau spécifique. Un port est généralement lié à un service utilisant ce numéro de port spécifique.   
Par exemple, un serveur HTTP se lierait au port TCP 80 par défaut ; de plus, si le serveur HTTP prend en charge SSL/TLS, il écoutera sur le port TCP 443 par défaut. De plus, pas plus d'un service ne peut écouter sur n'importe quel port TCP ou UDP (sur la même adresse IP).

Selon nmap, un port peut être classés selon 6 états :

- Ouvert : indique qu'un service écoute sur le port spécifié.  
- Fermé : indique qu'aucun service n'écoute sur le port spécifié, bien que le port soit accessible. Par accessible, nous entendons qu'il est accessible et qu'il n'est pas bloqué par un pare-feu ou d'autres dispositifs/programmes de sécurité.  
- Filtré : signifie que Nmap ne peut pas déterminer si le port est ouvert ou fermé car le port n'est pas accessible. Cet état est généralement dû à un pare-feu empêchant Nmap d'atteindre ce port. Les paquets de Nmap peuvent être empêchés d'atteindre le port ; alternativement, les réponses sont empêchées d'atteindre l'hôte de Nmap.  
- Non filtré : signifie que Nmap ne peut pas déterminer si le port est ouvert ou fermé, bien que le port soit accessible. Cet état est rencontré lors de l'utilisation d'un scan ACK -sA.  
- Ouvert|Filtré : Cela signifie que Nmap ne peut pas déterminer si le port est ouvert ou filtré.   
- Fermé|Filtré : Cela signifie que Nmap ne peut pas décider si un port est fermé ou filtré.   

## analyse port TCP 

### intro TCP

L'en-tête TCP correspond aux 24 premiers octets d'un segment TCP. Nous avons le numéro de port TCP source et le numéro de port de destination. NLe numéro de port est alloué sur 16 bits (2 octets). Ensuite, nous avons le numéro de séquence et le numéro d'accusé de réception.

Il y a également 6 indicateurs intéressant :  

- URG : indique que les données entrantes sont urgentes et qu'un segment TCP avec l'indicateur URG défini est traité immédiatement sans considération d'avoir à attendre des segments TCP précédemment envoyés.   
- ACK : est utilisé pour accuser réception d'un segment TCP.  
- PSH : demande à TCP de transmettre rapidement les données à l'application.  
- RST : est utilisé pour réinitialiser la connexion. Un autre périphérique, tel qu'un pare-feu, peut l'envoyer pour interrompre une connexion TCP. Cet indicateur est également utilisé lorsque des données sont envoyées à un hôte et qu'il n'y a pas de service du côté récepteur pour répondre.  
- SYN : est utilisé pour initier une poignée de main TCP à 3 voies et synchroniser les numéros de séquence avec l'autre hôte. Le numéro de séquence doit être défini de manière aléatoire lors de l'établissement de la connexion TCP.   
- FIN : L'expéditeur n'a plus de données à envoyer.   

### connexion TCP

Nous sommes intéressés à savoir si le port TCP est ouvert, et non à établir une connexion TCP. La connexion est donc rompue dès que son état est confirmé par l'envoi d'un RST/ACK (apèrs la terminaison de la prise de contact en trois étapes en envoyant un ACK).   

Vous pouvez choisir d'exécuter l'analyse de connexion TCP à l'aide de `-sT`.  
par défaut nmap scanne les 1000 ports les plus courants

remarque : si vous n'êtes pas un utilisateur privilégié (root ou sudoer), une analyse de connexion TCP est la seule option possible pour découvrir les ports TCP ouverts.  

### balayage SYN

Cependant, le mode d'analyse par défaut est l'analyse SYN et nécessite un utilisateur privilégié (root ou sudoer) pour l'exécuter. L'analyse SYN n'a pas besoin de terminer la poignée de main TCP à 3 voies ; au lieu de cela, il interrompt la connexion une fois qu'il reçoit une réponse du serveur. Comme nous n'avons pas établi de connexion TCP, cela réduit les chances que l'analyse soit enregistrée. Nous pouvons sélectionner ce type d'analyse en utilisant l'`-sS` option.

--> necessite l'execution en tant que root   

## UDP

UDP est un protocole sans connexion et ne nécessite donc aucune prise de contact pour l'établissement de la connexion. Nous ne pouvons pas garantir qu'un service écoutant sur un port UDP répondra à nos paquets. Cependant, si un paquet UDP est envoyé à un port fermé, une erreur de port ICMP inaccessible (type 3, code 3) est renvoyée. En d'autres termes, les ports UDP qui ne génèrent aucune réponse sont ceux que Nmap indiquera comme ouverts.   

Vous pouvez sélectionner l'analyse UDP à l'aide de l'`-sU` option ; de plus, vous pouvez le combiner avec un autre scan TCP.s

## Ajustement

- les ports   

Vous pouvez spécifier les ports que vous souhaitez analyser au lieu des 1000 ports par défaut.

- liste des ports : -p22,80,443 : analysera les ports 22, 80 et 443.   
- plage de ports : -p1-1023 analysera tous les ports entre 1 et 1023 inclus   

Vous pouvez demander l'analyse de tous les ports en utilisant -p-, qui analysera tous les 65535 ports. Si vous souhaitez analyser les 100 ports les plus courants, ajoutez -F. L'utilisation --top-ports 10 de vérifiera les dix ports les plus courants.

- le temps   

Vous pouvez contrôler la synchronisation de l'analyse à l'aide de -T<0-5>. -T0 est le plus lent (paranoïaque), tandis que -T5 est le plus rapide. Selon la page de manuel de Nmap, il existe six modèles :

- paranoïaque (0)    
- sournois (1)   
- poli (2)   
- normal (3)   
- agressif (4)   
- fou (5)   

Pour éviter les alertes IDS, vous pouvez envisager -T0 ou -T1. Par exemple, -T0 analyse un port à la fois et attend 5 minutes entre l'envoi de chaque sonde, de sorte que vous pouvez deviner combien de temps l'analyse d'une cible prendrait pour se terminer. 

Si vous ne spécifiez aucun timing, Nmap utilise normal -T3. 

Notez que -T5 c'est le plus agressif en termes de vitesse; cependant, cela peut affecter la précision des résultats de l'analyse en raison de la probabilité accrue de perte de paquets. 

Notez qu -T4 est souvent utilisé lors des CTF et lors de l'apprentissage du balayage sur des cibles d'entraînement, alors que -T1est souvent utilisé lors d'engagements réels où la furtivité est plus importante.

- débit    

Alternativement, vous pouvez choisir de contrôler le débit de paquets en utilisant --min-rate [nombre] et --max-rate [nombre]. Par exemple, --max-rate 10 ou --max-rate=10 s'assure que votre scanner n'envoie pas plus de dix paquets par seconde.

- la parrélisation 

De plus, vous pouvez contrôler la parallélisation du sondage en utilisant --min-parallelism [nombre] et --max-parallelism [nombre]. Nmap sonde les cibles pour découvrir quels hôtes sont actifs et quels ports sont ouverts ; la parallélisation des sondes spécifie le nombre de telles sondes qui peuvent être exécutées en parallèle. Par exemple, --min-parallelism=512 pousse Nmap à maintenir au moins 512 sondes en parallèle ; ces 512 sondes sont liées à la découverte d'hôtes et aux ports ouverts.