# Résumé

## Option

### Spécification de la cible :  

- list : MACHINE_IP scanme.nmap.org example.com va scanner 3 adresses IP.   
- plage : 10.11.12.15-20 va scanner 6 adresses IP : 10.11.12.15, 10.11.12.13.16,… et 10.11.12.13.20.   
- sous-réseau : MACHINE_IP/30 analysera 4 adresses IP.   
- un fichier : pour définir votre liste de cibles, -iL list_of_hosts.txt  

### Découverte hote

exemple :  `nmap -PR -sn 10.10.210.6/24`   

-PR : que une ARP analyse (sudo)
-PE : analyse ICMP (avec echo)
-PP : analyse avec horodatage ICMP (ICMP timestamp)
-PM : analyse avec masque adresse ICMP (ICMP address Mask)
-PS : ping TCP SYN
-PA : ping TCP ACK (sudo)
-PU : ping UDP

pour spécifier les ports pour le ping :    

- -PS21 ciblera le port 21  
- -PS21-25 ciblera les ports 21, 22, 23, 24 et 25   
- -PS80,443,8080 ciblera les trois ports 80, 443 et 8080  

### Analyse des ports

exemple : `nmap -sT [IP]`   

-sT : analyse connexion TCP complète   
-sS : analyse TCP SYN (sudo)   
-sU : analyse UDP (sudo)  

-sN : analyse null (sudo)   
-sF : analyse FIN (sudo)   
-sX : Xmas analyse (sudo)   
-sM : analyse maimon (sudo)   
-sA : analyse ACK (sudo)    
-sW : analyse fenetre    
--scanflags[Indicateur] : analyse TCP personalisé, choisir quel indicateur on met, exemple : --scanflagsRST  

### spécifier les ports  

par défaut les 1000 ports les plus courants   

-p[port] : -p22,80,443 analysera les ports 22, 80 et 443 / -p1-1023 analysera tous les ports entre 1 et 1023 inclus   
-p- : analyse tous les ports, 65535 ports   
--top-ports 10 : vérifera les dix ports les plus courants   
-F : active le mode rapide et diminue le nombre de ports scanné, seulement les 100 les plus courants   
-r : analyser les ports dans un ordre consécutif au lieu d'un ordre aléatoire   

--source-port [PORT_NUM] : spécifier le numéro de port source   

### usurpation 

-S [Spoofed IP] : Ip source usurpé   
--spoof-mac (Spoofed mac) : adresse mac usurpés   

### leurre 

-D [DECOY_IP],...,ME : Balayage leurre, mettre autant de decoy qu'on veut, le ME c'est notre adresse on peut le mettre où on veut dans la liste (cela a un impact)   
-sI [ZOMBIE_IP] : Balayage inactif (Zombie)   

### fragmenter les données

-f : Fragmenter les données IP en 8 octets	   
-ff : Fragmentez les données IP en 16 octets  

--data-length [NUM] : ajouter des données aléatoires pour atteindre une longueur donnée   

### version, système

-sV : déterminer les informations de service/version sur les ports ouverts   
--version-intensity [LEVEL] : pour définir l'intensité de la recherche de version   
-sV --version-light : essai les sondes les plus probables (level=2)
-sV --version-all : essai toutes les sondes disponibles (level=9)

-O	détecter le système d'exploitation

### script

-sC : exécute les scripts par défaut (comme --script=default)  
--script=[catégorie] : exécute les script de la catégorie   
--script "[SCRIPT-NAME]" : exécute le script indiqué (on peut aussi spécifier un modèle comme "ftp*")   

les catégories sont : auth, broadcast, brute, default, discovery, dos, exploit, external, fuzzer, intrusive, malware, safe, version et vuln   

### sortie fichier

permet de sauvegarder le resultat de nmap dans un fichier de différents types   

-oN : enregistrer la sortie au format normal    
-oG : enregistrer la sortie au format grepable   
-oX : enregistrer la sortie au format XML   
-oA : enregistrer la sortie aux formats normal, XML et Grepable   
-oS : enregistrer la sortie au format script kiddies

### le temps 

-T<0-5> : définit le délais entre chaque délais. 0 est le plus lent et 5 le plus rapide

- paranoïaque (0)   
- sournois (1)   
- poli (2)   
- normal (3)   
- agressif (4)   
- fou (5)   

### le débit 

défini le nombre de requete minimum ou maximum à envoyer

--min-rate [nombre]   
--max-rate [nombre]   

### parallélisme

défni le nombre de sonde executé en parallèle 

--min-parallelism [nombre]   
--max-parallelism [nombre]   

### Autres

-sL : liste les hotes que nmap va analyser sans les analyser (une résolution DNS est effectué par défaut aussi)    
-sn : découvre les hotes sans analyse de ports    
-n : pas de recherche DNS (nmap le fait par défaut)   
-R : interroge le serveur DNS meme pour les hotes hors lignes   
--dns-servers [serveur DNS] : spéicifer un serveur dns spécifique   
--reason : explique comment Nmap est arrivé à sa conclusion   
-v : verbeux (-vv : très verbeux)   
-d : débogage (-dd : plus de détails pour le débogage)
-A : équivalent à- sV -O -sC --traceroute
--traceroute : exécute traceroute vers la cible

## Astuce 

beaucoup d'option ont besoin de sudo

commande générale : `sudo nmap -v -Pn -sS -sV -sC [IP]`
