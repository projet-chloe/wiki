# Analyse plus poussé après les ports

Maintenant nous nous concentrons sur les dernières étapes :

- Détecter les versions des services en cours d'exécution (sur tous les ports ouverts)   
- Détecter le système d' exploitation en fonction de tout signe révélé par la cible    
- Exécutez le traceroute de Nmap    
- Exécutez des scripts Nmap sélectionnés    
- Enregistrez les résultats de l'analyse dans différents formats    

## Version

Une fois que Nmap a découvert les ports ouverts, vous pouvez sonder le port disponible pour détecter le service en cours d'exécution. Une enquête plus approfondie sur les ports ouverts est une information essentielle car le pentester peut l'utiliser pour savoir s'il existe des vulnérabilités connues du service.

L'ajout -sV à votre commande Nmap collectera et déterminera les informations de service et de version pour les ports ouverts. Vous pouvez contrôler l'intensité avec --version-intensity LEVEL un niveau compris entre 0, le plus léger, et 9, le plus complet. -sV --version-light a une intensité de 2, alors qu'il -sV --version-all a une intensité de 9.

Il est important de noter que l'utilisation -sV forcera Nmap à procéder à la poignée de main TCP à 3 voies et à établir la connexion. L'établissement de la connexion est nécessaire car Nmap ne peut pas découvrir la version sans établir une connexion complète et communiquer avec le service d'écoute. En d'autres termes, l'analyse SYN furtive -sS n'est pas possible lorsque l'-sV option est choisie.

Remarque : sudo est necessaire

## Détection du système d'exploitation

Nmap peut détecter le système d'exploitation (OS) en fonction de son comportement et de tout signe révélateur dans ses réponses. La détection du système d'exploitation peut être activée à l'aide de -O. 

La détection du système d'exploitation est très pratique, mais de nombreux facteurs peuvent affecter sa précision. Tout d'abord, Nmap doit trouver au moins un port ouvert et un port fermé sur la cible pour faire une estimation fiable. De plus, les empreintes digitales du système d'exploitation invité peuvent être déformées en raison de l'utilisation croissante de la virtualisation et de technologies similaires. Par conséquent, prenez toujours la version du système d'exploitation avec un grain de sel.

## Traceroute

Si vous voulez que Nmap trouve les routeurs entre vous et la cible, ajoutez simplement --traceroute. Notez que le traceroute de Nmap fonctionne légèrement différemment de la traceroute commande trouvée sur Linux et macOS outracert trouvée sur MS Windows. Standard traceroute commence par un paquet de faible TTL (Time to Live) et continue d'augmenter jusqu'à ce qu'il atteigne la cible. Le traceroute de Nmap commence par un paquet de TTL élevé et continue de le diminuer.

Il est à noter que de nombreux routeurs sont configurés pour ne pas envoyer ICMP Time-to-Live dépassé, ce qui nous empêcherait de découvrir leurs adresses IP.

## Script
  
Un script est un morceau de code qui n'a pas besoin d'être compilé. En d'autres termes, il reste dans sa forme originale lisible par l'homme et n'a pas besoin d'être converti en langage machine. De nombreux programmes fournissent des fonctionnalités supplémentaires via des scripts ; de plus, des scripts permettent d'ajouter des fonctionnalités personnalisées qui n'existaient pas via les commandes intégrées.   
De même, Nmap prend en charge les scripts utilisant le langage Lua. Une partie de Nmap, Nmap Scripting Engine (NSE) est un interpréteur Lua qui permet à Nmap d'exécuter des scripts Nmap écrits en langage Lua.

Votre installation par défaut de Nmap peut facilement contenir près de 600 scripts. Jetez un oeil à votre dossier d'installation Nmap : /usr/share/nmap/scripts. Vous remarquerez qu'il existe des centaines de scripts nommés de manière pratique en commençant par le protocole qu'ils ciblent.  

Vous pouvez spécifier d'utiliser n'importe lequel ou un groupe de ces scripts installés ; de plus, vous pouvez installer les scripts d'autres utilisateurs et les utiliser pour vos scans. Commençons par les scripts par défaut. Vous pouvez choisir d'exécuter les scripts dans la catégorie par défaut en utilisant --script=default ou simplement en ajoutant -sC. En plus de default, il y a de nombreuses catégorie :   

- auth : Scripts liés à l'authentification    
- broadcast : Découvrez les hôtes en envoyant des messages de diffusion    
- brute : Effectue un audit de mot de passe par force brute contre les connexions    
- default : Scripts par défaut, identiques à-sC    
- discovery : Récupérer des informations accessibles, telles que des tables de base de données et des noms DNS   
- dos : Détecte les serveurs vulnérables au déni de service ( DoS )    
- exploit : Tentatives d'exploitation de divers services vulnérables   
- external : Vérifications à l'aide d'un service tiers, tel que Geoplugin et Virustotal    
- fuzzer : Lancer des attaques fuzzing    
- intrusive : Scripts intrusifs tels que les attaques par force brute et l'exploitation    
- malware : Analyse les portes dérobées   
- safe : Des scripts sûrs qui ne planteront pas la cible    
- version : Récupérer les versions de service    
- vuln : Vérifie les vulnérabilités ou exploite les services vulnérables   

Certains scripts appartiennent à plusieurs catégories. De plus, il est crucial d'être prudent lors de la sélection des scripts à exécuter si vous ne voulez pas planter les services ou les exploiter.

Vous pouvez également spécifier le script par son nom en utilisant --script "SCRIPT-NAME" ou un modèle tel que --script "ftp*", qui inclurait ftp-brute. Si vous n'êtes pas sûr de ce que fait un script, vous pouvez ouvrir le fichier de script avec un lecteur de texte. Il faut faire attention car certains scripts sont assez intrusifs. De plus, certains scripts peuvent être destinés à un serveur spécifique et, s'ils sont choisis au hasard, ils vous feront perdre du temps sans aucun avantage.

Enfin, vous pouvez étendre les fonctionnalités de Nmap au-delà des scripts Nmap officiels ; vous pouvez écrire votre script ou télécharger des scripts Nmap depuis Internet. Le téléchargement et l'utilisation d'un script Nmap à partir d'Internet comportent un certain niveau de risque. C'est donc une bonne idée de ne pas exécuter un script d'un auteur en qui vous n'avez pas confiance.

## enregistrement de la sortie

Chaque fois que vous exécutez une analyse Nmap, il est raisonnable d'enregistrer les résultats dans un fichier. Les formats sont :   

- Normal :   
Comme son nom l'indique, le format normal est similaire à la sortie que vous obtenez à l'écran lors de la numérisation d'une cible. Vous pouvez enregistrer votre numérisation au format normal en utilisant -oN FILENAME; N signifie normal.  

- Grépable :   
Le format grepable tire son nom de la commande grep; grep signifie Global Regular Expression Printer. En termes simples, cela rend efficace le filtrage de la sortie d'analyse pour des mots-clés ou des termes spécifiques. Un exemple d'utilisation de grep est grep KEYWORD TEXT_FILE; cette commande affichera toutes les lignes contenant le mot clé fourni.   
Vous pouvez enregistrer le résultat de l'analyse au format grepable à l'aide de -oG FILENAME. Cependant, la sortie grepable a moins de lignes. La raison principale est que Nmap veut rendre chaque ligne significative et complète lorsque l'utilisateur s'applique grep. En conséquence, dans une sortie grepable, les lignes sont si longues et ne sont pas pratiques à lire par rapport à une sortie normale.

- XML :    
Le troisième format est XML . Vous pouvez enregistrer les résultats de l'analyse au format XML à l'aide de -oX FILENAME. Le format XML serait le plus pratique pour traiter la sortie dans d'autres programmes. 

--> De manière assez pratique, vous pouvez enregistrer la sortie de numérisation dans les trois formats en utilisant-oA FILENAME pour combiner -oN, -oG et -oX pour normal, grepable et XML .

- Script kiddie :   
Un quatrième format est script kiddie. Vous pouvez voir que ce format est inutile si vous souhaitez rechercher dans la sortie des mots-clés intéressants ou conserver les résultats pour référence future. Cependant, vous pouvez l'utiliser pour enregistrer la sortie de l'analyse nmap -sS 127.0.0.1 -oS FILENAME, afficher le nom du fichier de sortie et egarder 31337 devant des amis qui ne sont pas férus de technologie.
