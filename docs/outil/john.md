# John The Ripper

## Introduction

John the Ripper est l'un des outils de craquage de hachage les plus connus, les plus appréciés et les plus polyvalents. Il combine une vitesse de craquage rapide et une gamme extraordinaire de types de hachage compatibles. Cette salle ne supposera aucune connaissance préalable, nous devons donc d'abord couvrir certains termes et concepts de base avant de passer au craquage de hachage pratique.

- Que sont les hachages ?    
Un hachage est un moyen de prendre une donnée de n'importe quelle longueur et de la représenter sous une autre forme de longueur fixe. Cela masque la valeur d'origine des données. Cela se fait en exécutant les données d'origine via un algorithme de hachage. Il existe de nombreux algorithmes de hachage populaires, tels que MD4, MD5, SHA1 et NTLM.

- Qu'est-ce qui rend les hachages sécurisés ?     
Les algorithmes de hachage sont conçus pour ne fonctionner que dans un sens. Cela signifie qu'un hachage calculé ne peut pas être inversé en utilisant uniquement la sortie donnée. Cela renvoie à un problème mathématique fondamental connu sous le nom de relation P vs NP .

Cela signifie que l'algorithme pour hacher la valeur sera "NP" et pourra donc être calculé raisonnablement. Cependant, un algorithme de non-hachage serait "P" et difficile à résoudre, ce qui signifie qu'il ne peut pas être calculé dans un délai raisonnable à l'aide d'ordinateurs standard.

- Où John entre en jeu...    
Même si l'algorithme lui-même n'est pas réversible. Cela ne signifie pas qu'il est impossible de casser les hachages. Si vous avez la version hachée d'un mot de passe, par exemple - et que vous connaissez l'algorithme de hachage - vous pouvez utiliser cet algorithme de hachage pour hacher un grand nombre de mots, appelé dictionnaire. Vous pouvez ensuite comparer ces hachages à celui que vous essayez de casser, pour voir si l'un d'entre eux correspond. S'ils le font, vous savez maintenant quel mot correspond à ce hachage - vous l'avez déchiffré !

Ce processus s'appelle une attaque par dictionnaire et John the Ripper, ou John comme il est généralement abrégé, est un outil qui vous permet de mener des attaques rapides par force brute sur un large éventail de types de hachage différents.

## Wordlist

Comme nous l'avons expliqué, pour attaquer les hachages par dictionnaire, vous avez besoin d'une liste de mots que vous pouvez hacher et comparer, sans surprise, cela s'appelle une liste de mots. Il existe de nombreuses listes de mots différentes, une bonne collection à utiliser peut être trouvée dans le référentiel SecLists. Il y a quelques endroits où vous pouvez rechercher des listes de mots sur votre système d'attaque de choix, nous allons rapidement parcourir où vous pouvez les trouver.

Il y a également la célèbre liste de mots rockyou.txt - qui est une très grande liste de mots de passe commune, obtenue à partir d'une violation de données sur un site Web appelé rockyou.com en 2009. Si elle n'estpas de base sur votre distribution, vous pouvez obtenir la liste de mots rockyou.txt à partir du référentiel SecLists sous la sous section /Passwords/Leaked-Databases. Vous devrez peut-être l'extraire du format .tar.gz, en utilisant tar xvzf rockyou.txt.tar.gz.

## Hash de base

### Syntaxe de base de John

La syntaxe de base des commandes de John the Ripper est la suivante.

`john [options] [file]`    
file : Le fichier contenant le hachage que vous essayez de casser, s'il se trouve dans le même répertoire, vous n'aurez pas besoin de nommer un chemin, juste le fichier.

### Craquage automatique

John a des fonctionnalités intégrées pour détecter le type de hachage qui lui est donné et pour sélectionner les règles et les formats appropriés pour le déchiffrer pour vous, ce n'est pas toujours la meilleure idée car il peut ne pas être fiable, mais si vous ne pouvez pas identifier avec quel type de hachage vous travaillez et que vous voulez juste essayer de le casser, cela peut être une bonne option ! Pour ce faire, nous utilisons la syntaxe suivante :

`john --wordlist=[path to wordlist] [file]`    
--wordlist : Spécifie l'utilisation du mode liste de mots

### Identification des hachages
Parfois, John ne jouera pas bien avec la reconnaissance et le chargement automatiques des hachages, ce n'est pas grave ! Nous pouvons utiliser d'autres outils pour identifier le hachage, puis configurer john pour qu'il utilise un format spécifique. Il existe plusieurs façons de procéder, comme l'utilisation d'un identifiant de hachage en ligne comme celui- ci. J'aime utiliser un outil appelé hash-identifier , un outil Python très facile à utiliser et qui vous dira quels types de hachages celui que vous entrez est susceptible d'être, vous donnant plus d'options si le premier échoue.

Pour utiliser hash-identifier, vous pouvez simplement extraire le fichier python de gitlab en utilisant : `wget https://gitlab.com/kalilinux/packages/hash-identifier/-/raw/kali/master/hash-id.py`

Ensuite, lancez-le simplement avec `python3 hash-id.py` puis entrez le hachage que vous essayez d'identifier - et cela vous donnera les formats possibles !

### Craquage spécifique au format
Une fois que vous avez identifié le hachage avec lequel vous avez affaire, vous pouvez dire à john de l'utiliser tout en cassant le hachage fourni en utilisant la syntaxe suivante :

`john --format=[format] --wordlist=[path to wordlist] [file]`    
--format : C'est le drapeau pour dire à John que vous lui donnez un hachage d'un format spécifique, et pour utiliser le format suivant pour le déchiffrer

exemple --format=raw-md5 

Une note sur les formats :     
Lorsque vous dites à john d'utiliser des formats, si vous avez affaire à un type de hachage standard, par exemple md5 comme dans l'exemple ci-dessus, vous devez le préfixer avec raw- pour dire à john que vous n'avez affaire qu'à un type de hachage standard, bien que cela ne s'applique pas toujours. Pour vérifier si vous devez ajouter le préfixe ou non, vous pouvez répertorier tous les formats de John en utilisant `john --list=formats` et vérifier manuellement ou grep pour votre type de hachage en utilisant quelque chose comme `john --list=formats | grep -iF "md5"`.

## Hash authentification windows 

### Craquage des hachages Windows

Maintenant que nous comprenons la syntaxe et l'utilisation de base de John the Ripper, passons à la résolution de quelque chose d'un peu plus difficile, quelque chose que vous voudrez peut-être même essayer si vous participez à un véritable test de pénétration ou à un engagement de l'équipe rouge. Les hachages d'authentification sont les versions hachées des mots de passe qui sont stockés par les systèmes d'exploitation, il est parfois possible de les déchiffrer en utilisant les méthodes de force brute que nous utilisons.

### NTHash / NTLM
NThash est le format de hachage dans lequel les machines modernes du système d'exploitation Windows stockent les mots de passe des utilisateurs et des services. Il est également communément appelé "NTLM" qui fait référence à la version précédente du format Windows pour le hachage des mots de passe connu sous le nom de "LM", donc "NT/LM ".

Un peu d'histoire, la désignation NT pour les produits Windows signifiait à l'origine "Nouvelle technologie" et était utilisée - à partir de Windows NT , pour désigner les produits qui n'étaient pas construits à partir du système d'exploitation MS-DOS. Finalement, la ligne "NT" est devenue le type de système d'exploitation standard publié par Microsoft et le nom a été abandonné, mais il vit toujours dans les noms de certaines technologies Microsoft. 

Vous pouvez acquérir des hachages NTHash/NTLM en vidant la base de données SAM sur une machine Windows, en utilisant un outil comme Mimikatz ou à partir de la base de données Active Directory : NTDS.dit. Vous n'aurez peut-être pas à déchiffrer le hachage pour continuer l'escalade des privilèges - car vous pouvez souvent mener une attaque "passer le hachage" à la place, mais parfois le hash cracking est une option viable s'il existe une politique de mot de passe faible.

## les hach depuis /etc/shadow

Le fichier /etc/shadow est le fichier sur les machines Linux où les hachages de mot de passe sont stockés. Il stocke également d'autres informations, telles que la date du dernier changement de mot de passe et les informations d'expiration du mot de passe. Il contient une entrée par ligne pour chaque utilisateur ou compte utilisateur du système. Ce fichier n'est généralement accessible qu'à l'utilisateur root. Par conséquent, pour mettre la main sur les hachages, vous devez disposer de privilèges suffisants, mais si vous le faites, il est possible que vous puissiez casser certains des hachages

### unshadow

John peut être très particulier sur les formats dans lesquels il a besoin de données pour pouvoir travailler avec, pour cette raison - afin de déchiffrer les mots de passe /etc/shadow, vous devez le combiner avec le fichier /etc/passwd pour que John puisse comprendre les données qui lui sont données. Pour ce faire, nous utilisons un outil intégré à la suite d'outils John appelé unshadow. La syntaxe de base de unshadow est la suivante :

`unshadow [path to passwd] [path to shadow]`    
unshadow- Invoque l'outil de suppression d'ombre     
[path to passwd]- Le fichier qui contient la copie du fichier /etc/passwd que vous avez récupéré sur la machine cible     
[path to shadow]- Le fichier qui contient la copie du fichier /etc/shadow que vous avez récupéré sur la machine cible     

Exemple d'utilisation : `unshadow local_passwd local_shadow > unshadowed.txt`

### Craquage
Nous sommes alors en mesure d'alimenter la sortie de unshadow, dans notre exemple de cas d'utilisation appelé "unshadowed.txt" directement dans John. Nous ne devrions pas avoir besoin de spécifier un mode ici car nous avons fait l'entrée spécifiquement pour John, cependant dans certains cas, vous devrez spécifier le format comme nous l'avons fait précédemment en utilisant :--format=sha512crypt

`john --wordlist=/usr/share/wordlists/rockyou.txt --format=sha512crypt unshadowed.txt`

## Mot unique

Jusqu'à présent, nous avons utilisé le mode liste de mots de John pour gérer le forçage simple et les hachages pas si simples. Mais John a également un autre mode, appelé mode Single Crack. Dans ce mode, John utilise uniquement les informations fournies dans le nom d'utilisateur, pour essayer de trouver des mots de passe possibles de manière heuristique, en modifiant légèrement les lettres et les chiffres contenus dans le nom d'utilisateur.

### Manipulation de mots
La meilleure façon de montrer ce qu'est le mode Single Crack et ce qu'est la manipulation de mots est de passer par un exemple :

Si on prend le nom d'utilisateur : Markus

Certains mots de passe possibles pourraient être :    
- Markus1, Markus2, Markus3 (etc.)     
- MARkus, MARkus, MARkus (etc.)     
- Markus!, Markus$, Markus* (etc.)     
Cette technique s'appelle la manipulation de mots. John construit son propre dictionnaire sur la base des informations qu'il a reçues et utilise un ensemble de règles appelées "règles de manipulation" qui définissent comment il peut muter le mot avec lequel il a commencé pour générer une liste de mots basée sur des facteurs pertinents pour la cible que vous essayez de craquer. Cela exploite la façon dont les mots de passe médiocres peuvent être basés sur des informations sur le nom d'utilisateur ou le service auquel ils se connectent.

### GÉCOS
L'implémentation de John de la manipulation de mots offre également une compatibilité avec les champs Gecos du système d'exploitation UNIX et d'autres systèmes d'exploitation de type UNIX tels que Linux . Que sont donc les Gecos ? Chacun des champs dans lesquels ces enregistrements sont divisés est appelé champs Gecos. John peut prendre des informations stockées dans ces enregistrements, telles que le nom complet et le nom du répertoire personnel pour les ajouter à la liste de mots qu'il génère lors du crackage des hachages /etc/shadow avec le mode de crack unique.

### Utilisation du mode de craquage unique
Pour utiliser le mode single crack, nous utilisons à peu près la même syntaxe que celle que nous avons utilisée jusqu'à présent, par exemple si nous voulions cracker le mot de passe de l'utilisateur nommé "Mike", en utilisant le mode single, nous utiliserions :

`john --single --format=[format] [path to file]`

Une note sur les formats de fichiers en mode Single Crack :     
Si vous cassez des hachages en mode de crack unique, vous devez modifier le format de fichier que vous fournissez à John pour qu'il comprenne à partir de quelles données créer une liste de mots. Pour ce faire, ajoutez au hash le nom d'utilisateur auquel appartient le hachage, donc selon l'exemple nous changerions le fichier hashes.txt

De 1efee03cdcb96d90ad48ccc7b8666033      
À Mike:1efee03cdcb96d90ad48ccc7b8666033    

## Règles personnalisées

### Que sont les règles personnalisées ? 

Au fur et à mesure que nous parcourions notre exploration de ce que John peut faire en mode Single Crack, vous pouvez avoir quelques idées sur ce que seraient de bons modèles de manipulation, ou sur les modèles que vos mots de passe utilisent souvent, qui pourraient être reproduits avec un certain modèle de manipulation. La bonne nouvelle est que vous pouvez définir vos propres ensembles de règles, que John utilisera pour créer dynamiquement des mots de passe. Ceci est particulièrement utile lorsque vous connaissez plus d'informations sur la structure du mot de passe de votre cible, quelle qu'elle soit.

### Règles personnalisées courantes

De nombreuses organisations auront besoin d'un certain niveau de complexité de mot de passe pour essayer de combattre les attaques par dictionnaire.

Un mot de passe avec la lettre majuscule en premier et un chiffre suivi d'un symbole à la fin sont très courants. Ce modèle de mot de passe familier, ajouté et précédé de modificateurs (tels que la lettre majuscule ou des symboles) est un modèle mémorable que les gens utiliseront et réutiliseront lorsqu'ils créeront des mots de passe. Ce modèle peut nous permettre d'exploiter la prévisibilité de la complexité des mots de passe.

Maintenant, cela répond aux exigences de complexité des mots de passe, mais en tant qu'attaquant, nous pouvons exploiter le fait que nous connaissons la position probable de ces éléments ajoutés pour créer des mots de passe dynamiques à partir de nos listes de mots

### Comment créer des règles personnalisées 

Les règles personnalisées sont définies dans le john.conf fichier, généralement situées dans /etc/john/john.conf si vous avez installé John à l'aide d'un gestionnaire de packages ou construit à partir de la source avec make.

Passons en revue la syntaxe de ces règles personnalisées. Notez qu'il existe un niveau massif de contrôle granulaire que vous pouvez définir dans ces règles, je suggérerais de jeter un œil au wiki [ici](https://www.openwall.com/john/doc/RULES.shtml) afin d'avoir une vue complète des types de modificateurs que vous pouvez utiliser, ainsi que d'autres exemples de mise en œuvre des règles.

La première ligne :

[List.Rules:THMRules]- Sert à définir le nom de votre règle, c'est ce que vous utiliserez pour appeler votre règle personnalisée en tant qu'argument John.

Nous utilisons ensuite une correspondance de modèle de style regex pour définir où le mot sera modifié, encore une fois - nous ne couvrirons ici que les modificateurs de base et les plus courants :

- Az : Prend le mot et l'ajoute avec les caractères que vous définissez    
- A0 : Prend le mot et le préfixe avec les caractères que vous définissez    
- c : Capitalise le caractère positionnellement    

Ceux-ci peuvent être utilisés en combinaison pour définir où et quoi dans le mot que vous souhaitez modifier.

Enfin, nous devons ensuite définir quels caractères doivent être ajoutés, préfixés ou autrement inclus, nous le faisons en ajoutant des jeux de caractères entre crochets [ ] dans l'ordre dans lequel ils doivent être utilisés. Ceux-ci suivent directement les modèles de modificateur à l'intérieur des guillemets doubles " ". Voici quelques exemples courants :    

- [0-9]- Comprendra les numéros 0-9    
- [0]- Comprendra uniquement le chiffre 0    
- [A-z]- Comprendra à la fois des majuscules et des minuscules    
- [A-Z]- Ne comprendra que des lettres majuscules     
- [a-z]- Inclura uniquement des lettres minuscules     
- [a]- Comprendra seulement un      
- [!£$%@]- Inclura les symboles !£$%@    

Exemple : générer une liste de mots à partir des règles qui correspondraient à l'exemple de mot de passe "Polopassword1!" (en supposant que le mot polopassword était dans notre liste de mots), nous créerions une entrée de règle qui ressemble à ceci : [List.Rules:PoloPassword] cAz"[0-9] [!£$%@]"

### Utilisation de règles personnalisées
Nous pourrions alors appeler cette règle personnalisée comme un argument John en utilisant : `john --wordlist=[path to wordlist] --rule=PoloPassword [path to file]`


En guise de remarque, je trouve utile de parler des modèles si vous écrivez une règle - comme indiqué ci-dessus, il en va de même pour l'écriture de modèles RegEx.     
Jumbo John est déjà livré avec une longue liste de règles personnalisées, qui contiennent des modificateurs à utiliser dans presque tous les cas. Si vous êtes bloqué, essayez de regarder ces règles si votre syntaxe ne fonctionne pas correctement.

## Mot de passe protégeant les fichier ZIP

Nous pouvons utiliser John pour déchiffrer le mot de passe sur les fichiers Zip protégés par mot de passe. Encore une fois, nous allons utiliser une partie distincte de la suite d'outils john pour convertir le fichier zip dans un format que John comprendra, mais à toutes fins utiles, nous allons utiliser la syntaxe que vous êtes déja assez familier maintenant.

### Zip2John
De la même manière que pour l'outil de suppression d'ombre que nous avons utilisé précédemment, nous allons utiliser l'outil zip2john pour convertir le fichier zip dans un format de hachage que John est capable de comprendre et, espérons-le, de déchiffrer. L'utilisation de base est la suivante :

`zip2john [options] [zip file] > [output file]`

### Craquage
Nous pouvons alors prendre le fichier que nous avons sorti de zip2john et, comme nous l'avons fait avec unshadow, l'alimenter directement dans John car nous avons créé l'entrée spécifiquement pour lui.

`john --wordlist=/usr/share/wordlists/rockyou.txt zip_hash.txt`

## Mot de passe protégeant les fichier RAR

Nous pouvons utiliser un processus similaire à celui que nous avons utilisé dans la dernière tâche pour obtenir le mot de passe pour les archives rar. Si vous n'êtes pas familier, les archives rar sont des fichiers compressés créés par le gestionnaire d'archives Winrar. Tout comme les fichiers zip, ils compressent une grande variété de dossiers et de fichiers.

### Rar2John
Presque identique à l'outil zip2john que nous venons d'utiliser, nous allons utiliser l'outil rar2john pour convertir le fichier rar dans un format de hachage que John est capable de comprendre. La syntaxe de base est la suivante :

`rar2john [rar file] > [output file]`

### Craquage
Encore une fois, nous sommes alors en mesure de prendre le fichier que nous avons sorti de rar2john dans notre exemple de cas d'utilisation appelé "rar_hash.txt" et, comme nous l'avons fait avec zip2john, nous pouvons l'introduire directement dans John.

`john --wordlist=/usr/share/wordlists/rockyou.txt rar_hash.txt`

## Mot de passe pour les clés ssh

Explorons une autre utilisation de John qui revient semi-fréquemment dans les défis CTF. Utilisation de John pour déchiffrer le mot de passe de la clé privée SSH des fichiers id_rsa. Sauf configuration contraire, vous authentifiez votre connexion SSH à l'aide d'un mot de passe. Cependant, vous pouvez configurer l'authentification basée sur une clé, qui vous permet d'utiliser votre clé privée, id_rsa, comme clé d'authentification pour vous connecter à une machine distante via SSH. Cependant, cela nécessitera souvent un mot de passe - ici, nous utiliserons John pour déchiffrer ce mot de passe afin de permettre l'authentification sur SSH à l'aide de la clé.

### SSH2John
ssh2john convertit la clé privée id_rsa que vous utilisez pour vous connecter à la session SSH dans un format de hachage avec lequel john peut travailler. La syntaxe correspond à ce que vous attendez. Notez que si vous n'avez pas installé ssh2john, vous pouvez utiliser ssh2john.py, qui se trouve dans /opt/john/ssh2john.py. Si vous faites cela, remplacez la ssh2john commande par python3 /opt/ssh2john.py ou sur Kali, python /usr/share/john/ssh2john.py.

`ssh2john [id_rsa private key file] > [output file]`

### Craquage

Pour la dernière fois, nous alimentons le fichier que nous avons sorti de ssh2john, qui dans notre exemple de cas d'utilisation s'appelle "id_rsa_hash.txt" et, comme nous l'avons fait avec rar2john, nous pouvons l'utiliser de manière transparente avec John :

john --wordlist=/usr/share/wordlists/rockyou.txt id_rsa_hash.txt

