# Commande de l'outil git

## Les classiques : add, commit, branch, checkout

- `git commit` : comme un snapchot, sauvegarde les modification effectué. lié à un commit parent. il faut indéxé les modifs avant.  
--amend permet de modifier le commit en cours  
-m pour ajouter un message  
-a pour lui dire d'indexer automatiquement les fichiers qui ont été modifié ou supprimé mais n'ajoute pas les fichiers nouvellement crée  

- `git add [fichier]` : ajoute les fichiers pour la validation avec le commit    
-f ajoute les fichiers ingorés  
mettre un . à la place du fichier permet à git d'indexer tous seul les modif (ajout, suppression et modification)  
-A fait la meme chose que le .  

- `git rm` : supprime un fichier du depot (local et distant) avant un commit  

- `git branch [nom] [endroit]` : créer une nouvelle branche à partir d'une branche principale, endroit spécifie où on veut qu'elle se crée  
-d supprime la branche  
-f réassigne la branche à un commit  

- `git chekout [nom]` : changement de branche   

## Liés au dépot distants : clone, remote, fetch, pull, push

- `git clone [url]` : permet de créer une copie local du depot distant. le depot sera nommé origin, la branche Origin/main représente la branche distante main. git créeer des branche local qui suive les branche distante  

- `git remote add [url]` : permet d'jaouter au depot distant les fichiers et dossier qu'on a dans le repertoire  

- `git fetch` : rapporter des données depuis le distant vers le local, il télécharge les données necessaires mais ne met pas à jours les fichiers, on ne se trouve pas dans le meme etat que le distant  
on peut faire un merge sur Origin/main pour integrer les commit qu'on a télélchargé avec fetch  
les argument sont les meme que pour push mais le sens est inversé car on va du distant sur le local  
si on met rien dans source : on crée juste une nouvelle branche  

- `git pull` : met à jour le depot local par rapport au distant (équivaut à un fetch puis un merge)  
par conséquent on peut mettre les meme argument que fetch   
--rebase : équivaut à un fetch puis un rebase  
1) git fetch : télécharger les nouvelles modification partagé  
2) git rebase origin/main : on rajoute notre travail on vient mettre la branche sur laquelle on est après la version à jour du distant  

- `git push [depot ex:origin] [source]:[destination]` : envoie nos modification au depot distant et le met à jour avec nos commit  
ne fonctionne pas si il y a des différence d'historique entre distant et local, il faut d'abord ce mettre à jour  
les argument en sont pas obligatoire : source désinge l'endroit local que l'on veut pousser et destination l'endroit où on veut le mettre (ca peut etre un nom de branche ou un commit)  
astuce : destination peut etre un nouveau nom de branche  
si on ne met rien dans source on supprime la branche destination  

Astuce :
si on a fait un commit sans créer de nouvelle branche on peut toujours créer une nouvelle branche où on est (nos modif seront dedans) puis supprimer le dernier commit (git reset HEAD^) et enfin push

## Fusionner et déplacer notre travail : merge et rebase

- `git merge [nom]` : fusionner 2 branches, il faut se placer sur la branche sur laquelle on veut merger. garde un vrai historique mais plus compliqué de s'y retrouver  
--no-ff permet de rendre plus visible l'historique quand on a fait des rebase  

objectif : créer une nouvelle branche, developper la fonctionalité puis intégrer la nouvelle fonctionalité en combiant cette branche à la branche d'origine 

- `git rebase [nom] [nom2]` : on se place sur la branche que l'on veut transplanter ou on l'ecrit dans nom2 et nom désigne la branche principale (destination). déplace une branche au bout d'une autre, permet un ordre des tache sur une seule ligne (mais peut fausser l'historique). ensuite on peut faire un merge  
-i [endroit ex HEAD~4]: interactif, ouvre une fenetre graphique. permet de réarranger les commit, peut omettre des commit (pick). git recopie les commit et dans l'ordre spécifier et deplace la branche dessus  

HEAD : le nom symbolique pour le commit sur lequel nous nous situons actuellement. HEAD pointe toujours sur le commit le plus récent dans l'arbre des commits  
^ permet de se placer au commit parent (on peut mettre un nombre pour dire lequel parent avec les merges par exemple)  
~[nombre] remonter nombre fois de commit en arrière  
astuce : on peut enchainer ces symbole  

- `git cherry-pick [commit1] [commit2] .. ` : copie des commit en desous de où on se situe actuellemnt, permet de ne pas prendre tous les commits  

## Avoir des infos : log, diff, blame, status

- `git log` : connaitre les identifiants des commit identifié par des hash. seulement besoin de tapeer les 8 premeirs caracteres. Cela permet de puvoir se deplacer sur les commits.  

- `git diff` : nous affiche les changements par rapport notre branche  

- `git blame [nom de fichier]` : donne des informations sur les modifs qui ont été faites, le nom et l'heure  

- `git status` : affiche des inforamtions sur labranche sur laquelle on est et ce qu'on a fait  

- `git tag [mot] [commit]` : permet de mettre des référence au commit  

## Supprimer un développement : reset, revert, restore

- `git reset [endroit ex HEAD^]` : annule des changements en déplaçant la référence en arrière dans le temps sur un commit plus ancien. En ce sens, on peut considérer cela comme une façon de "réécrire l'histoire"; git reset nous fait remonter en arrière à l'endroit que l'on veut comme si le(s) commit(s) n'avait jamais eu lieu.  
fonctionne pour les branche locales mais pas sur les branches distantes  
--hard réinitialise toutes les modif, les index et l'arbre de travail  

- `git revert [endroit]` : spécifier l'endroit où on veut revenir afin d'annuler des changements et les partager avec les autres. création nouveau commit avec l'annulation et puis on peut push se nouveau commit alors que reset ne crée pas de nouveau commit  

- `git restore` : nous remet au dernier commit donc enlève les modis qu'on a fait et qu'on a pas sauvegarder  

## Sauvegarder des modifs et réappliquer : stash

- `git stash` : sauvegarde nos modifs et enregistre l'etat actuel pour pouvoir faire autre chose  
git stash pop : restorer notre sauvegarde et la supprime de la pile  
git stash list : liste les modif enregistré  
git stash show : affiche les modifications  
git stash drop : supprime la sauvegarde  
git stash apply : applique les modif sauvegarder mes ne les supprime pas de la pile  
on peut en faire plusieurs et donc spécifier une référence (stash@{0} correspond au dernier stash)  