# SecDevOps

## Comment fonctionne DevOps ?

DevOps est visualisé comme une boucle infinie, décrivant toutes les phases qui le composent :    

![image devops](/images/secdevops.jpg "Schéma DevOps")

En suivant la boucle infinie du diagramme DevOps, développons certains outils et processus DevOps que nous examinerons au fur et à mesure que nous suivrons la voie DevSecOps et comment ils aident une organisation :


1. CI/CD (intégration continue et déploiement continu) traite de la fusion fréquente du code et de l'ajout de tests de manière automatisée pour effectuer des vérifications au fur et à mesure que le nouveau code est poussé et fusionné. Nous pouvons tester le code au fur et à mesure que nous poussons et fusionnons grâce à une nouvelle dynamique et routine de déploiement, qui prend la forme de modifications mineures du code systématiquement et régulièrement. Grâce à ce changement de dynamique, le CI/CD aide à détecter les bogues plus tôt et diminue massivement l'effort de maintenance du code modulaire, ce qui introduit des rollbacks fiables des versions/code.   
2. INFRASTRUCTURE AS CODE (IaC) - un moyen de gérer et de provisionner l'infrastructure via le code et l'automatisation. Grâce à cette approche, nous pouvons réutiliser le code utilisé pour déployer l'infrastructure (par exemple, les instances cloud), ce qui facilite la création et la gestion de ressources incohérentes. Les outils standard pour IaC sont terraform, vagrant, etc. Nous utiliserons ces outils plus loin dans le parcours au fur et à mesure que nous expérimentons la sécurité IaC.   
3. GESTION DE LA CONFIGURATION - C'est là que l'état de l'infrastructure est géré en permanence et que les changements sont appliqués efficacement, ce qui la rend plus maintenable. Grâce à cela, beaucoup de temps est gagné et plus de visibilité sur la configuration de l'infrastructure. Vous pouvez utiliser IaC pour la gestion de la configuration.   
4. ORCHESTRATION – L'orchestration est l'automatisation des workflows. Il aide à atteindre la stabilité; par exemple, en automatisant la planification des ressources, nous pouvons avoir des réponses rapides chaque fois qu'il y a un problème (par exemple, les contrôles de santé échouent) ; cela peut être réalisé grâce à la surveillance.    
5. SURVEILLANCE - se concentre sur la collecte de données sur les performances et la stabilité des services et de l'infrastructure. Cela permet une récupération plus rapide, aide à la visibilité inter-équipes, fournit plus de données à analyser pour une meilleure analyse des causes profondes et génère également une réponse automatisée, comme mentionné précédemment.   
6. MICROSERVICES - Une architecture qui décompose une application en plusieurs petits services. Cela présente plusieurs avantages, tels que la flexibilité en cas de besoin d'évolutivité, une complexité réduite et davantage d'options pour choisir la technologie parmi les microservices. Nous les examinerons plus en détail dans le parcours DevSecOps.   

## Shifting Left

### Intro

Vous avez peut-être entendu parler du concept "Shifting Left". Cela signifie que  les équipes DevOps se concentrent sur l'instauration de la sécurité dès les premières étapes du cycle de vie du développement et sur l'introduction d'une culture plus collaborative entre le développement et la sécurité.

Comme la sécurité peut maintenant être introduite tôt, les risques sont considérablement réduits. Dans le passé, vous découvriez les failles de sécurité et les bogues à des stades très avancés, même en production. Ils entraînent des tensions, des retours en arrière et des pertes économiques. L'intégration d'outils d'analyse de code et de tests automatisés plus tôt dans le processus peut désormais identifier ces failles de sécurité au début du développement.

### Concept

Auparavant, les tests de sécurité étaient mis en œuvre à la fin du cycle de développement. Au fur et à mesure que l'industrie évoluait et que des fonctions de sécurité étaient introduites, les équipes de sécurité effectuaient diverses analyses et tests de sécurité dans les dernières étapes du cycle de vie. 

En fonction des résultats des tests de sécurité, cela permettrait soit à l'application de procéder au déploiement en production, soit de rejeter l'application et de la renvoyer aux développeurs pour qu'ils corrigent les failles identifiées. Cela a entraîné de longs retards de développement et des frictions entre les équipes.

La mise en œuvre de mesures de sécurité à toutes les étapes du cycle de vie du développement (décalage vers la gauche) plutôt qu'à la fin du cycle garantira que le logiciel est conçu avec les meilleures pratiques de sécurité intégrées. ne serait pas nécessaire d'annuler les modifications car elles sont traitées à temps. Cela réduit les coûts, renforce la confiance et améliore la sécurité et la qualité du produit.

### Pourquoi ?

À l'époque, avant Agile, les développeurs demandaient une infrastructure au service informatique et recevaient des serveurs des semaines ou des mois plus tard. De nos jours, ce provisionnement de l'infrastructure dans le cloud est automatisé. Ce changement a amélioré la productivité et la vitesse de développement. Cependant, cette vitesse accrue peut également susciter des problèmes de sécurité et entraîner des failles qui peuvent passer inaperçues. 

L'approche Shift-Left garantit que ces défauts sont détectés tôt en introduisant des processus dès le début. Dans cet environnement en évolution rapide, les examens de sécurité post-développement des nouvelles versions logicielles ou l'analyse des configurations d'infrastructure cloud deviennent un goulot d'étranglement. Même lorsque des problèmes sont découverts, il n'y a pas assez de temps pour les résoudre avant l'introduction de la prochaine version ou fonctionnalité. Pour répondre aux besoins des clients, ils ont besoin d'un environnement rapide pour évoluer et croître. La sécurité risque d'être laissée pour compte ; inculquer la sécurité au début et adapter les tests de sécurité pour devenir flexibles et adaptés au cycle de vie du développement augmente les chances de résoudre les problèmes rapidement.

Cette approche de développement vers la gauche dans DevOps peut être appelée DevSecOps.

Avec DevOps, la sécurité est introduite tôt dans le cycle de développement, ce qui minimise considérablement les risques. L'intégration d'outils d'analyse de code et de tests automatisés plus tôt dans le processus peut conduire à une meilleure identification et à l'élimination des failles de sécurité. Et au fur et à mesure que le logiciel arrive au stade du déploiement, tout fonctionne comme prévu. La sécurité n'est pas un ajout. C'est une caractéristique de conception incontournable. L'intégration de la sécurité dans DevOps renforcerait l'impact de DevOps et éliminerait de nombreux autres goulots d'étranglement qui pourraient survenir autrement. Avec l'augmentation de la fréquence des cybermenaces et le durcissement des réglementations, ajouter de la sécurité au DevOps n'est plus un choix mais bien une obligation.

## DevSecOps

### Objectifs

DevSecOps aide à réduire les vulnérabilités, maximise la couverture des tests et intensifie l'automatisation des cadres de sécurité. Cela réduit considérablement les risques, aidant les organisations à prévenir les atteintes à la réputation de la marque et les pertes économiques dues aux incidents de failles de sécurité, facilitant ainsi la vie des audits et de la surveillance.

### Comment ?

La culture est la clé. Cela ne fonctionne pas sans communication ouverte et sans confiance. Cela ne fonctionne qu'avec un effort collectif. DevSecOps devrait viser à combler les lacunes en matière de connaissances en matière de sécurité entre les équipes ; pour que chacun pense et soit responsable de la sécurité, il faut d'abord les outils et les connaissances nécessaires pour conduire cette autonomie de manière efficace et en toute confiance.

### Silos de sécurité

Il est courant que de nombreuses équipes de sécurité soient exclues des processus DevOps et présentent la sécurité comme une entité distincte, où des personnes spécialisées ne peuvent que maintenir et diriger les pratiques de sécurité. Cette situation crée un silo autour de la sécurité et empêche les ingénieurs de comprendre la nécessité de la sécurité ou d'appliquer des mesures de sécurité dès le début.

Ce n'est pas évolutif ou flexible. La sécurité doit être une fonction de soutien pour aider les autres équipes à évoluer et à renforcer la sécurité, sans que les équipes de sécurité soient un obstacle, mais plutôt une rampe pour promouvoir des solutions et des décisions sécurisées. La meilleure pratique consiste à partager ces responsabilités entre tous les membres de l'équipe au lieu d'avoir un ingénieur en sécurité spécialisé.

### Manque de visibilité et de priorisation

Viser à créer une culture où la sécurité et d'autres composants essentiels de l'application traitent la sécurité comme un aspect normal de l'application. Les développeurs peuvent alors se concentrer sur le développement avec confiance en la sécurité au lieu que les services de sécurité jouent à la police et au jeu du blâme. La confiance doit s'établir entre les équipes, et la sécurité doit favoriser l'autonomie des équipes en établissant des processus qui inculquent la sécurité.

### Processus rigoureux

Chaque nouvelle expérience ou logiciel ne doit pas passer par un processus compliqué et une vérification des conformités de sécurité avant d'être utilisé par les développeurs. Les procédures doivent être flexibles pour tenir compte de ces scénarios, où les tâches de niveau inférieur doivent être traitées différemment, et les tâches et modifications à haut risque sont ciblées pour ces processus plus stricts.

Les développeurs ont besoin d'environnements pour tester de nouveaux logiciels sans limitations de sécurité communes. Ces environnements sont connus sous le nom de "SandBox", qui sont des environnements temporairement isolés. Ces environnements n'ont aucune connexion à un réseau interne et n'ont aucune donnée client.

## Culture DevSecOps

### Favoriser l'autonomie des équipes

Qu'il s'agisse d'une grande organisation ou d'une start-up en hypercroissance, la seule façon de ne pas laisser de côté la sécurité est de favoriser l'autonomie des équipes. Cela peut être fait en automatisant les processus qui s'intègrent parfaitement au pipeline de développement jusqu'à ce que les tests de sécurité deviennent juste un autre type de test, comme les tests unitaires, les bombes fumigènes, etc.

Donner l'exemple et promouvoir l'éducation comme la création de playbooks / runbooks pour repérer ces défauts et les corriger, comprendre leur risque et renforcer la confiance dans les ingénieurs pour prendre la décision sécurisée de manière indépendante. Le ratio de développeurs, plate-forme, ingénieurs d'infrastructure, etc., ne sera pas le même que les ingénieurs de sécurité, et nous devons comprendre qu'ils ne peuvent pas être dans toutes les conversations. La sécurité doit agir comme une fonction de support qui se concentre sur l'établissement de la confiance et la création d'autant de chevauchement de connaissances entre les équipes que possible.

### Visibilité et transparence

Pour chaque outil introduit ou pratiqué, il doit y avoir un processus de soutien qui offre une visibilité et favorise la transparence pour les autres équipes. Cela signifie que si nous voulons construire l'autonomie des groupes, comme mentionné précédemment, ils doivent avoir une visibilité sur l'état de sécurité du service qu'ils possèdent ou maintiennent. Par exemple, un tableau de bord visualise le nombre de failles de sécurité selon la criticité du service. Cela permet de hiérarchiser en conséquence, afin que les tâches ne se perdent pas dans l'arriéré ou le bruit, et qu'elles puissent s'attaquer aux défauts au bon moment. La mesure de l'état de sécurité dépend de l'entreprise, mais il peut s'agir du nombre de résultats élevés qu'un service peut avoir ou non qui détermine s'il est dans un bon état de sécurité.

La transparence ferait référence à la mise en place d'outils et de pratiques accessibles aux équipes. Par exemple, si vous présentez une vérification avant de fusionner du code, et que la révision ne passe pas et affiche un message disant "signature d'un éventuel défaut d'injection de code détecté, veuillez y remédier", le développeur ou l'ingénieur doit avoir accès à l'outil qui signale ce message. Habituellement, ces outils d'analyse qui signalent ces alertes ont une interface utilisateur qui spécifie la ligne de code où elle est affectée. Ils comprennent une définition et une suggestion de correction avec des étapes. Dans cet exemple, un rôle de développeur peut être créé afin qu'il ait accès à plus d'informations. Cela favorise l'éducation et l'autonomie en étendant la transparence qui, traditionnellement, n'était accessible qu'aux équipes de sécurité.

### Tenir compte de la flexibilité grâce à la compréhension et à l'empathie

Comme mentionné précédemment, instiller la sécurité dans les processus DevOps avec visibilité et transparence n'est pas une tâche facile. Il y a un facteur qui peut déterminer le succès : le niveau de compréhension et d'empathie. Cela signifie que la définition du risque pour les équipes de sécurité est sans équivoque, mais pour les autres équipes, le risque peut être différent et tout aussi précis pour elles. Cela ne s'applique pas seulement au risque, mais à un ensemble de choses ; cela se ramifie dans ce qu'ils priorisent, comment ils travaillent et ce qu'ils pensent être suffisamment important pour laisser de côté un projet avec un délai serré pour corriger un bogue.

Il n'y a pas d'outil ou de processus magique pour tout le monde. Il est essentiel de comprendre comment travaillent les développeurs/ingénieurs, ce qu'ils savent être un risque et ce qu'ils priorisent. Si vous connaissez leur point de vue, il est plus facile de construire un processus qui trouve un terrain d'entente et a plus de chances de fonctionner que d'ajouter un autre outil qui crée plus de bruit et de stress pour tout le monde. Cette compréhension crée une perspective, qui tient compte de l'empathie pour la façon dont les autres équipes travaillent et construit un processus qui tient compte de la flexibilité. Cela est nécessaire car chaque situation peut être différente, les délais peuvent être différents et la bande passante peut changer avec le temps.

En tant qu'ingénieur DevSecOps, supposons que vous preniez le temps de comprendre comment une équipe possède un service. Dans ce cas, cela aura un scanner de sécurité ajouté à son processus de développement, travaillé et vu en priorité ; il sera plus facile d'obtenir leur adhésion et de démontrer leur valeur. Par exemple, s'il s'agit d'une équipe de plate-forme et qu'elle possède un service interne mais un service principal, un risque serait un bogue qui perturbe le service, et non une injection potentielle qui vit derrière un proxy. Vous auriez besoin d'informations d'identification internes pour l'exploiter. Vous pouvez régler les scanners ou ajouter un processus de triage qui répond aux questions qu'ils se poseraient, ce qui, à son tour, renforcerait la confiance contre le loup qui pleure et les processus de sécurité remis en question.