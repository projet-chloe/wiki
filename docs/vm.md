utiliser version 6.1.28 de virtual box

décocher certaines fonctionalité windows:  
- HyperV  
- Plateforme de l'hyperviseur windows   
- Plateforme d'ordianteur virtuel   
- lancer powershell en tant qu'administrateur et executer : bcdedit /set hypervisorlaunchtype off   

créer une nouvelle VM  
mettre beaucoup de configuration    
- 8 Go RAM   
- 2 processeur   
- 15 à 20 Go stockage   

ensuite lancer et insérer l'iso (téléchargé avant)    

modifier configuration compte :  
- définir le mot de passe d'un utilisateur (root aussi) : sudo passwd username    
- se rajouter dans les sudoers : usermod -aG sudo [user]   
- modifier le username : usermod -l newusername oldusername   

installer les addons :    
- inserer le disque   
- essayer de le lancer    
- sinon aller dans le repertoire /media/cdrom0   
- executer ./VBoxLinuxAdditions.run (tester sh VBoxLinuxAdditions.run si ca marche pas)   
- sinon rajouter exec dans les options dans le fichier /etc/fstab    

utiliser git :   
- créer une paire de clé ssh : ssh keygen -t rsa -b 4096   
les clé seront stockées dans le dossier .ssh   
- copier le contenu de id_rsa.pub et le mettre dans gitlab (cat ~/.ssh/id_rsa.pub)    
- git config --global user.name "[Nom Prenom]"   
- git config --global user.email "[email]"   

créer une environnement python :   
- installer venv : sudo apt install python3-venv  
- créer l'enviro : python3 -m venv [nom]  
- activer : source [nom]/bin/activate  
- deactiver : deactivate     

récuperer mon wiki :   
- git clone [nom repertoire]   
- créer l'enviro dans le dossier   
- après activation de l'enviro : pip install mkdocs-material   
- créer un script lancer_wiki.sh dans le repertoire :   
    #!/bin/bash  
    source [nom]/bin/activate  
    mkdocs serve  
- installer vscode   
- créer les alias : aller dans le fichier .zshrc (ou .bashrc)   
- rajouter ces deux lignes :   
alias wikiedit='cd ~/Documents/wiki && code .'   
alias wikirun='cd ~/Documents/wiki && ./lancer_wiki.sh'  

installer vscode :   
- aller sur la page officiel   
- télécharger de .deb correspondant   
- l'executer sudo apt install ./[fichier]    

modifier les configuration clavier :   
- sudo dpkg-reconfigure keyboard-configuration   
- choisir les config (laisser les choix par défaut pour tous sauf la langue)   
- sudo service keyboard-setup restart   
- restart la machine   

améliorer vim :
- créer un fichier .vimrc dans le dossier utilisateur (~/.vimrc)  
- mettre ca dedans   
    set number "show line number
    set wrap   "wrap line 
    set encoding=utf-8
    syntax enable
    set noshowmode 
    set ai
    set expandtab
    set tabstop=4 "number of spaces per <TAB>
    set shiftwidth=4
    set nocompatible
    set hidden
    set termguicolors
    set list
    set termguicolors
    set hlsearch "highligh regex
    command W execute 'silent w !sudo tee %  devnull  edit!'
